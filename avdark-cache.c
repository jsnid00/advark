/**
 * Cache simulation using a functional system simulator.
 *
 * Course: Advanced Computer Architecture, Uppsala University
 * Course Part: Lab assignment 1
 *
 * Original authors: UART 1.0(?)
 * Modified by: Andreas Sandberg <andreas.sandberg@it.uu.se>
 * Revision (2015, 2016): German Ceballos 

 * Additions made to implement D2D by Johan Snider
 *
 */

#include "avdark-cache.h"

#include <stdarg.h>
#include <string.h>
#include <execinfo.h>
#include <stdio.h>

#ifdef SIMICS
/* Simics stuff  */
#include <simics/api.h>
#include <simics/alloc.h>
#include <simics/utils.h>

#define AVDC_MALLOC(nelems, type) MM_MALLOC(nelems, type)
#define AVDC_FREE(p) MM_FREE(p)

#else

#define AVDC_MALLOC(nelems, type) malloc(nelems * sizeof(type))
#define AVDC_FREE(p) free(p)

#endif

void assert_addr_dbg(avdc_pa_t *addr_ptr, avdc_pa_t pa, int fx_code, int md_index, int md_way, int l_index, int l_way, int li) {


    //TODO: find out why this has to accept both starting from zero and with memory accesses
    if(!((*addr_ptr - BLOCK_SIZE <= pa) || (*addr_ptr <= pa))) {
        printf("addr_ptr1, pa: %llu : %llu -> %d, md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", *addr_ptr, pa, fx_code, md_index, md_way, l_index, l_way, li);
        //printf("md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", md_index, md_way, l_index, l_way, li);


        //exit(1);

    }

    if(!(pa < *addr_ptr + BLOCK_SIZE)) {
        printf("addr_ptr2, pa: %llu : %llu -> %d, md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", *addr_ptr, pa, fx_code, md_index, md_way, l_index, l_way, li);
        //printf("md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", md_index, md_way, l_index, l_way, li);

        //exit(1);


    }

}

void assert_addr(avdc_pa_t *addr_ptr, avdc_pa_t pa, int fx_code) {

   /* //TODO: find out why this has to accept both starting from zero and with memory accesses
    assert((*addr_ptr - BLOCK_SIZE <= pa) || (*addr_ptr <= pa));
    //assert(*addr_ptr <= pa);
    assert(pa < *addr_ptr + BLOCK_SIZE);
    */
    //TODO: find out why this has to accept both starting from zero and with memory accesses
    if(!((*addr_ptr - BLOCK_SIZE <= pa) || (*addr_ptr <= pa))) {
        printf("addr_ptr, pa: %llu : %llu -> %d\n", *addr_ptr, pa, fx_code);
        //exit(1);

        void* callstack[128];
        int i, frames = backtrace(callstack, 128);
        char** strs = backtrace_symbols(callstack, frames);
        for (i = 0; i < frames; ++i) {
            printf("%s\n", strs[i]);
        }
        free(strs);

        //assert(false);

    }
    // //assert(*addr_ptr <= pa);
    if(!(pa < *addr_ptr + BLOCK_SIZE)) {
        printf("addr_ptr, pa: %llu : %llu -> %d\n", *addr_ptr, pa, fx_code);
        //exit(1);


        void* callstack[128];
        int i, frames = backtrace(callstack, 128);
        char** strs = backtrace_symbols(callstack, frames);
        for (i = 0; i < frames; ++i) {
            printf("%s\n", strs[i]);
        }
        free(strs);

        //assert(false);

    }

}

/**
 * Extract the cache line tag from a physical address.
 */
avdc_pa_t
tag_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    /* printf("tag_from_pa: 0x%.16lx, tag = 0x%.16lx\n",
      (unsigned long)pa ,
      (unsigned long)pa >> (self->tag_shift));*/
    return pa >> (self->tag_shift);
}

/*
 * To fill up the md1 and md2 entries
 */
avdc_pa_t
pa_from_tag(avdark_cache_core *self, avdc_pa_t tag) {
    return tag << (self->tag_shift);
}

/**
 * Calculate the cache line index from a physical address.
 */
int
l1_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {

    return (pa >> self->block_size_log2) & (self->number_of_l1_sets - 1);
}

int
l2_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {

    return (pa >> self->block_size_log2) & (self->number_of_l2_sets - 1);
}

int
l3_index_from_pa(avdc_cache *self, avdc_pa_t pa) {
    return (pa >> self->cores[0]->block_size_log2) & (self->l3_sets - 1);
}


//returns the region place for a cache line
int region_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    /* printf("pa: 0x%.16lx, region num = 0x%.16lx\n",
          (unsigned long)pa ,
          (unsigned long)((pa >> self->block_size_log2) & (self->region_size -1)));
      */

    return (pa >> self->block_size_log2) & (REGION_SIZE - 1);
}

int md1_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    /*printf("pa: 0x%.16lx, md1_region = 0x%.16lx\n",
            (unsigned long)pa ,
            (unsigned long)((pa >> self->tag_shift) & (self->md1_size -1)));
    */
    return (pa >> self->tag_shift) & (self->md1_size - 1);
}

int md2_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    return (pa >> self->tag_shift) & (self->md2_size - 1);
}

int md3_index_from_pa(avdc_cache *self, avdc_pa_t pa) {
    return (pa >> self->cores[0]->tag_shift) & (self->md3_size - 1);
}

/**
 * Computes the log2 of a 32 bit integer value. Used in dc_init
 *
 * Do NOT modify!
 */
static int
log2_int32(uint32_t value) {
    int i;

    for (i = 0; i < 32; i++) {
        value >>= 1;
        if (value == 0)
            break;
    }
    return i;
}

/**
 * Check if a number is a power of 2. Used for cache parameter sanity
 * checks.
 *
 * Do NOT modify!
 */
static int
is_power_of_two(uint64_t val) {
    return ((((val) & (val - 1)) == 0) && (val > 0));
}

void
avdc_dbg_log(avdc_cache *self, const char *msg, ...) {
    va_list ap;

    if (self->dbg) {
        const char *name = self->dbg_name ? self->dbg_name : "AVDC";
        printf("[%s] dbg: ", name);
        va_start(ap, msg);
        vfprintf(stdout, msg, ap);
        va_end(ap);
    }
}

int check_l1(int li) {
    if (((li & L1_BASE) == L1_BASE) &&
        ((li & L2_BASE) != L2_BASE) &&
        ((li & L3_BASE) != L3_BASE)) {
        return 1;
    } else {
        return 0;
    }
}

int check_l2(int li) {
    if (((li & L1_BASE) != L1_BASE) &&
    ((li & L2_BASE) == L2_BASE) &&
    ((li & L3_BASE) != L3_BASE)) {
        return 1;
    } else {
        return 0;
    }
}

int check_l3(int li) {
    if ((li & L3_BASE) == L3_BASE) {
        return 1;
    } else {
        return 0;
    }
}

int check_mem(int li) {
    if (((li & L1_BASE) == L1_BASE) &&
     ((li & L2_BASE) == L2_BASE) &&
     ((li & L3_BASE) != L3_BASE)) {
        return 1;
    } else {
        return 0;
    }
}

int check_rem(int li) {
    if ((0 <= li) &&
        (li <= CORES)) {
        return 1;
    } else
        return 0;
}


void
avdc_access(avdc_cache *cache, avdc_pa_t pa, type_t type, int thread_id) {

    //map threads to cores
    thread_id = thread_id % CORES;
    int hit1 = -1, hit2 = -1, hit3 = -1; //hits for different md levels
    cache->accesses += 1;

    hit1 = search_md1(cache, thread_id, pa, type);

    if (hit1 == 0) {
        //MD1 miss
        hit2 = search_md2(cache, thread_id, pa, type);
        if (hit2 == 0) {
            //MD2 miss
            hit3 = search_md3(cache, pa, thread_id, type);
            if (hit3 == 0) {
                ///CASE D.1 untracked to private
                avdc_insert_md3(cache, pa, thread_id, type); //only access if first time for whole region
            }
        }
    }

    //TODO: restructure this
    //TODO: i don't even think i need this anymore
    //TODO: remove maybe
    avdc_pa_t tag = tag_from_pa(cache->cores[thread_id], pa);
    switch (type) {

        case AVDC_READ: /* Read accesses */
            cache->cores[thread_id]->stat.data_read += 1;
            if (hit1 == 0 && hit2 == 0 && hit3 == 0) {
                cache->cores[thread_id]->stat.data_read_miss += 1;
            }
            break;

        case AVDC_WRITE: /* Write accesses */
            avdc_dbg_log(cache, "write: pa: 0x%.16lx, tag: 0x%.16lx, hit1: %d, hit2: %d\n",
                         (unsigned long) pa, (unsigned long) tag, hit1, hit2);
            cache->cores[thread_id]->stat.data_write += 1;
            if (hit1 == 0 && hit2 == 0 && hit3 == 0 )
                cache->cores[thread_id]->stat.data_write_miss += 1;
            break;
    }

}

//insert entry into md1 and md2, and, l1 cache and l2 cache
int avdc_insert(avdc_cache *cache, int thread_id, avdc_pa_t pa) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, pa);
    avdc_pa_t base_pa = pa_from_tag(core, tag);
    int region_num = region_from_pa(core, pa);
    //cache resources
    int l1_index = l1_index_from_pa(core, pa);
    int l2_index = l2_index_from_pa(core, pa);
    int l1_way = find_l1_way(core, pa);
    int l2_way = find_l2_way(cache, thread_id, pa);
    //md resources
    int md1_index = md1_index_from_pa(core, pa);
    int md2_index = md2_index_from_pa(core, pa);

    int md1_way = find_md1_way(core, pa);
    int md2_way = find_md2_way(cache, thread_id, pa);

    avdc_md_t *md1 = &core->md1[md1_index][md1_way];
    avdc_md_t *md2 = &core->md2[md2_index][md2_way];

    avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

    /// CREATE md1 entry
    if (md1->valid == 0) {
        for (int i = 0; i < REGION_SIZE; i++) {
            //initalize to memory ie: 011000
            md1->li[i] = MEM_BASE;
            md1->address[i] = base_pa + (core->block_size * i);
        }
        md1->valid = 1;
        md1->tag = tag;
        md1->active = 1;
    }

    md1->tracker_pointer = md2_way;
    md1->li[region_num] = L1_BASE + l1_way;
    md1->timestamp = clock();
    core->md1_entries++;

    /// CREATE md2 entry
    if ((md2->valid && md2->active) &&
        (md2->tag != tag)) {
        printf("***err: OVERWRITE ACTIVE MD2 INDEX: %d\n", md2_index);
        exit(EXIT_FAILURE);
    }

    if (md2->valid == 0) {
        for (int i = 0; i < REGION_SIZE; i++) {
            //initalize to memory ie: 011000
            md2->li[i] = MEM_BASE;
            md2->address[i] = base_pa + (core->block_size * i);

        }
        md2->valid = 1;
        md2->tag = tag;
        md2->active = 0;
    }

    md2->tracker_pointer = md1_way;  //TODO: this needs to be double checked

    md2->li[region_num] = L1_BASE + l1_way; //Point to active L2 entry
    md2->timestamp = clock();
    md2->active = 0;
    core->md2_entries++;


    /// CREATE l1 entry
    assert(l1->valid == 0 && l1->active == 0);

    l1->valid = 1;
    l1->address = pa;
    l1->active = 1;
    l1->timestamp = clock();
    l1->tracker_pointer = md2_way;
    l1->replacement_pointer = l2_way;
    core->number_of_l1_entries++;

    ///CREATE l2 entry
    assert(l2->valid == 0 && l2->active == 0);

    l2->valid = 1;
    l2->address = pa;
    l2->active = 0;
    l2->timestamp = clock();
    l2->tracker_pointer = md2_way;
    core->number_of_l2_entries++;
    //TODO: l2->replacement_pointer = MD3_way;

    return md1_index;
}


//TODO: remove replicated code so that avdc_insert calls a specific insert_from_md3 function that just sets the MD from md3

//insert entry into md1 and md2, and, l1 cache and l2 cache from md3
int avdc_insert_from_md3(avdc_cache *cache, int thread_id, avdc_pa_t pa) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, pa);
    avdc_pa_t base_pa = pa_from_tag(core, tag);
    int region_num = region_from_pa(core, pa);
    //cache resources
    int l1_index = l1_index_from_pa(core, pa);
    int l2_index = l2_index_from_pa(core, pa);

    //these searches are kinda work around to avoid adding data twice
    //this could happen if a cachline was invalidated unnessesarily
    int l1_way_search = search_l1_way(core, pa);
    int l2_way_search = search_l2_way(core, pa);

    //TODO: it would be nice to restructure to avoid this
    //TODO:doing okay maybe this doesn't happen as much now
    if(l1_way_search != -1 || l2_way_search != -1 )
        return -1;

    int l1_way = find_l1_way(core, pa);
    int l2_way = find_l2_way(cache, thread_id, pa);
    //md resources
    int md1_index = md1_index_from_pa(core, pa);
    int md2_index = md2_index_from_pa(core, pa);

    int md1_way = find_md1_way(core, pa);
    int md2_way = find_md2_way(cache, thread_id, pa);

    avdc_md_t *md1 = &core->md1[md1_index][md1_way];
    avdc_md_t *md2 = &core->md2[md2_index][md2_way];

    int md3_index = md3_index_from_pa(cache, pa);
    int md3_way = find_md3_way(cache, pa);

    avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];

    avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

//    assert(md1->active == 0);
//    assert(md1->valid == 0);
//
//    assert(md2->active == 0);
//    assert(md2->valid == 0);

    assert(md3->active == 1);
    assert(md3->valid == 1);

    /// CREATE md1 entry
    if (md1->valid == 0) {
        for (int i = 0; i < REGION_SIZE; i++) {
            //initalize to memory ie: 011000
            md1->li[i] = md3->li[i];
            md1->address[i] = md3->address[i];
        }
        md1->valid = 1;
        md1->tag = tag;
        md1->active = 1;
    }

    md1->tracker_pointer = md2_way;
    md1->li[region_num] = L1_BASE + l1_way;
    md1->timestamp = clock();
    core->md1_entries++;

    /// CREATE md2 entry
    if ((md2->valid && md2->active) &&
        (md2->tag != tag)) {
        printf("***err: OVERWRITE ACTIVE MD2 INDEX: %d\n", md2_index);
        exit(EXIT_FAILURE);
    }

    if (md2->valid == 0) {
        for (int i = 0; i < REGION_SIZE; i++) {
            //initalize to memory ie: 011000
            md2->li[i] = md1->li[i];
            md2->address[i] = md1->address[i];

        }
        md2->valid = 1;
        md2->tag = tag;
        md2->active = 0;
    }

    md2->tracker_pointer = md1_way;  //TODO: this needs to be double checked

    md2->li[region_num] = L1_BASE + l1_way; //Point to active L2 entry
    md2->timestamp = clock();
    md2->active = 0;
    core->md2_entries++;

    /// CREATE l1 entry
    assert(l1->valid == 0 && l1->active == 0);

    l1->valid = 1;
    l1->address = pa;
    l1->active = 1;
    l1->timestamp = clock();
    l1->tracker_pointer = md2_way;
    l1->replacement_pointer = l2_way;

    ///CREATE l2 entry
    if (l2->valid && l2->active) {
        printf("***OVERWRITE ACTIVE REGION IN L2 :((((---0-0-0: L2 INDEX: %d\n", l2_index);
    }

    l2->valid = 1;
    l2->address = pa;
    l2->active = 0;
    l2->timestamp = clock();
    l2->tracker_pointer = md2_way;
    //TODO: l2->replacement_pointer = MD3_way;

    return md1_index;
}

/**
 * go through the entiure cache and invalidate all copies of the cachline of the va
 * @param cache
 * @param thread_id the calling thread which isn't invalidated, or -1 to invalidate all threads
 * @param va
 */

void invalidate_cachline(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    int region_num = region_from_pa(cache->cores[0], va);

    int md3_index = md3_index_from_pa(cache, va);
    int md3_way = search_md3_way(cache, va);
    assert(md3_way != -1);

    avdc_md3_t * md3 = &cache->md3[md3_index][md3_way];

    //loop through pb and invalidate all cachlines
    int pb = md3->pb;
    for(int i = 0; i<CORES; i++) {
        if(((int)pow((double)2, i) & pb) && i != thread_id) { //TODO-done: test this
            avdark_cache_core *rem_core = cache->cores[i];

            //md resources
            int md1_index = md1_index_from_pa(rem_core, va);
            int md2_index = md2_index_from_pa(rem_core, va);
            int md1_way = search_md1_way(rem_core, va);
            int md2_way = search_md2_way(cache, rem_core, va);
            //assert(md1_way != -1); //doesn't need to be an md1 //todo maybe doesn't neeed md1
            assert(md2_way != -1);

            avdc_md_t *md1 = &rem_core->md1[md1_index][md1_way];
            avdc_md_t *md2 = &rem_core->md2[md2_index][md2_way];

            int li = -1;
            if(md1->active == 1)  {
                li = md1->li[region_num];
                if(thread_id != -1){
                    md1->li[region_num] = thread_id;
                } else {
                    md1->li[region_num] = MEM_BASE;
                }
                md1->private_bit = 0; //TODO- little bit of a work around might not be the best place for this
                                        //TODO-cont but if invalidate cachline is called then it will become a shared region right?
            } else if(md2->active == 1)  {
                li = md2->li[region_num];

                if(thread_id != -1) {
                    md2->li[region_num] = thread_id;
                } else {
                    md2->li[region_num] = MEM_BASE;
                }
                md2->private_bit = 0;
            } else  {
                printf("***err: md active error");
                exit(1);
            }
            //decode li to find out if data is in l1 or l2
            if (check_l1(li)) {

                clear_l1_entry_va(rem_core, va);
                clear_l2_entry_va(rem_core, va); //if it's in l1 then inclusively has to be in l2

            } else if (check_l2(li)) {

                clear_l2_entry_va(rem_core, va);

            } else if ((check_l3(li))) {

                //TODO: double check that this correctly effects the md3 entry

                //TODO-question: we doesn't have to reset the actual data do we

                int l3_index = l3_index_from_pa(cache, va);
                int l3_way = search_l3_way(cache, va);
                //TODO: why did this get commented out
                clear_l3_entry(cache, l3_index, l3_way);

            } else if ((check_mem(li))) {

                //maybe add a statistic

            } else if (check_rem(li)) {
                //error

                //TODO-question: is this an error
                //printf("**info remote data not located in remote node\n");
                //not nessesarily a failure but cool to know
                //exit(EXIT_FAILURE);

                //TODO: maybe an interesting statistic
                ///this code executes for how often a remote node is invalided unnessessarily becasue another node has the data

            } else {
                printf("***err: severe error in li encoding tolken 2948\n");
                exit(EXIT_FAILURE);
            }

        }

    }

}

/**
avdc_insert_into_md_region is called to bring an entry in from memory and add it's data to the cache
the md1 or md2 entry already exists and the data just needs to be added to l1 and/or l2
*/

int avdc_insert_into_md_region(avdc_cache *cache, int thread_id, avdc_pa_t pa, int md_level, int md_way) {

    avdark_cache_core *self = cache->cores[thread_id];

    int region_num = region_from_pa(self, pa);
    avdc_pa_t tag = tag_from_pa(self, pa);

    //cache resources
    int l1_index = l1_index_from_pa(self, pa);
    int l2_index = l2_index_from_pa(self, pa);
    int l1_way = find_l1_way(self, pa);
    int l2_way = find_l2_way(cache, thread_id, pa);
    //md resources
    int md1_index = md1_index_from_pa(self, pa);
    int md2_index = md2_index_from_pa(self, pa);
    int md2_way = -1;
    int md1_way = -1;

    //note: keep in mind md1, md2 already exist, l1 and l2 entries need to be made
    if (md_level == 1) {

        avdc_md_t * md1 = &self->md1[md1_index][md_way];

        assert(md1->tag == tag);

        md2_way = md1->tracker_pointer;
        md1_way = md_way;

        md1->li[region_num] = L1_BASE + l1_way;
        md1->timestamp = clock();

        self->stat.md1_region_hit += 1;
    } else if (md_level == 2) {

        avdc_md_t * md2 = &self->md2[md2_index][md_way];
        assert(md2->tag == tag);


        md1_way = md2->tracker_pointer;
        md2_way = md_way;

        md2->li[region_num] = L1_BASE + l1_way;
        md2->timestamp = clock();

        self->stat.md2_region_hit += 1;
    }

    if (self->l1[l1_index][l1_way].valid && self->l1[l1_index][l1_way].active) {
        printf("***err: OVERWRITE ACTIVE REGION IN L1 (1) :((((---0-0-0: L1 INDEX: %d\n", l1_index);
        exit(EXIT_FAILURE);
    }

    //**info: CREATE l1 entry
    self->l1[l1_index][l1_way].valid = 1;
    self->l1[l1_index][l1_way].address = pa;
    self->l1[l1_index][l1_way].active = 1;
    self->l1[l1_index][l1_way].timestamp = clock();
    self->l1[l1_index][l1_way].tracker_pointer = md2_way;
    self->l1[l1_index][l1_way].replacement_pointer = l2_way;
    self->number_of_l1_entries++;

    if (self->l2[l2_index][l2_way].valid && self->l2[l2_index][l2_way].active)
        printf("***OVERWRITE ACTIVE REGION IN L2 :((((---0-0-0: L1 INDEX: %d\n", l2_index);

    //**info: CREATE l2 entry
    self->l2[l2_index][l2_way].valid = 1;
    self->l2[l2_index][l2_way].address = pa;
    self->l2[l2_index][l2_way].active = 0;
    self->l2[l2_index][l2_way].timestamp = clock();
    self->l2[l2_index][l2_way].tracker_pointer = md2_way;
    self->number_of_l2_entries++;

    return 2;
}

void
avdc_flush_cache(avdark_cache_core *self) {
    for (int i = 0; i < self->number_of_l1_sets; i++) {
        for (int k = 0; k < L1_ASSOC; k++) {
            self->l1[i][k].valid = 0;
            self->l1[i][k].active = 0;
            self->l1[i][k].address = 0;
            self->l1[i][k].tracker_pointer = 0;
            self->l1[i][k].replacement_pointer = 0;
            self->l1[i][k].timestamp = 0;
        }
    }

    for (int i = 0; i < self->number_of_l2_sets; i++) {
        for (int k = 0; k < L2_ASSOC; k++) {
            self->l2[i][k].valid = 0;
            self->l2[i][k].active = 0;
            self->l2[i][k].address = 0;
            self->l2[i][k].tracker_pointer = 0;
            self->l2[i][k].replacement_pointer = 0;
            self->l2[i][k].timestamp = 0;
        }
    }

    for (int i = 0; i < self->md1_size; i++) {
        for (int j = 0; j < MD1_ASSOC; j++) {
            self->md1[i][j].tag = 0;
            self->md1[i][j].valid = 0;
            self->md1[i][j].active = 0;
            self->md1[i][j].timestamp = 0;
            self->md1[i][j].tracker_pointer = 0;
            int g;
            for (g = 0; g < self->region_size; g++) {
                self->md1[i][j].address[g] = 0;
                self->md1[i][j].li[g] = 0;
            }
        }
    }

    for (int i = 0; i < self->md2_size; i++) {
        for (int j = 0; j < MD2_ASSOC; j++) {
            self->md2[i][j].tag = 0;
            self->md2[i][j].valid = 0;
            self->md2[i][j].active = 0;
            self->md2[i][j].timestamp = 0;
            self->md2[i][j].tracker_pointer = 0;
            int g;
            for (g = 0; g < self->region_size; g++) {
                self->md2[i][j].address[g] = 0;
                self->md2[i][j].li[g] = 0;
            }
        }
    }

    self->number_of_l1_entries = 0;
    self->number_of_l2_entries = 0;
    self->md1_entries = 0;
    self->md2_entries = 0;

    //set all stats to 0
    self->stat.data_write = 0;
    self->stat.data_write_miss = 0;
    self->stat.data_read = 0;
    self->stat.data_read_miss = 0;
    self->stat.md1_hit = 0;
    self->stat.md1_l1_hit = 0;
    self->stat.md1_l2_hit = 0;
    self->stat.md1_ns_hit = 0;
    self->stat.md1_fs_hit = 0;
    self->stat.md1_rem_hit = 0;
    self->stat.md1_mem_hit = 0;
    self->stat.md1_region_hit = 0;
    self->stat.md2_region_hit = 0;
    self->stat.md2_hit = 0;
    self->stat.md2_l1_hit = 0;
    self->stat.md2_l2_hit = 0;
    self->stat.md2_ns_hit = 0;
    self->stat.md2_fs_hit = 0;
    self->stat.md2_rem_hit = 0;
    self->stat.md2_mem_hit = 0;
    self->stat.md2_mem_miss = 0;
    self->stat.md3_hit = 0;
    self->stat.md3_l1_hit = 0;
    self->stat.md3_l2_hit = 0;
    self->stat.md3_ns_hit = 0;
    self->stat.md3_fs_hit = 0;
    self->stat.md3_rem_hit = 0;
    self->stat.md3_mem_hit = 0;
}

void
avdc_flush_cache_full(avdc_cache *self) {

    for(int i = 0; i < CORES; i ++) {
        avdc_flush_cache(self->cores[i]);
    }
    for (int i = 0; i < self->l3_sets; i++) {
        for (int k = 0; k < self->l3_assoc; k++) {
            self->l3[i][k].valid = 0;
            self->l3[i][k].active = 0;
            self->l3[i][k].address = 0;
            self->l3[i][k].tracker_pointer = 0;
            self->l3[i][k].replacement_pointer = 0;
            self->l3[i][k].timestamp = 0;

            //TODO: add l3 specifics if nessesary
        }
    }

    for (int i = 0; i < self->md3_size; i++) {

        //TODO-DONE add md3 specifics like pb

        for (int j = 0; j < self->md3_assoc; j++) {
            self->md3[i][j].tag = 0;
            self->md3[i][j].valid = 0;
            self->md3[i][j].active = 0;
            self->md3[i][j].timestamp = 0;
            self->md3[i][j].pb = 0;

            int g;
            for (g = 0; g < self->cores[0]->region_size; g++) {
                self->md3[i][j].address[g] = 0;
                self->md3[i][j].li[g] = 0;
            }
        }
    }
    /*
    self->number_of_l3_entries = 0;
    self->md3_entries = 0;
     */

    self->stat3.md3_hit = 0;
    self->stat3.md3_l1_hit = 0;
    self->stat3.md3_l2_hit = 0;
    self->stat3.md3_l3_hit = 0;
    self->stat3.md3_ns_hit = 0;
    self->stat3.md3_fs_hit = 0;
    self->stat3.md3_rem_hit = 0;
    self->stat3.md3_mem_hit = 0;
    self->stat3.md3_mem_miss = 0;

}


int
avdc_resize(avdark_cache_core *core,
            avdc_size_t size,
            avdc_block_size_t block_size,
            avdc_assoc_t assoc,
            avdc_region_t region_size) {

    /* Verify that the parameters are sane */
    if (!is_power_of_two(size) ||
        !is_power_of_two(block_size) ||
        !is_power_of_two(region_size)) {
        printf("size, block-size, assoc and region size all have to be powers of two and > zero\n");
        return 0;
    }

    /* Update the stored parameters */
    core->l1_size = L1_SIZE;
    core->block_size = BLOCK_SIZE; //TODO: make this dynamic
    //core->assoc = ASSOC;
    core->region_size = REGION_SIZE; //TODO: make this dynamic

    core->l2_size = L1_SIZE * 8; //TODO-fix: 8 times bigger is the spec

    /* Save some common values */
    core->number_of_l1_sets = (core->l1_size / core->block_size) / L1_ASSOC;

    core->max_l1_entries = core->number_of_l1_sets * L1_ASSOC;

    core->number_of_l2_sets = (core->l2_size / core->block_size) / L2_ASSOC;

    core->block_size_log2 = log2_int32(core->block_size);


    core->region_shift = log2_int32(core->region_size);


    core->tag_shift = core->block_size_log2 + core->region_shift;
    core->md1_size = MD1_SIZE;
    core->md2_size = MD2_SIZE;

    /*printf("block size:\t %d\n", self->block_size );
    printf("assoc:\t %d\n", self->assoc );

    printf("regin size:\t %d\n", self->region_size );
    printf("l1 size:\t %d\n", self->l1_size );
    printf("l2 size:\t %d\n", self->l2_size );
    printf("md1 size:\t %d\n", self->md1_size );
    printf("md2 size:\t %d\n", self->md2_size );*/


    /* (Re-)Allocate space for the tags array */
    if (core->l1)
        AVDC_FREE(core->l1);
    core->l1 = (avdc_cache_line_t **) AVDC_MALLOC(core->number_of_l1_sets, avdc_cache_line_t*);
    for (int i = 0; i < core->number_of_l1_sets; i++) {
        core->l1[i] = (avdc_cache_line_t *) AVDC_MALLOC(L1_ASSOC, avdc_cache_line_t);
    }

    if (core->md1)
        AVDC_FREE(core->md1);
    core->md1 = (avdc_md_t **) AVDC_MALLOC(core->md1_size, avdc_md_t*);
    for (int i = 0; i < core->md1_size; i++) {
        core->md1[i] = (avdc_md_t *) AVDC_MALLOC(MD1_ASSOC, avdc_md_t);
    }

    if (core->md2)
        AVDC_FREE(core->md2);
    core->md2 = (avdc_md_t **) AVDC_MALLOC(core->md2_size, avdc_md_t*);
    for (int i = 0; i < core->md2_size; i++) {
        core->md2[i] = (avdc_md_t *) AVDC_MALLOC(MD2_ASSOC, avdc_md_t);
    }

    if (core->l2)
        AVDC_FREE(core->l2);
    core->l2 = (avdc_cache_line_t **) AVDC_MALLOC(core->number_of_l2_sets, avdc_cache_line_t*);
    for (int i = 0; i < core->number_of_l2_sets; i++) {
        core->l2[i] = (avdc_cache_line_t *) AVDC_MALLOC(L2_ASSOC, avdc_cache_line_t);
    }

    /* Flush the cache, this initializes the tag array to a known state */
    avdc_flush_cache(core);

    return 1;
}

void
avdc_reset_statistics(avdark_cache_core *self) {
    self->stat.data_read = 0;
    self->stat.data_read_miss = 0;
    self->stat.data_write = 0;
    self->stat.data_write_miss = 0;

    self->stat.md1_hit = 0;
    self->stat.md1_l1_hit = 0;
    self->stat.md1_l2_hit = 0;

    self->stat.md2_hit = 0;
    self->stat.md2_l1_hit = 0;
    self->stat.md2_l2_hit = 0;

    //TODO: add all stats if neccessary
}

avdark_cache_core * avdc_new(avdc_size_t size, avdc_block_size_t block_size,
         avdc_assoc_t assoc, avdc_region_t region_size) {
    avdark_cache_core *core;

    core = AVDC_MALLOC(1, avdark_cache_core);

    memset(core, 0, sizeof(*core));

    if (!avdc_resize(core, size, block_size, assoc, region_size)) {
        AVDC_FREE(core);
        return NULL;
    }

    return core;
}

avdc_cache * avdc_new_multi_core(avdc_size_t size, avdc_block_size_t block_size,
                    avdc_assoc_t assoc, avdc_region_t region_size) {
    //assert(assoc == ASSOC);
    //assert(region_size == REGION_SIZE);

    avdc_cache *cache;

    cache = AVDC_MALLOC(1, avdc_cache);

    memset(cache, 0, sizeof(*cache));

    cache->dbg = 0;


    for (int i = 0; i < CORES; i++) {
        cache->cores[i] = avdc_new(32768, 64, MD1_ASSOC, REGION_SIZE);
    }

    cache->l3_size = size * 128; //TODO-fix: 128 times bigger is the spec
    cache->md3_assoc = MD3_ASSOC; //todo: make dynamic
    cache->l3_assoc = L3_ASSOC;
    cache->accesses = 0;
    cache->md3_size = MD3_SIZE;
    cache->block_size = block_size;
    cache->l3_sets = (cache->l3_size / cache->block_size) / cache->l3_assoc;

    if (cache->l3)
        AVDC_FREE(cache->l3);
    cache->l3 = (avdc_cache_line_t **) AVDC_MALLOC(cache->l3_sets, avdc_cache_line_t*);
    for (int i = 0; i < cache->l3_sets; i++) {
        cache->l3[i] = (avdc_cache_line_t *) AVDC_MALLOC(cache->l3_assoc, avdc_cache_line_t);
    }

    if (cache->md3)
        AVDC_FREE(cache->md3);
    cache->md3 = (avdc_md3_t **) AVDC_MALLOC(cache->md3_size, avdc_md3_t*);
    for (int i = 0; i < cache->md3_size; i++) {
        cache->md3[i] = (avdc_md3_t *) AVDC_MALLOC(cache->l3_assoc, avdc_md3_t);
    }

    //avdc_flush_cache_full(cache);

    return cache;
}


void
avdc_delete(avdark_cache_core *self) {
    if (self->l1) {
        for (int i = 0; i < self->number_of_l1_sets; i++) {
            if (self->l1[i]) {
                AVDC_FREE(self->l1[i]); //TODO: fix this line blowing up
            }
        }
        AVDC_FREE(self->l1);
    }

    if (self->l2) {
        for (int i = 0; i < self->number_of_l2_sets; i++) {
            if (self->l2[i]) {
                AVDC_FREE(self->l2[i]); //TODO: fix this line blowing up
            }
        }
        AVDC_FREE(self->l2);
    }

    // free md1 and md2 like this
    if (self->md1) {
        for (int i = 0; i < self->md1_size; i++) {
            if (self->md1[i]) {
                AVDC_FREE(self->md1[i]);
            }
        }
        AVDC_FREE(self->md1);
    }

    if (self->md2) {
        for (int i = 0; i < self->md2_size; i++) {
            if (self->md2[i])
                AVDC_FREE(self->md2[i]);
        }
        AVDC_FREE(self->md2);
    }

    AVDC_FREE(self);
}

void
multi_core_delete(avdc_cache *self) {
    //delete all cores
    for (int i = 0; i < CORES; i++) { //TODO-fix there shouldn't be a -1 here but it is sending a really annoying error
        avdc_delete(self->cores[i]);
    }

    if (self->l3)
        AVDC_FREE(self->l3);

    // free md3
    if (self->md3) {
        for (int i = 0; i < self->md3_size; i++) {
            if (self->md3[i]) {
                AVDC_FREE(self->md3[i]);
            }
        }
        AVDC_FREE(self->md3);
    }

    AVDC_FREE(self);
}
