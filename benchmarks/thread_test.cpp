#include <sched.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <thread>         // std::thread
#include <mutex>          // std::mutex

using namespace std;
mutex mtx;           // mutex for critical section

#define MAXTHREADS 3
<<<<<<< HEAD
#define REPEATS 10
#define DATA_SIZE 524288*2
#define STRIDE 256
=======
#define REPEATS 2
#define DATA_SIZE 1048576*4

#define STRIDE 8
>>>>>>> temp

//#define BYTE_PAYLOAD 512
//#define DATA_SIZE 524288 //TODO: goal 1048576 4194304

int data[DATA_SIZE];

int start(int id) {

    //int data[DATA_SIZE];
    int dummy;
    mtx.lock();

    //cout << "thread: " << id << endl;
    for(int times = 0; times < REPEATS; times++) {
        for (int i = 0; i < DATA_SIZE; i = i + STRIDE) {
          //use stride to hit the same indexes
          //dummy = (int*) malloc(BYTE_PAYLOAD);
          dummy = data[i];
        }
    }
    mtx.unlock();

    return 0;
}

int main()
{
    std::thread threads[MAXTHREADS];
    int i;

    cout << "Threads: \t" << MAXTHREADS <<  endl;
    cout << "Data size:  \t" << DATA_SIZE << endl;
    cout << "Repeats: \t" <<  REPEATS  << endl;
    cout << "Stride: \t" <<  STRIDE  << endl;

    for (i = 0; i< MAXTHREADS; i++)
        threads[i] = thread(start,i+1);

    for (i = 0; i< MAXTHREADS; i++)
        threads[i].join();

    cout << "All threads joined" << endl;
    return 0;
}
