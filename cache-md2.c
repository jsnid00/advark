//
// Created by jsnider on 6/4/17.
//

#include "avdark-cache.h"

int search_md2(avdc_cache *cache, int thread_id, avdc_pa_t va, type_t type) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    int region_num = region_from_pa(core, va);
    int md2_index = md2_index_from_pa(core, va);
    int l1_index = l1_index_from_pa(core, va);
    int l2_index = l2_index_from_pa(core, va);
    int md2_way;
    int l1_way;

    for (md2_way = 0; md2_way < MD2_ASSOC; md2_way++) {

        avdc_md_t *md2 = &core->md2[md2_index][md2_way];

        if ((md2->tag == tag) &&
            (md2->valid == 1) &&
            (md2->active == 1)) {

                int li = md2->li[region_num];
                assert_addr(&md2->address[region_num], va, 13);
                core->stat.md2_hit += 1;

                if (check_l1(li)) {

                    if(type == AVDC_READ || md2->private_bit == 1) { //reads are always safe and if it's private it doesn't matter

                        //TODO-done: add this back in somehow
                        //assert(md2->private_bit==1);

                        //update internal statistics:
                        core->stat.md2_l1_hit += 1;

                        l1_way = li & (L1_BASE - 1);

                        avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
                        assert(l1->active == 1);
                        assert(l1->valid == 1);
                        assert_addr(&l1->address, va, 14);
                    } else if(type == AVDC_WRITE && md2->private_bit == 0) { //then you have to go to md3
                        return 0;
                    }
                } else if (check_l2(li)) {

                    if(type == AVDC_READ || md2->private_bit == 1) { //reads are always safe and if it's private it doesn't matter

                        //TODO-done: add this back in somehow
                        //assert(md2->private_bit==1);

                        //update internal statistics:
                        core->stat.md2_l2_hit += 1;

                        int l2_way = li & (L2_BASE - 1);

                        avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];
                        assert(l2->active == 1);
                        assert(l2->valid == 1);
                        assert_addr(&l2->address, va, 15);

                        //todo-move to l1
                        copy_l2_to_l1(cache, thread_id, va, 2); //test this

                    } else if(type == AVDC_WRITE && md2->private_bit == 0) { //then you have to go to md3
                        return 0;
                    }

                } else if (check_mem(li)) {

                    //update internal statistics:
                    core->stat.md2_mem_hit += 1;

                    if(md2->private_bit ==1) {
                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                        //copy_md_to_md3(cache, thread_id, va); //test this

                    }else {
                        //TODO
                        //find master location
                        if(type == AVDC_READ) {
                            //TODO-done: don't access md3 go straight to the sorce
                            //install untracked data into md1 and md2

                            //TODO-question-erik: hits from memory can just beadded to the region yeah?
                            //copy_from_remote_node(cache, thread_id, li, va);
                            avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);

                            //TODO: and update MD3
                            copy_md_to_md3(cache, thread_id, va); //test this


                        } else if(type == AVDC_WRITE) {
                            //FROMERIK: invalidate only the cache line on write
                            //invalidate
                            invalidate_cachline(cache, thread_id, va); //TODO: test this
                            avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                        }
                    }

                } else if (check_rem(li)) {

                    assert(md2->private_bit==0);
                    core->stat.md2_rem_hit += 1;

                    if(type == AVDC_READ) {
                        copy_from_remote_node(cache, thread_id, li, va);
                    } else if(type == AVDC_WRITE) {
                        //invalidate
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                    }
                } else if (check_l3(li)) {

                    //TODO: assert data is in l3
                    //update internal statistics:
                    core->stat.md2_l3_hit += 1;

                    int l3_index = l3_index_from_pa(cache, va);
                    int l3_way = li & (L3_BASE - 1);

                    avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];
                    assert(l3->active == 1);
                    assert(l3->valid == 1);
                    assert_addr(&l3->address, va, 16); //TODO-fix errors.. errors everywhere prio-1

                    //insert data into requesting node
                    if (md2->private_bit == 1) {
                        //if in memory and region is private add to region - addendum might not have to be private, no maybe should be, not sure
                        //TODO: check if this returns 2
                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                    } else {
                        //TODO
                        //find master location

                        if(type == AVDC_READ) {
                            //TODO-done: don't access md3 go straight to the source
                            avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way); //this kinda worked
                            copy_md_to_md3(cache, thread_id, va); //test this

                        } else if(type == AVDC_WRITE) {
                            //FROMERIK: invalidate only the cache line on write
                            invalidate_cachline(cache, thread_id, va); //TODO: test this
                            avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                        }
                    }
                } else {
                    printf("***err: severe error in li encoding md2\n");
                    exit(EXIT_FAILURE);
                }

                //because we're in md2 is we have a hit here we copy the entry to md1 everytime
                int result = copy_md2_to_md1(core, va, md2_way);

                return result;

        }
    }
    //core->stat.md2_mem_miss += 1;
    return 0;
}

int search_md2_way(avdc_cache *cache, avdark_cache_core *core, avdc_pa_t va) {

    int md2_index = md2_index_from_pa(core, va);
    avdc_pa_t tag = tag_from_pa(core, va);


    int i;
    for (i = 0; i < MD2_ASSOC; i++) {
        if (core->md2[md2_index][i].tag == tag && // TODO test this does it always match
            core->md2[md2_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_md2_way(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];

    int md2_index = md2_index_from_pa(core, va);
    avdc_pa_t tag = tag_from_pa(core, va);
    int md1_index = md1_index_from_pa(core, va);

    //TODO: remove this part replace with search md2
    //if entry exists return it's index
    int i;
    for (i = 0; i < MD2_ASSOC; i++) {
        if (core->md2[md2_index][i].tag == tag &&
            core->md2[md2_index][i].valid == 1) {
            return i;
        }
    }

    //find an entry that isn't valid yet use that one
    for (i = 0; i < MD2_ASSOC; i++) {
        if (core->md2[md2_index][i].valid == 0) {
            return i;
        }
    }

    // find the inactive md2 region with the smallest time stamp
    int LRU_way = 0;
    double diff_time = 0;
    for (i = 1; i < MD2_ASSOC; i++) {
        diff_time = difftime(core->md2[md2_index][LRU_way].timestamp,
                             core->md2[md2_index][i].timestamp);
        if (diff_time > 0)
            LRU_way = i;
    }

    //at this point LRU is the right way to evict
    //TODO: make this it's own function
    avdc_pa_t LRU_pa = core->md2[md2_index][LRU_way].address[0]; //default use first address in region
    int region_num = region_from_pa(core, LRU_pa);
    //int LRU_li = core->md2[md2_index][LRU_way].li[region_num];

    int LRU_md2_index = md2_index_from_pa(core, LRU_pa);
    int LRU_md1_index = md1_index_from_pa(core, LRU_pa);
    int LRU_md1_way = -1;
    int LRU_md1_way_test = -1;
    int LRU_md1_match = -1;

    avdc_md_t * LRU_md2 = &core->md2[LRU_md2_index][LRU_way];

    //search for active md1 entry, has to match on addr!
    LRU_md1_way = LRU_md2->tracker_pointer;

    //search to test if md1 exists
    for (int g = 0; g < MD1_ASSOC; g++) {
        for (int j = 0; j < REGION_SIZE; j++) {
            if (core->md1[LRU_md1_index][g].tag == LRU_md2->tag &&
                core->md1[md1_index][LRU_md1_way].valid == 1) {
                LRU_md1_match = 1;
                LRU_md1_way_test = g;
                break;
            }
        }
    }

    //copy valid MD to md2
    assert(LRU_md1_way_test == LRU_md1_way);
    if (LRU_md1_match == 1) {
        assert((int) core->md1[md1_index][LRU_md1_way].address[region_num] <= (int) LRU_pa);
        assert((int) LRU_pa <= (int) core->md1[md1_index][LRU_md1_way].address[region_num] + BLOCK_SIZE);

        assert_addr(&LRU_md2->address[region_num], LRU_pa, 17);

        avdc_md_t *LRU_md1 = &core->md1[LRU_md1_index][LRU_md1_way];

        //copy md1 data to md2
        if (LRU_md1->active && LRU_md1->valid) {

            copy_md1_to_md2(core, va, md1_index, LRU_md1_way);

        }
    }

    //remove data from cache
    //find active
    assert(LRU_md2->active == 1);

    //find the md3 entry
    int md3_index = md3_index_from_pa(cache,LRU_pa);
    avdc_pa_t LRU_tag = tag_from_pa(core, LRU_pa);

    int md3_way = -1;
    for(int i =0; i<cache->md3_assoc; i++)
    {
        if(cache->md3[md3_index][i].tag == LRU_tag) {
            md3_way = i;
            break;
        }
    }
    assert(md3_way != -1);

    avdc_md3_t * LRU_md3 = &cache->md3[md3_index][md3_way];

    //TODO-question this is how this works? : take each piece of data from the evicted md2 and move it to l3
    for (i = 0; i < REGION_SIZE; i++) {
        //TODO-done add to l3
        avdc_pa_t evict_pa = LRU_md2->address[i]; //default use first address in region
        int evict_li = LRU_md2->li[i];
        int l1_index = l1_index_from_pa(core, evict_pa);
        int l2_index = l2_index_from_pa(core, evict_pa);

        //because memory is 011000

        if (check_l1(evict_li)) {//if in L1
            int evict_li_way = evict_li & (L1_BASE - 1);

            assert_addr(&core->l1[l1_index][evict_li_way].address, evict_pa, 18); //bug fix

            //TODO-done: add to l3 before removing
            add_to_l3(cache, evict_pa);

            clear_l1_entry(core, l1_index, evict_li_way);

            //also need to clear md2 (spend 3 hours on this bug)
            clear_l2_entry_va(core, evict_pa);

        } else if (check_l2(evict_li)) { //if in L2
            int evict_li_way = evict_li & (L2_BASE - 1);
            //core->l2[l2_index][li].valid = 0;

            assert_addr(&core->l2[l2_index][evict_li_way].address, evict_pa, 19); //bug fix

            add_to_l3(cache, evict_pa);

            clear_l2_entry(core, l2_index, evict_li_way);
        } else if (check_mem(evict_li)) { //if in mem
            //no-op
            //TODO-done: remove for production
            ///added for testing
            //add_to_l3(cache, evict_pa, LRU_md3);

        } else if(check_l3(evict_li)) {
            //TODO: maybe send an error
        }
    }
    //this is the damn line i spent a half hour looking for.. gosh
    //LRU_md3->pb = 0;

    LRU_md3->pb = remove_pb(LRU_md3->pb, thread_id);

    clear_md2(core, md2_index, LRU_way);

    return LRU_way;

}

//TODO: add asserts that check that only one md entry is active
int copy_md2_to_md1(avdark_cache_core *core, avdc_pa_t va, int md2_way) {

    avdc_pa_t tag = tag_from_pa(core, va);
    int region_num = region_from_pa(core, va);
    int md2_index = md2_index_from_pa(core, va);
    int md1_index = md1_index_from_pa(core, va);

    //move md2 entry to md1
    int md1_way = -1;
    int md1_way_assert = core->md2[md2_index][md2_way].tracker_pointer;
    for (int j = 0; j < MD1_ASSOC; j++) {
        avdc_pa_t tag_md1 = core->md1[md1_index][j].tag;
        int active = core->md1[md1_index][j].active;

        if (tag_md1 == tag && //TODO-done: test changed to go off the tag not the addr
            active == 1) {
                md1_way = j;
        }
    }

    //TODO: pull common lines down to remove some duplicated code
    avdc_md_t *md2, *md1;

    if (md1_way != -1) {
        //if the md1 entry exists just update it
        assert(md1_way == md1_way_assert);

        md2 = &core->md2[md2_index][md2_way];


        //md1 = md2;
        //TODO:test case write a test case for this
        core->md1[md1_index][md1_way] = *md2; //content of md2 copy to md1
        md1 = &core->md1[md1_index][md1_way];
        md1->timestamp = clock();
        md2->active = 0;
        md1->tracker_pointer = md2_way; //this is new
        md2->tracker_pointer = md1_way; //this is new

    } else {
        //if it doesn't exist create an md1 entry
        md1_way = find_md1_way(core, va); //TODO: be sure it's new

        md2 = &core->md2[md2_index][md2_way];
        md1 = &core->md1[md1_index][md1_way];

        ///I think these asserts are a good idea to make sure the md1 is new
        assert(md1->active == 0);
        assert(md1->valid == 0);
        assert(md1->tag == 0);

        ////**info: CREATE md1 entry from md2 entry
        for (int i = 0; i < REGION_SIZE; i++) {
            md1->li[i] = md2->li[i];
            md1->address[i] = md2->address[i];
        }

        assert(md2->valid == 1);
        assert(md2->tag == tag);
        assert(md2->active == 1);

        md1->valid = 1;
        md1->tag = tag;
        md1->active = 1;
        md1->private_bit = md2->private_bit; //i think this was the line //oh this was a very bad line

        md1->tracker_pointer = md2_way;
        md2->tracker_pointer = md1_way;

        assert_addr(&md2->address[region_num], va, 20);
        md1->timestamp = clock();
        md2->active = 0;
    }

    assert(md1->active != md2->active);


    return 1;
}


///tested once changes li in md3 to 32
int add_to_l3(avdc_cache *cache, avdc_pa_t va) {

    int md3_index = md3_index_from_pa(cache, va);
    int md3_way = search_md3_way(cache, va);
    if(md3_way == -1) { printf("error in add to l3 type"); exit(1) ; }
    avdc_md3_t * LRU_md3 = &cache->md3[md3_index][md3_way]; //TODO: test this

    int l3_way = find_l3_way(cache, va);
    int l3_index = l3_index_from_pa(cache, va);

    int region_num = region_from_pa(cache->cores[0], va);

    avdc_cache_line_t * l3 = &cache->l3[l3_index][l3_way];

    l3->valid = 1;
    l3->address = va;
    l3->active = 1;
    l3->timestamp = clock();
    l3->tracker_pointer = md3_way; //TODO-done: also should fix this
    l3->replacement_pointer = -1; //TODO: fix this, pretty sure it should be null though

    LRU_md3->li[region_num] = L3_BASE + l3_way;

    return l3_way;

}


void clear_md2(avdark_cache_core *core, int md2_index, int md2_way) {

    avdc_md_t  *md2 = &core->md2[md2_index][md2_way];

    if(md2->active == 0 && md2->valid ==1) {
        //then there is an md1 entry that has to be delted right?
        printf("clear an md1\n");
        exit(1);
    }

    if(md2->active && md2->valid) {

        for(int i = 0; i < REGION_SIZE; i++){
            int li = md2->li[i];
            avdc_pa_t va = md2->address[i];

            if(check_l1(li)){
                clear_l1_entry_va(core,va);
            } else if (check_l2(li)) {
                clear_l2_entry_va(core, va);
            } else if (check_l3(li)) {
                //don't need to do anything i don't think
                //maybe notify md3
            } else if (check_mem(li)) {
                //don't do anything
            } else if (check_rem(li)) {
                //maybe send a notification to the remote node or to md3
                //to be removed from the pb
            } else {
                printf("error in li encosing 3212\n");
                exit(1);
            }

        }
    }

    md2->valid = 0;
    md2->active = 0;
    md2->tracker_pointer = -1;
    md2->tag = 0;
    md2->timestamp = 0;
    md2->private_bit = 0;

    //TODO: delete the data


    for (int g = 0; g < REGION_SIZE; g++) {
        md2->address[g] = 0;
        md2->li[g] = 0;
    }

}