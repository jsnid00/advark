[PARSEC] Benchmarks to run:  parsec.fluidanimate

[PARSEC] [========== Running benchmark parsec.fluidanimate [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simsmall'.
in_35K.fluid
[PARSEC] Running '/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/16_d2m_pin.so -o /home/josn3503/advark/stats/fluidanimate_16_small_stats.txt --  /it/slask/student/josn3503/parsec-3.0/pkgs/apps/fluidanimate/inst/amd64-linux.gcc/bin/fluidanimate 1 5 in_35K.fluid out.fluid':
[PARSEC] [---------- Beginning of output ----------]
PARSEC Benchmark Suite Version 3.0-beta-20150206
Loading file "in_35K.fluid"...
Number of cells: 36900
Grids steps over x, y, z: 0.00433333 0.00439024 0.00433333
Number of particles: 36504
Saving file "out.fluid"...
block size:	 64
region size:	 16
l1 ASSOC:	 8
md1 ASSOC:	 8
md2 ASSOC:	 8
md3 ASSOC:	 8
***info: L1 CAP
***info: L2 CAP
***info: MD1 CAP
***info: L3 CAP
***info: MD2 CAP
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.
