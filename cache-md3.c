//
// Created by jsnider on 6/10/17.
//

#include "avdark-cache.h"

int search_md3(avdc_cache *cache, avdc_pa_t pa, int thread_id, type_t type) {

    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa);
    int region_num = region_from_pa(cache->cores[0], pa);
    int md3_index = md3_index_from_pa(cache, pa);

    int md3_way;
    for(md3_way = 0; md3_way < MD3_ASSOC; md3_way++) {

        avdc_md3_t * md3 = &cache->md3[md3_index][md3_way];

        if((md3->tag == tag) &&
           (md3->valid == 1) &&
           (md3->active == 1)) {

            cache->stat3.md3_hit += 1;
            assert_addr( &md3->address[region_num], pa, 5);

            if(md3->pb == 0) {
                ///CASE D.1 untracked to private
                //TODO: this is not D.1 pb being zero means that is no data in nodes


                //TODO: check if it's in memory or if it's in l3

                int li = md3->li[region_num];
                if(check_mem(li)){



                    /*
                     * TODO: do something to test this
                     * this has never worked so it's not tested at all
                     */

                    //TODO-done: update md3_mem_hit
                    cache->stat3.md3_mem_hit += 1;

                    //install untracked data into md1 and md2
                    int test_md1_index = avdc_insert(cache, thread_id, pa);

                    //TODO-done: set pb bits
                    md3->timestamp = clock();
                    md3->pb = set_pb(thread_id); //TODO-done: TEST this

                    //TODO: remove duplicate code
                    avdark_cache_core * core = cache->cores[thread_id];
                    int md1_index = md1_index_from_pa(core,pa);
                    int md2_index = md2_index_from_pa(core,pa);
                    int md1_way = search_md1_way(core, pa);
                    int md2_way = search_md2_way(cache, core, pa);
                    avdc_md_t * md1 = &core->md1[md1_index][md1_way];
                    avdc_md_t * md2 = &core->md2[md2_index][md2_way];
                    md1->private_bit=1;
                    md2->private_bit=1;

                    //because we can
                    assert(test_md1_index == md1_index);
                    return 1;

                } else if (check_l3(li)) {

                    //TODO-done: update md3_l3_hit
                    cache->stat3.md3_l3_hit += 1;


                    /*
                     * TODO: do something to test this
                     * this has never worked so it's not tested at all
                     */


//                    //find an open md1
//                    avdark_cache_core *core = cache->cores[thread_id];
//                    int md1_index = md1_index_from_pa(core, pa);
//                    int md1_way = find_md1_way(core, pa);
//                    assert(md1_way != -1);
//                    avdc_md_t *md1 = &core->md1[md1_index][md1_way];


                    //TODO: remove duplicate code
                    int l3_index = l3_index_from_pa(cache, pa);
                    int l3_way = li & (L3_BASE - 1);

                    avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];
                    assert(l3->active == 1);
                    assert(l3->valid == 1);

                    //validate entry
                    assert_addr(&l3->address, pa, 40); //TODO: fix this it errors sometimes

                    int result = avdc_insert_from_md3(cache, thread_id, pa);
                    assert(result != -1);

                    //TODO:remove l3 entry
                    clear_l3_entry(cache, l3_index, l3_way);

                    //TODO-done: set thread bit, rem bit
                    ///should be 3
                    md3->pb = set_pb(thread_id) ; //TODO: test this

                    return 1;

                } else {
                    printf("data not in memory or l3\n");
                    exit(1);
                }





            }  else if (number_of_pb(md3->pb) == 1 ) { //TODO-done: done this needs to check if any pb is set, 1 2, 3, 4, 5, 6

                int pb = md3->pb;
                int rem_thread_id = get_thread(pb);

                if(type == AVDC_READ) {
                    //CASE D.2: from paper READ miss, MD miss
                    //memory is in a remote node
                    cache->stat3.md3_rem_hit += 1;

                    copy_md_to_md3(cache, rem_thread_id, pa); //make sure this still works, tested still works
                    //install untracked data into md1 and md2
                    avdc_insert_from_md3(cache, thread_id, pa);

                    //TODO-done: set thread bit, rem bit
                    ///should be 3
                    md3->pb = set_pb(thread_id) ^
                              set_pb(rem_thread_id); //TODO-done: this needs to be both the calling core and callee core

                    //TODO-done: copy md from rem thread to md3
                    //TODO-done: clear remote private bits
                    //TODO-done: change md3 LI to point to rem_node
                    //TODO-done: send md to calling thread with LI for the access set to that node

                    return 1; //TODO-done oh this is super hacky, there is an entry but not in this node
                } else if (type == AVDC_WRITE) {
                    //TODO: block md3 region

                    //invalidate all slave cachlines and set LI to req node_id
                    invalidate_cachline(cache, thread_id, pa);

                    //add the data
                    avdc_insert_from_md3(cache, thread_id, pa); //TODO: need to test!!

                    md3->pb = set_pb(thread_id) ^
                              set_pb(rem_thread_id);

                    cache->stat3.md3_rem_hit += 1;

                    return 1;
                }

            } else if (md3->pb > 1) { //TODO-done: test this I think this works becasue if pb is bigger than 1 then it's shared

                if(type == AVDC_READ) { //TODO: test this section
                //CASE D.3 read miss, md1/md2 miss
                    //#PB > 1: shared -> shared

                    //memory is in a remote node
                    //TODO: not if it's an md1/md2 hit
                    cache->stat3.md3_rem_hit += 1;

                    //install untracked data into md1 and md2
                    avdc_insert_from_md3(cache, thread_id, pa); //TODO TEST
                    md3->pb = set_pb(thread_id) ^ md3->pb;

                    return 1;

                } else if(type == AVDC_WRITE) {
                    ///Case C from the paper write
                    ///write miss, shared region, md1/md2 hit

                    //TODO: block md3 region

                    //invalidate all slave cachlines and set LI to req node_id
                    invalidate_cachline(cache, thread_id, pa);

                    //add the data
                    avdc_insert_from_md3(cache, thread_id, pa); //TODO: need to test!!
                    md3->pb = set_pb(thread_id) ^ md3->pb;
                    cache->stat3.md3_rem_hit += 1;

                    return 1;
                } else {printf("error in AVDC type"); exit(1);}

            }else {printf("***err: severe error in li encoding md3\n"); exit(EXIT_FAILURE);}
        }
    }
    cache->cores[thread_id]->stat.md2_mem_miss += 1;
    return 0;
}

/*
 * okay this looks through the bit pattern and determines if only one core is set
 * TODO-done: test this
 * TODO-done: add error result for case 0
 */
int number_of_pb(int pb){
    if(pb==0)
        return 0;
    int flag = 0;
    for(int i = 0; i<CORES; i++) {

        int result = pow((double)2, i);
        if(result & pb) {
            if (flag == 0) {
                flag = 1;
            } else {
                return 0;
            }
        }
    }
    return 1;
}

int search_md3_way(avdc_cache *cache, avdc_pa_t pa) {

    int md3_index = md3_index_from_pa(cache, pa);
    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa);

    int i;
    for (i = 0; i < MD3_ASSOC; i++) {
        if (cache->md3[md3_index][i].tag == tag &&
            cache->md3[md3_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_md3_way(avdc_cache *cache, avdc_pa_t pa) {

    int md3_index = md3_index_from_pa(cache,pa);
    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa); //doesn't matter that it's core 0

    //TODO: remove this code from this function
    int i;
    for(i = 0; i < MD3_ASSOC; i++) {
        if(cache->md3[md3_index][i].tag == tag &&
           cache->md3[md3_index][i].valid == 1) {
            return i;
        }
    }

    //find an entry that isn't valid yet use that one

    for (int i = 0; i < MD3_ASSOC; i++) {
        if (cache->md3[md3_index][i].valid == 0) {
            //printf("-------------------found free md3 (valid) way: %d\n",i );
            return i;
        }
    }

    // find the md3 region with the smallest time stamp
    int LRU_way = 0; //found a bug this has to be zer to start with
    double diff_time = 0;
    for(i=1; i < MD3_ASSOC; i++){
        diff_time = difftime(cache->md3[md3_index][LRU_way].timestamp,
                             cache->md3[md3_index][i].timestamp);
        if(diff_time > 0)
            LRU_way = i;
    }

    avdc_md3_t * md3 = &cache->md3[md3_index][LRU_way];
    int pb = md3->pb;

    if (pb == 0) {
        //there is no data in any nodes it's all in l3 or in memory
        //TODO:add assert for this
        //evict all data out of l3
        for(int i = 0; i<MD3_ASSOC; i++){
            int li = md3->li[i];
            if(check_l3(li)) {
                avdc_pa_t evict_va = md3->address[i];
                int l3_index = l3_index_from_pa(cache, evict_va);
                int l3_way = search_l3_way(cache, evict_va);
                assert(l3_way != -1);
                clear_l3_entry(cache, l3_index, l3_way);
            } else if (check_mem(li)) {
                //no-op
                //it's in memory what you gonna do

            }
        }
    } else if (pb >= 1) {
        //data is found in one or multiple cores
        // needs to be removed from md1, md2, l1 and l2

        //TODO-done implement for multiple cores
        //this can return -1

        //avdark_cache_core *core = cache->cores[thread_id];

        //core->m

        //TODO-done: all this stuff
        //find md1 index md2 index
        //find md1 way and md2 way
        //delete all data from l1 and l2
        //remove md1 and md2

        //loop through the regions and evict every cacheline from every core
        for(int j = 0; j<REGION_SIZE; j++ ) {
            invalidate_cachline(cache,-1, md3->address[j]);
        }
    }

    clear_md3(cache, md3_index, LRU_way);

    return LRU_way;

}

/*
 * In this function we install a md entry into the md3
 * And we will call avdc_insert(self, self->cores[thread_id], pa);
 * to add entries in md1 and md2 and l1 and l2
 *
 * covers case D.1 untracked -> private
 *
 */

int avdc_insert_md3(avdc_cache *cache, avdc_pa_t pa, int thread_id, type_t type){

    avdark_cache_core * core = cache->cores[thread_id];
    avdc_pa_t tag = tag_from_pa(core, pa);
    avdc_pa_t base_pa = pa_from_tag(core, tag);
    //int region_num = region_from_pa(core, pa);
    int md1_index = md1_index_from_pa(core,pa);

    //test to see if we need to inset entries into md1, md2, l1 and l2
    int test_md1_index;

    //here the data is made for md1 and md2
    test_md1_index = avdc_insert(cache, thread_id, pa);
    assert(test_md1_index == md1_index);

    int md1_way = search_md1_way(core, pa);
    if(md1_way == -1) {printf("error in avdc_insert_md3 type"); exit(1);}

    int md2_index = md2_index_from_pa(core,pa);
    int md2_way = search_md2_way(cache, core, pa);
    if(md2_way == -1) {printf("error in avdc_insert_md3 type: kod 234"); exit(1);}


    int md3_index = md3_index_from_pa(cache,pa);
    int md3_way = find_md3_way(cache, pa);

    avdc_md3_t * md3 = &cache->md3[md3_index][md3_way];

    //check if md3 is active and valid
    if(md3->valid == 1 && md3->active ==1){
        //don't need to do anything i think
        printf("error tolken: md3 being overwritten 4543"); // 1
        exit(1); /// i think this is an error, if it already exists it should be found before

    } else {
        //create new md3
        for (int i = 0; i < REGION_SIZE; i++) {
            md3->li[i] = MEM_BASE;
            md3->address[i] = base_pa + (core->block_size * i);
        }

        md3->valid = 1;
        md3->tag = tag;
        md3->active = 1;
    }

    avdc_md_t * md1 = &core->md1[md1_index][md1_way];
    avdc_md_t * md2 = &core->md2[md2_index][md2_way];

    //assert(!cache->md3[md3_index][md3_way].valid && !cache->md3[md3_index][md3_way].active);
    //assert(!md3->valid && !md3->active);

    //TODO: this assert should be included but fails
    //assert(md3->pb==0); //TODO:question pb should be invalidated everywhere i think, as of now

    int pb = set_pb(thread_id);
    md3->pb = md3->pb | pb; //add the thread id to the bit pattern

    //if the md3->pb is set only to the calling thread
    if(md3->pb == pb) {
        //then set the md1 and md2 pb to 1
        md1->private_bit=1;
        md2->private_bit=1;
    } else {
        printf("error tolken 1298");

        exit(1); ///it should always be private here
    }
    cache->stat3.md3_mem_miss += 1;

    //TODO: should this really happen here?
    //TODO-fix: i think this line was incorrect and causing problems
    //md3->li[region_num] = thread_id;
    md3->timestamp = clock();

    return 0;

}

/*
 * this function takes a thread id
 * and return bit position for that thread set to 1
 */

int set_pb(int thread_id){
    if (thread_id >= 0 && thread_id <= CORES)
        return pow(2, thread_id);
    else {
        printf("err*** set pb boom");
        exit(EXIT_FAILURE);
    }
}

/*
 * get a pb bit pattern and return the one thread that is bas
 */
int get_thread(int pb){
    if(pb > 0)
        return log2((double)pb);
    else
        return -1;
}

int remove_pb(int pb, int thread_id){
    assert(pb > 0);
    return pb ^ set_pb(thread_id);
}


void clear_md3(avdc_cache *cache, int md3_index, int md3_way){

    avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];

    if(md3->active == 0 && md3->valid == 1) {
        //then there is an md3 entry that has to be delted right?
        printf("clear an md3\n");
        exit(1);
    }

    if(md3->active && md3->valid) {

        for(int i = 0; i < REGION_SIZE; i++){
            int li = md3->li[i];
            avdc_pa_t va = md3->address[i];

            if(check_l1(li)){
                printf("error in li encoding l1 5435\n");
                exit(1);
            } else if (check_l2(li)) {
                printf("error in li encoding l2 5535\n");
                exit(1);
            } else if (check_l3(li)) {
                clear_l3_entry_va(cache, va);

            } else if (check_mem(li)) {
                //don't do anything
            } else if (check_rem(li)) {
                //maybe send a notification to the remote node or to md3
                //to be removed from the pb
            } else {
                printf("error in li encosing 7212\n");
                exit(1);
            }

        }
    }

    cache->md3[md3_index][md3_way].valid = 0;
    cache->md3[md3_index][md3_way].active = 0;
    cache->md3[md3_index][md3_way].tag = 0;
    cache->md3[md3_index][md3_way].pb = 0;

    //TODO: delte all the data

    for(int g=0; g<REGION_SIZE; g++){

        cache->md3[md3_index][md3_way].address[g] = 0;
        cache->md3[md3_index][md3_way].li[g] = 0;
    }

}


/*
 * used to copy active md entry to md3 entry
 * */

void copy_md_to_md3(avdc_cache *cache, int rem_thread_id, avdc_pa_t pa){

    avdark_cache_core * rem_core = cache->cores[rem_thread_id];

    int md3_index = md3_index_from_pa(cache,pa);
    int md2_index = md2_index_from_pa(rem_core,pa);
    int md1_index = md1_index_from_pa(rem_core,pa);

    avdc_pa_t tag = tag_from_pa(cache->cores[rem_thread_id], pa);
    int md3_way = search_md3_way(cache, pa);

    int md2_way = search_md2_way(cache, rem_core, pa);
    int md1_way = search_md1_way(rem_core, pa);
    //assert(md1_way != -1); //sometimes there is no md1
    assert(md2_way != -1); //i think there has to be an md2_directory here

    int md3_way_test = -1;
    for(int i =0; i<cache->md3_assoc; i++)
    {
        if(cache->md3[md3_index][i].tag == tag) {
            md3_way_test = i;
            break;
        }
    }
    assert(md3_way != -1);
    assert(md3_way == md3_way_test);

    avdc_md_t * rem_md2 = &rem_core->md2[md2_index][md2_way];
    avdc_md_t * rem_active_md;
    if(md1_way != -1) {
        avdc_md_t * rem_md1 = &rem_core->md1[md1_index][md1_way];
        if(rem_md1->active) {
            rem_active_md = rem_md1;
        } else if (rem_md2->active) {
            rem_active_md = rem_md2;
        } else {
            printf("error in md active tolken 1292");
            exit(1);
        }
    } else {
        rem_active_md = rem_md2;
    }


    avdc_md3_t * md3 = &cache->md3[md3_index][md3_way];

    rem_active_md->private_bit = 0; //TODO-doing test this, tested once

    md3->active = rem_active_md->active;
    md3->tag = rem_active_md->tag; //TODO-done yes: test: is this right?
    md3->valid = rem_active_md->valid;
    md3->timestamp = clock();

    assert(md3->active == 1 && md3->valid == 1); //TODO: don't use implict true bool value check for equality to 1

    /*from paper
     * It clears its private bit and sends8 metadata to MD3;
     * LIs pointing to local location in the slave node (e.g. in L1D) are converted to point to the slave NodeID
     * and stored in MD3 metadata;
     */

    for(int j=0; j<REGION_SIZE; j++){
        md3->address[j] = rem_active_md->address[j];

        //TODO-doing: add l3 when it comes - don't thin kl3 needs to be here
        int li = rem_active_md->li[j];

        if(check_l1(li) || check_l2(li) ){
            md3->li[j] = rem_thread_id;
        } else {
            md3->li[j] = rem_active_md->li[j];
        }
    }

}
