//
// Created by Johan Snider on 2017-10-25.
//

/**
 * Cache simulator test case -
 *
 * Course: Advanced Computer Architecture, Uppsala University
 * Course Part: Lab assignment 1
 *
 * Author: Andreas Sandberg <andreas.sandberg@it.uu.se>
 * Revision (2015, 2016) German Ceballos
 */

#include "d2m_cache.h"
#include <memory.h>

#define ASSOC 8

//TODO: shouldn't all be testing on core 0
#define STAT_ASSERT(c, r, rm, w, wm) do {                       \
                assert(c->cores[0]->stat.data_read_miss == (rm));         \
                assert(c->cores[0]->stat.data_write == (w));              \
                assert(c->cores[0]->stat.data_write_miss == (wm));        \
        } while (0)

#define TEST_SIMPLE_STAT() \
        if (type == AVDC_READ)                                  \
                STAT_ASSERT(cache, hits+misses, misses, 0, 0);  \
        else if (type == AVDC_WRITE)                            \
                STAT_ASSERT(cache, 0, 0, hits+misses, misses);  \
        else                                                    \
                abort()






static void sim_ls(avdc_cache *cache) {

    FILE * fp;

    fp = fopen("/Users/jsnider/advark/trace.out", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    int j = 0;
    avdc_pa_t va;

    char line[256];
    char *type_char;
    char *va_char;
    char *id_char;
    int id;

    while (fgets(line, sizeof(line), fp) && strcmp(line, "#eof\n")) {

        type_char = strtok(line, " ");
        va_char = strtok(NULL, " ");
        id_char = strtok(NULL, " ");
        strtok(id_char, "\n");

        j++;



        va = strtoll(va_char, NULL, 0);
        id = atoi(id_char);

        int region_num = region_from_pa(cache->cores[0], va);
        avdc_pa_t tag = tag_from_pa(cache->cores[0], va);

        if(tag == 4207140) {
           // printf("j: %d\n", j);
        }
        if(j == 7538666) {
           // printf("break\n");
        }

        if(*type_char == 'R')
            avdc_access(cache, va, AVDC_READ, id);
        else if (*type_char == 'W')
            avdc_access(cache, va, AVDC_WRITE, id);
        else{
            printf("***err no read write info found");
            exit(1);
        }
    }

    fclose(fp);


}

static void case_e(avdc_cache *cache) {


}

static void case_a(avdc_cache *cache) {

    //TODO: add manual assert after tests

    //A: Read Miss, MD1 hit

    printf("Core 0\n");
    //address      //core
    //avdc_access(cache, 0, AVDC_READ, 0); //load region into md1 miss

    //A master location in LLC (8.9) or memory (2.7) can be accessed directly, similarly to D2D,
    //avdc_access(cache, 64, AVDC_READ, 0); // MD1 hit, l1 and l2 miss collect from memory

    //while reading a master cacheline in a remote slave node (0.8), e.g. in a remote L1, requires an indirection through that nodeâ€™s active MD1 or MD2 to determine its location in that node
    printf("Core 1\n");

    //fill up md1
    int md1_rounds = (2048 * ASSOC) + 1;
    for (int i = 0; i < BLOCK_SIZE * md1_rounds; i = i + BLOCK_SIZE) {
        avdc_access(cache, i, AVDC_READ, 1);
    }

    avdc_access(cache, 0, AVDC_READ, 0); //load region into md1 miss
    avdc_access(cache, 0, AVDC_READ, 1); //md2 hit to remote node

    //TODO-answer: question add reads all shared right? yes!
    //assert_cache(cache, 0, 0);
    //assert_cache(cache, 1, 0);

    avdc_print_statistics(cache);
    avdc_print_cache_statistics(cache);
    avdc_flush_cache_full(cache);

    //A: Read Miss, MD2 hit

    //fill up md1
    md1_rounds = (2048 * ASSOC) + 1;
    for (int i = 0; i < BLOCK_SIZE * md1_rounds; i = i + BLOCK_SIZE) {
        avdc_access(cache, i, AVDC_READ, 0);
    }

    //1019904 is the unlucky address that gets evicted when 0 gets brought back into l1

    avdc_access(cache, 0, AVDC_READ, 0); // should hit md2, read miss from memory
    avdc_access(cache, 1019904, AVDC_READ, 1); // should hit md2 remopte core, read miss from memory
    //assert_cache(cache, 0, 0);
    //assert_cache(cache, 1, 1019904);

    avdc_print_statistics(cache);
    avdc_flush_cache_full(cache);

}

static void case_d(avdc_cache *cache) {

    //Read miss, MD1 miss

    //4) MD3 miss: uncached -> private
    avdc_flush_cache_full(cache);
    avdc_access(cache, 0, AVDC_READ, 0); //first access md1 miss, md2 miss, md3 miss, loaded to be private
    assert(cache->cores[0]->md1[0][0].private_bit == 1);
    avdc_print_statistics(cache);
    avdc_print_cache_statistics(cache);
    avdc_flush_cache_full(cache);

    //2) #PB == 1: private -> shared
    avdc_access(cache, 0, AVDC_READ, 0); //first access set to private in core 0
    avdc_access(cache, 0, AVDC_READ, 1); //second access from core 1, set md->pb == 3
    assert(cache->cores[0]->md1[0][0].private_bit == 0);
    assert(cache->cores[1]->md1[0][0].private_bit == 0);
    assert(cache->md3[0][0].pb == 3);

    avdc_print_statistics(cache);
    avdc_print_cache_statistics(cache);
    avdc_flush_cache_full(cache);

    //3) #PB > 1 shared -> shared
    avdc_access(cache, 0, AVDC_READ, 0); //first access set to private in core 0
    avdc_access(cache, 0, AVDC_READ, 1); //second access from core 1, set md->pb == 3
    avdc_access(cache, 0, AVDC_READ, 2); //second access from core 1, set md->pb == 3
    assert(cache->cores[0]->md1[0][0].private_bit == 0);
    assert(cache->cores[1]->md1[0][0].private_bit == 0);
    assert(cache->cores[2]->md1[0][0].private_bit == 0);
    assert(cache->md3[0][0].pb == 7);

    avdc_print_statistics(cache);
    avdc_print_cache_statistics(cache);
    avdc_flush_cache_full(cache);

    //1)#PB == 0, untracked to private

    //fill up md1 and md2
    int rounds = (4096*ASSOC*REGION_SIZE) + 1; //MD2
    for (int i = 0; i < BLOCK_SIZE * rounds; i = i + BLOCK_SIZE) {
        avdc_access(cache, i, AVDC_READ, 0);
    }
    assert(cache->md3[0][0].pb == 0);
    avdc_access(cache, 0, AVDC_READ, 0);
    assert(cache->md3[0][0].pb == 1);
    assert(cache->cores[0]->md1[0][0].private_bit == 1);
    avdc_print_statistics(cache);
    avdc_print_cache_statistics(cache);
    avdc_flush_cache_full(cache);

    //TODO
    //Read miss, MD2 miss

    //1)

    //2)

    //3)

    //4)



}

static void multi_core(avdc_cache *cache, type_t type) {


    avdc_access(cache, 0, type, 0);
    avdc_access(cache, 0, AVDC_READ, 1);
    avdc_access(cache, 0, AVDC_READ, 2);
    avdc_access(cache, 0, AVDC_READ, 3);
    avdc_access(cache, 0, AVDC_READ, 4);
    avdc_access(cache, 0, AVDC_WRITE, 0);


}

static void test_capacity(avdc_cache *cache, type_t type) {

    int i;

    int misses = 0;
    int rounds = 512+1;
    rounds = 4096*ASSOC*REGION_SIZE *4 + 1;

    printf("Core 0:\n");
    int j = 0;

    //fill up md1
    for(i= 0; i < BLOCK_SIZE*rounds; i = i +BLOCK_SIZE) {
        avdc_access(cache, i, type, 0);
        j++;
    }
    j=0;

<<<<<<< HEAD
    int li = (int)active_md->li[region_num];

    if (check_l1(li)) { //tested

        int l1_index = l1_index_from_pa(core, pa);
        int l1_way = search_l1_way(core, pa);
        int l2_way = search_l2_way(core, pa);
        assert(l1_way != -1);
        assert(l2_way != -1);

        avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
        assert(l1->active == 1);
        assert(l1->valid == 1);
        assert(l1->timestamp > 0);
        assert(l1->tracker_pointer == md2_way);
        assert(l1->replacement_pointer == l2_way);
        assert(l1->address == active_md->address[region_num]); //TODO: THIS WILL HAVE TO BE CHANGED TO THE RANGE

        //TODO: add that md3 pb == 1
        //assert(active_md->private_bit == 1);


    } else if (check_l2(li)) {

        int l2_index = l2_index_from_pa(core, pa);
        int l2_way = search_l2_way(core, pa);
        assert(l2_way != -1);

        avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];
        assert(l2->active == 1);
        assert(l2->valid == 1);
        assert(l2->timestamp > 0);
        assert(l2->tracker_pointer == md2_way);
        //assert(l2->replacement_pointer == l2_way); TODO: add this case
        assert(l2->address == active_md->address[region_num]); //TODO: THIS WILL HAVE TO BE CHANGED TO THE RANGE

        //TODO: add that md3 pb == 1
        //assert(active_md->private_bit == 1);

    } else if (check_mem(li)) {
        //check l1 and l2 make sure it's not there
        int l1_way = search_l1_way(core, pa);
        int l2_way = search_l2_way(core, pa);
        assert(l1_way == -1);
        assert(l2_way == -1);

    } else if (check_rem(li)) { //if in remote core     ///tested
        assert(active_md->private_bit == 0);
        //assert data is in remote core and in md3
        int region_num = region_from_pa(core, pa);

        int md3_index = md3_index_from_pa(cache, pa);
        int md3_way = search_md3_way(cache, pa);
        assert(md3_way != -1);

        avdc_md3_t * md3 = &cache->md3[md3_index][md3_way];
        assert(md3->timestamp > 0);
        assert(md3->active == 1);
        assert(md3->valid == 1);
        assert(md3->tag == tag);

        int pb = md3->pb;
        assert(pb > 0); // has to be in shared state

        for(int i = 0; i<CORES; i++) { //check for evry shared core
            if (((int) pow((double) 2, i) & pb) && i != thread_id) { //TODO: test this
                avdark_cache_core *rem_core = cache->cores[i];

                //md resources
                int md1_index = md1_index_from_pa(rem_core, pa);
                int md2_index = md2_index_from_pa(rem_core, pa);
                int md1_way = search_md1_way(rem_core, pa);
                int md2_way = search_md2_way(cache, rem_core, pa);
                assert(md1_way != -1);
                assert(md2_way != -1);

                avdc_md_t *md1 = &rem_core->md1[md1_index][md1_way];
                avdc_md_t *md2 = &rem_core->md2[md2_index][md2_way];
                avdc_md_t *active_md;
                if(md1->active == 1 && md2->active == 0){
                    active_md = md1;
                }else if(md1->active == 0 && md2->active == 1) {
                    active_md = md2;
                } else {
                    printf("md1 error");
                    exit(1);
                }

                int li = (int) active_md->li[region_num];

                if (((li & L1_BASE) == L1_BASE) &&
                    ((li & L2_BASE) != L2_BASE)) {      ///tested

                    assert(active_md->private_bit == 0);
                    int l1_index = l1_index_from_pa(rem_core, pa);
                    int l1_way = search_l1_way(rem_core, pa);
                    int l2_way = search_l2_way(rem_core, pa);
                    assert(l1_way != -1);
                    assert(l2_way != -1);

                    avdc_cache_line_t *l1 = &rem_core->l1[l1_index][l1_way];
                    assert(l1->active == 1);
                    assert(l1->valid == 1);
                    assert(l1->timestamp > 0);
                    assert(l1->tracker_pointer == md2_way);
                    assert(l1->replacement_pointer == l2_way);
                    assert(l1->address == active_md->address[i]); //TODO: THIS WILL HAVE TO BE CHANGED TO THE RANGE

                } else if (((li & L1_BASE) != L1_BASE) &&
                           ((li & L2_BASE) == L2_BASE)) {

                    assert(active_md->private_bit == 0);
                    int l2_index = l2_index_from_pa(rem_core, pa);
                    int l2_way = search_l2_way(rem_core, pa);
                    assert(l2_way != -1);

                    avdc_cache_line_t *l2 = &rem_core->l2[l2_index][l2_way];
                    assert(l2->active == 1);
                    assert(l2->valid == 1);
                    assert(l2->timestamp > 0);
                    assert(l2->tracker_pointer == md2_way);
                    //assert(l2->replacement_pointer == l2_way); TODO: add this case
                    assert(l2->address == active_md->address[i]); //TODO: THIS WILL HAVE TO BE CHANGED TO THE RANGE

                } else if (((li & L1_BASE) == L1_BASE) &&
                           ((li & L2_BASE) == L2_BASE)) {
                    printf("error: master cacheline has to be in remote core\n");
                    exit(1);

                }else if ((0 <= li) &&
                          (li <= CORES)) {
                    printf("can't be in remote core already in remote core, master location is invalid state'\n");
                    exit(1);
                }

            }
        }
=======
    for(i= 0; i < BLOCK_SIZE*rounds; i = i +BLOCK_SIZE) {
        // printf("\nJ: %d\n", j);
>>>>>>> temp

        avdc_access(cache, i, type, 0);
        j++;
//        misses++;
        //TEST_SIMPLE_STAT();
    }


    avdc_access(cache, 0, type, 0); //l2 hit
    //avdc_access(cache, 0, type, 0); //l1 hit

}


int main(int argc, char *argv[])
{
    avdc_cache *cache;

    cache = avdc_new_multi_core(32768, 64, 8, 512);

    //printf("Simple [read]\n");
    //test_block_size(cache, AVDC_READ );
    //avdc_flush_cache(cache);

    int rounds;
    //rounds = 64*ASSOC; //L1

    rounds = (512*ASSOC); //L2
    //rounds = (2048*ASSOC) + 1; //MD1
    //rounds = (4096*ASSOC*REGION_SIZE) + 1; //MD2
    //rounds = 589825;        //L3
    //rounds = 4096*ASSOC*REGION_SIZE *4 + 1; //MD3

    //l3 test
    //rounds = 524289;
    //rounds = 69633; //causes md2 hit to memory
    //rounds = 4096 + 1;

    printf("start \n");
    //test_capacity(cache, AVDC_READ);
    //multi_core(cache, AVDC_WRITE);
    printf("finish \n");
    sim_ls(cache);

    //case_a(cache);
    /*
        printf("Read:\n");
        //multi_core(cache, AVDC_READ);

        avdc_print_statistics(cache);
        printf("\n");
        avdc_print_cache_statistics(cache);

        printf("Write: \n");
        //multi_core(cache, AVDC_WRITE);
      */
    avdc_print_statistics(cache);
    printf("\n");
    avdc_print_cache_statistics(cache);
    avdc_flush_cache_full(cache);

    //test_simple(cache, AVDC_READ);
    //printf("End Simple [read]\n");

    /*     printf("Simple [write]\n");

         //test_block_size(cache, AVDC_WRITE );
         //avdc_flush_cache(cache);

         test_capacity(cache, AVDC_WRITE, rounds );
         avdc_print_statistics(cache);

         avdc_flush_cache(cache);

         //test_simple(cache, AVDC_WRITE);
   */

    multi_core_delete(cache);

    printf("\n%s done.\n", argv[0]);
    return 0;
}

/*
 * Local Variables:
 * mode: c
 * c-basic-offset: 8
 * indent-tabs-mode: nil
 * c-file-style: "linux"
 * compile-command: "make -k -C ../../"
 * End:
 */