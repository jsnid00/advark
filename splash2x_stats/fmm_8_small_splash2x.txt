[PARSEC] Benchmarks to run:  splash2x.fmm

[PARSEC] [========== Running benchmark splash2x.fmm [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] No archive for input 'native' available, skipping input setup.
[PARSEC] Running '/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/splash2x_stats/fmm_8_small_splash2x_stats.txt --   /it/slask/student/josn3503/parsec-3.0/ext/splash2x/apps/fmm/inst/amd64-linux.gcc/bin/run.sh 1 native':
[PARSEC] [---------- Beginning of output ----------]
Generating input file input_1...
cat: input.template: No such file or directory
Running /it/slask/student/josn3503/parsec-3.0/ext/splash2x/apps/fmm/inst/amd64-linux.gcc/bin/fmm 1 < input_1:
Segmentation fault (core dumped)
block size:	 64
region size:	 8
l1 ASSOC:	 8
md1 ASSOC:	 12
md2 ASSOC:	 12
md3 ASSOC:	 12
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Woo et al. The SPLASH-2 Programs: Characterization and Methodological Considerations. ISCA, 1995.
[PARSEC]
[PARSEC] Done.
