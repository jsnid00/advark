//
// Created by jsnider on 6/4/17.
//

#include "avdark-cache.h"

int search_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, type_t type) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    int region_num = region_from_pa(core, va);
    int md1_index = md1_index_from_pa(core, va);
    int l1_index = l1_index_from_pa(core, va);
    int l2_index = l2_index_from_pa(core, va);

    int md1_way;
    for (md1_way = 0; md1_way < MD1_ASSOC; md1_way++) {

        avdc_md_t *md1 = &core->md1[md1_index][md1_way];

        if ((md1->tag == tag) &&
            (md1->valid == 1) &&
            (md1->active == 1)) {

            int li = md1->li[region_num];
            assert_addr(&md1->address[region_num], va, 7);
            core->stat.md1_hit += 1;

            if (check_l1(li)){
                //if the tag matches and pos is valid then extract region for cache line info

                //TODO-done: add this back in somehow
                //assert(md1->private_bit == 1);

                if(type == AVDC_READ || md1->private_bit == 1) {
                //TODO-question reads are always safe and if it's private it doesn't matter
                    //update internal statistics:
                    core->stat.md1_l1_hit += 1;

                    int l1_way = li & (L1_BASE - 1);

                    avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
                    assert(l1->active == 1);
                    assert(l1->valid == 1);
                    assert_addr(&l1->address, va, 8);

                    return 1;
                } else if(type == AVDC_WRITE && md1->private_bit == 0) {
                    //then you have to go to md3
                    //TODO: add statistics maybe
                    return 0;
                }
            } else if (check_l2(li)) {

                //TODO: add this back in somehow
                //assert(md1->private_bit == 1);
                if(type == AVDC_READ || md1->private_bit == 1) {
                    //update internal statistics:
                    core->stat.md1_l2_hit += 1;

                    int l2_way = li & (L2_BASE - 1);

                    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];
                    assert(l2->active == 1);
                    assert(l2->valid == 1);
                    assert_addr(&l2->address, va, 9);

                    //todo-done: move to l1

                    copy_l2_to_l1(cache, thread_id, va, 1); //test this

                    return 1;
                } else if(type == AVDC_WRITE && md1->private_bit == 0) { //then you have to go to md3
                    return 0;
                }
            } else if (check_mem(li)) {

                core->stat.md1_mem_hit += 1; //TODO-question this stat should always be here right?
                //can't just grab from memory have to check md3 location
                if (md1->private_bit == 1) {
                    //if in memory and region is private add to region - addendum might not have to be private, no maybe should be, not sure
                    //TODO: check if this returns 2
                    avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);

                    //TODO: remove
                    //TODO: i think here if we access something from memory we need to notify all the other shared nodes
                    //copy_md_to_md3(cache, thread_id, va); //test this
                } else { // this else is becasue if the region in shared there is data in other cores
                    //TODO: find master location?

                    if(type == AVDC_READ) {
                        //TODO-done: don't access md3 go straight to the source
                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);

                        //TODO-done: and update MD3
                        copy_md_to_md3(cache, thread_id, va); //test this

                    } else if(type == AVDC_WRITE) {
                        //FROMERIK: invalidate only the cache line on write
                        //invalidate
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
                    }
                }
                return 1; //TODO-question why wasn't this here before
            } else if (check_rem(li)) {

                assert(md1->private_bit == 0);
                core->stat.md1_rem_hit += 1;

                //TODO-done: check if read or write, i think if it's write you have to invalidate,
                //todo-cont: if it's a read you can maybe allow both copies
                if(type == AVDC_READ) {
                    //TODO-done: don't access md3 go straight to the sorce
                    //install untracked data into md1 and md2
                    copy_from_remote_node(cache, thread_id, li, va); //TODO: test this

                } else if(type == AVDC_WRITE) {
                    //invalidate
                    invalidate_cachline(cache, thread_id, va); //TODO: test this
                    avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
                }

                return 1;
            } else if (check_l3(li)) {

                //TODO: assert data is in l3
                //update internal statistics:
                core->stat.md1_l3_hit += 1;

                int l3_index = l3_index_from_pa(cache, va);
                int l3_way = li & (L3_BASE - 1);

                avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];
                assert(l3->active == 1);
                assert(l3->valid == 1);

                assert_addr(&l3->address, va, 10); //TODO: fix this it errors sometimes

                //insert data into requesting node
                if (md1->private_bit == 1) {
                    //if in memory and region is private add to region - addendum might not have to be private, no maybe should be, not sure
                    //TODO: check if this returns 2
                    return avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
                } else {
                    //TODO
                    //find master location

                    if(type == AVDC_READ) {
                        //TODO-done: don't access md3 go straight to the source
                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way); //this kinda worked
                        copy_md_to_md3(cache, thread_id, va); //test this

                    } else if(type == AVDC_WRITE) {
                        //FROMERIK: invalidate only the cache line on write
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
                    }
                }
                return 1;
            } else {
                printf("***err: severe error in li encoding md1 in search_md1\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    return 0;
}

int search_md1_way (avdark_cache_core *core, avdc_pa_t pa) {
    //if entry already exists return it

    int md1_index = md1_index_from_pa(core, pa);
    avdc_pa_t tag = tag_from_pa(core, pa);
    int i;
    for(i = 0; i <MD1_ASSOC; i++) {
        if(core->md1[md1_index][i].tag == tag &&
           core->md1[md1_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_md1_way(avdark_cache_core *core, avdc_pa_t pa) {

    int md1_index = md1_index_from_pa(core, pa);
    avdc_pa_t tag = tag_from_pa(core, pa);

    //TODO: remove the code for searching
    //if entry already exists return it
    int i;
    for(i = 0; i <MD1_ASSOC; i++) {
        if(core->md1[md1_index][i].tag == tag &&
                core->md1[md1_index][i].valid == 1) {
            return i;
        }
    }

    //find an entry that isn't valid yet use that one
    for(i = 0; i < MD1_ASSOC; i++) {
        if (core->md1[md1_index][i].valid == 0){
            return i;
        }
    }


//    //TODO: remove aftet testing
//    if(tag == 4259536) {
//        printf("test eviction here\n");
//    }

    //if all entries are taken, select one for eviction then remove everything
    int LRU_way = 0;
    double diff_time = 0;
    for(i=1; i < MD1_ASSOC; i++){
        diff_time = difftime(core->md1[md1_index][LRU_way].timestamp,
                             core->md1[md1_index][i].timestamp);
        if(diff_time > 0)
            LRU_way = i;
    }
    //int LRU_way now has the smallest index

    copy_md1_to_md2(core, pa, md1_index, LRU_way);

    return LRU_way;
}

/*
 * this function copies data from one core to another core
 *
 * tailored for case a read miss, md1/md2 hit going to a remote node
 *
 * TODO-done: test please, tested for md1, testing alot
 *
 * TODO: test for md2
 */
void copy_from_remote_node(avdc_cache *cache, int thread_id, int li, avdc_pa_t va){

    avdark_cache_core *core = cache->cores[thread_id];
    avdark_cache_core *rem_core = cache->cores[li];

    //TODO: assert shared in md3 maybe
    //TODO: assert active in md1/md2 for both cores

    int md1_index = md1_index_from_pa(core,va);
    int md1_way = search_md1_way(core, va);
    //assert(md1_way != -1);  //TODO: this will have to change

    int md2_index = md2_index_from_pa(core,va);
    int md2_way = search_md2_way(cache, core, va);
    assert(md2_way != -1);

    avdc_md_t * md2 = &core->md2[md2_index][md2_way];
    avdc_md_t * active_md;
    if(md2_way != -1) {
        avdc_md_t * md1 = &core->md1[md1_index][md1_way];
        if(md1->active == 1) {
            active_md = md1;
        } else if(md2->active == 1){
            active_md = md2;
        } else {printf("error in md active : copy_from_remote_node"); exit(1);}
    } else {
        active_md = md2;
    }

    /*int rem_md1_index = md1_index_from_pa(rem_core,va);
    int rem_md1_way = search_md1_way(rem_core, va);
    assert(md1_way != -1);

    int rem_md2_index = md2_index_from_pa(rem_core,va);
    int rem_md2_way = search_md2_way(cache, rem_core, va);
    assert(md2_way != -1);

    avdc_md_t * rem_md1 = &rem_core->md1[rem_md1_index][rem_md1_way];  //TODO: test
    avdc_md_t * rem_md2 = &rem_core->md2[rem_md2_index][rem_md2_way]; //TODO: test
    avdc_md_t * active_rem_md;

    if(rem_md1->active == 1) {
        active_rem_md = rem_md1;
    } else if(rem_md2->active == 1){
        active_rem_md = rem_md2;
    } else {printf("error in md active : copy_from_remote_node"); exit(1);}

     assert(active_rem_md->private == 0);
     */

    //TODO: assert that data in remote core is in l1 or l2

    //TODO-done: install data core

    int region_num = region_from_pa(core, va);
    //cache resources
    int l1_index = l1_index_from_pa(core, va);
    int l2_index = l2_index_from_pa(core, va);
    int l1_way = find_l1_way(core, va);
    int l2_way = find_l2_way(cache, thread_id, va);

    avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

    //update active md entry
    active_md->timestamp = clock();
    active_md->li[region_num] = L1_BASE + l1_way;
    core->md1_entries++;

    //duplicated code :(
    /// CREATE l1 entry
    assert(l1->valid == 0 && l1->active == 0);

    l1->valid = 1;
    l1->address = va;
    l1->active = 1;
    l1->timestamp = clock();
    l1->tracker_pointer = md2_way;
    l1->replacement_pointer = l2_way;
    core->number_of_l1_entries++;

    ///CREATE l2 entry
    assert(l2->valid == 0 && l2->active == 0);

    l2->valid = 1;
    l2->address = va;
    l2->active = 0;
    l2->timestamp = clock();
    l2->tracker_pointer = md2_way;
    core->number_of_l2_entries++;
    //TODO: l2->replacement_pointer = MD3_way;
    //end duplicated code :)
}


void copy_md1_to_md2(avdark_cache_core *core, avdc_pa_t pa, int md1_index, int LRU_way){
    avdc_md_t * LRU_md1 = &core->md1[md1_index][LRU_way];

    avdc_pa_t LRU_va = LRU_md1->address[0]; //default use first address in region
    int region_num = region_from_pa(core, LRU_va);

    int LRU_md1_index = md1_index_from_pa(core, LRU_va);
    int LRU_md2_index = md2_index_from_pa(core, LRU_va);

    int LRU_md2_way = LRU_md1->tracker_pointer;

    avdc_md_t * LRU_md2 = &core->md2[LRU_md2_index][LRU_md2_way];

    assert(LRU_md1->valid == 1);
    assert(LRU_md1->active == 1);

    assert_addr(&LRU_md1->address[region_num], LRU_va, 11); //dumb assert
    assert_addr(&LRU_md2->address[region_num], LRU_va, 12);

    *LRU_md2 = *LRU_md1; //whoa
    LRU_md1->valid = 0;
    LRU_md1->active = 0;
    LRU_md2->valid = 1;
    LRU_md2->active = 1;
    LRU_md2->tracker_pointer = -1;
    LRU_md2->timestamp = clock();

    clear_md1_entry(core, LRU_md1_index, LRU_way);
}

void clear_md1_entry(avdark_cache_core *core, int md1_index, int md1_way){

    avdc_md_t  *md1 = &core->md1[md1_index][md1_way];

    if(md1->active == 0 && md1->valid == 1) {
        //then there is an md1 entry that has to be delted right?
        printf("clear an md1\n");
        exit(1);
    }

    if(md1->active && md1->valid) {

        for(int i = 0; i < REGION_SIZE; i++){
            int li = md1->li[i];
            avdc_pa_t va = md1->address[i];

            if(check_l1(li)){
                clear_l1_entry_va(core,va);
            } else if (check_l2(li)) {
                clear_l2_entry_va(core, va);
            } else if (check_l3(li)) {
                //don't need to do anything i don't think
                //maybe notify md3
            } else if (check_mem(li)) {
                //don't do anything
            } else if (check_rem(li)) {
                //maybe send a notification to the remote node or to md3
                //to be removed from the pb
            } else {
                printf("error in li encosing 7212\n");
                exit(1);
            }

        }
    }

    md1->valid = 0;
    md1->active = 0;
    md1->tag = 0;
    md1->timestamp = 0; //maybe this breaks things
    md1->private_bit = 0;
    md1->tracker_pointer = -1;



    for(int g=0; g<REGION_SIZE; g++){
        md1->address[g] = 0;
        md1->li[g] = 0;
    }

}
