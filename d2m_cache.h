/**
 * Cache simulation using a functional system simulator.
 *
 * Course: Advanced Computer Architecture, Uppsala University
 * Course Part: Lab assignment 1
 *
 * Author: Andreas Sandberg <andreas.sandberg@it.uu.se>
 * Revision (2015, 2016): German Ceballos

 * Additions made to implement D2D by Johan Snider

 */

#ifndef AVDARK_CACHE_H
#define AVDARK_CACHE_H

#include <stdint.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
//#include <MacTypes.h>

#define DEBUG 1
#define PRINT 1 
#define CAP_PRINT 1

/**
 * Cache parameters
 */

#define BLOCK_SIZE 64

<<<<<<< HEAD
#define MD1_ASSOC 8
#define MD2_ASSOC 8
#define MD3_ASSOC 8
=======
#define REGION_SIZE 64
#define MD1_ASSOC 3
#define MD2_ASSOC 3
#define MD3_ASSOC 3

>>>>>>> temp
#define L1_ASSOC 8
#define L2_ASSOC 8
#define L3_ASSOC 16

<<<<<<< HEAD
//these are the MD1 number of
#define MD1_SIZE 128
#define MD2_SIZE 4096
#define MD3_SIZE 16384
=======
#define MD1_SIZE 16
#define MD2_SIZE 512
#define MD3_SIZE 2048
>>>>>>> temp

#define L1_SIZE 32768

///constants for li encoding, only need to change if cores change
#define L1_BASE 8
#define L2_BASE 16
#define MEM_BASE 24
#define L3_BASE 32

#define CORES 8

/** Physical address representation within the cache model */
typedef uint64_t avdc_pa_t;

typedef unsigned avdc_size_t;
typedef unsigned avdc_block_size_t;
typedef unsigned avdc_assoc_t;
typedef unsigned avdc_region_t;

/**
 * Memory access type to simulate.
 */
typedef enum {
    AVDC_READ = 0,  /** Single read access */
    AVDC_WRITE,     /** Single write access */
} type_t;

/**
 *    Struct for cacheline
 */

typedef struct avdc_cache_line {
    int valid;
    int active;
    avdc_pa_t address;
    //data would be here
    int tracker_pointer;
    int replacement_pointer;
    clock_t timestamp;
} avdc_cache_line_t;

/**
 *    Struct for md1 and md2 meta-data
 */

typedef struct avdc_md {
    int valid;
    avdc_pa_t tag;
    int active;
    int private_bit; //should be bool

    int tracker_pointer;
    avdc_pa_t address[REGION_SIZE];
    short li[REGION_SIZE];
    clock_t timestamp;

} avdc_md_t;

/**
 *    Struct for md3 meta-data
 */

typedef struct avdc_md3 {
    int valid;
    avdc_pa_t tag;
    int active;

    avdc_pa_t address[REGION_SIZE];
    short li[REGION_SIZE];
    clock_t timestamp;
    int pb;

} avdc_md3_t;

/**
  * Statistics for md1 and md2
  */
typedef struct {
    unsigned long long data_write;
    unsigned long long data_write_miss;
    unsigned long long data_read;
    unsigned long long data_read_miss;
    unsigned long long md1_hit;
    unsigned long long md1_l1_hit;
    unsigned long long md1_l2_hit;
    unsigned long long md1_l3_hit;
    unsigned long long md1_ns_hit;
    unsigned long long md1_fs_hit;
    unsigned long long md1_rem_hit;
    unsigned long long md1_mem_hit;
    unsigned long long md1_region_hit;
    unsigned long long md2_region_hit;
    unsigned long long md2_hit;
    unsigned long long md2_l1_hit;
    unsigned long long md2_l2_hit;
    unsigned long long md2_l3_hit;
    unsigned long long md2_ns_hit;
    unsigned long long md2_fs_hit;
    unsigned long long md2_rem_hit;
    unsigned long long md2_mem_hit;
    unsigned long long md2_mem_miss;
    unsigned long long md3_hit;
    unsigned long long md3_l1_hit;
    unsigned long long md3_l2_hit;
    unsigned long long md3_ns_hit;
    unsigned long long md3_fs_hit;
    unsigned long long md3_rem_hit;
    unsigned long long md3_mem_hit;
    unsigned long long errors;
    unsigned long long addr_errors;

} stat_t;

/**
  * Statistics. for MD3
  */
typedef struct {
    //TODO: find a nice place to initalize all these to zero

    unsigned long long md3_hit;
    unsigned long long md3_l1_hit;
    unsigned long long md3_l2_hit;
    unsigned long long md3_l3_hit;
    unsigned long long md3_ns_hit;
    unsigned long long md3_fs_hit;
    unsigned long long md3_rem_hit;
    unsigned long long md3_mem_hit;
    unsigned long long md3_mem_miss;
    unsigned long long invalidations;
    unsigned long long errors;
    unsigned long long addr_errors;


} stat3_t;

/**
 *         Core simulator instance variables
 */
typedef struct {


    /**
     * cacheline pointer structures for l1 and l2
     */
    avdc_cache_line_t **l1;
    avdc_cache_line_t **l2;

    /**
     * metadata pointer structures for md1 and md2
     */
    avdc_md_t **md1;
    avdc_md_t **md2;


    int number_of_l2_entries;
    int number_of_l1_entries;
    int md1_entries;
    int md2_entries;
    stat_t stat;
    /**
         * Cached internal data.
         */
    int tag_shift;
    int block_size_log2;
    int number_of_l1_sets;
    int max_l1_entries;
    int number_of_l2_sets;
    int region_shift;

    int md1_size;
    int md1_reach;
    int md2_size;

    int md2_reach;


    /**
    *       Cache parameters. Use avdc_resize() update them.
    */
    avdc_size_t l1_size;
    avdc_block_size_t block_size;
    avdc_assoc_t assoc;
    avdc_region_t region_size;
    avdc_size_t l2_size;


} avdark_cache_core;

/**
 * this will be the main structure of the
 * cache that contains cores and llc
 */
typedef struct {

    /**
     * For debugging
     */
    int dbg;
    const char *dbg_name;
    /**
         * Cached internal data.
         */
    int tag_shift;
    int block_size_log2;
    int number_of_l1_sets;
    int max_l1_entries;
    int number_of_l2_sets;
    int region_shift;

    int md1_size;
    int md1_reach;
    int md2_size;
    int md2_reach;

    /**
     *  structure for cores
     */
    avdark_cache_core *cores[CORES];


    /**
     * Structures for l3 and md3
     */
    avdc_cache_line_t **l3;
    avdc_md3_t **md3;

    avdc_size_t l3_size;
    int l3_sets;
    avdc_size_t md3_size;
    int md3_assoc;
    int l3_assoc;
    avdc_block_size_t block_size;


    /**
     *  Statistics specific to md3 and l3
     */
    stat3_t stat3;

    int accesses; //for sanity checks

} avdc_cache;

/**
 * Initalize a new instance of a core
 *
 */
avdark_cache_core *avdc_new();

/**
 * Create a new instance of the cache simulator with multi core
 *
 * @param size Cache size in bytes
 * @param block_size Cache block size in bytes
 * @param assoc Cache associativiy

 * @param region_size the number of cachlines in a region *this is what the project is on*

 */
avdc_cache *avdc_new_multi_core(avdc_size_t size, avdc_block_size_t block_size,
                                avdc_assoc_t assoc, avdc_region_t region_size);

/**
 * Destroy an instance of the cache simulator.
 *
 * @param self Simulator instance
 */
void avdc_delete(avdark_cache_core *self);

void multi_core_delete(avdc_cache *self);

/**
 * Resize the cache.
 *
 * Initializes the cache using the new cache size parameters. This
 * function also precomputes a set of values used in various functions
 * used by the cache simulator.
 *
 * @param core Simulator instance
 * @param size Cache size in bytes
 * @param block_size Cache block size in bytes
 * @param assoc Cache associativiy
 * @return 0 on error, 1 on success
 */
int avdc_resize(avdark_cache_core *core, avdc_size_t size,
                avdc_block_size_t block_size, avdc_assoc_t assoc,
                avdc_region_t region_size);


/**
 * Debug printing. This function works just like printf but the first
 * argument must be the avdark_cache_t structure. This function only
 * prints information if debug printing is enabled, this can be
 * controled with the dbg-enable and dbg-disable commands (from
 * python)
 *
 * @param self Simulator instance
 * @param msg printf formated string
 */
void avdc_dbgssoc_log(avdark_cache_core *self, const char *msg, ...)
__attribute__((format (printf, 2, 3)));

/**
 *  Searching md1 for metadata entries. These functions check to see if a cache entry exists for va
 * @param core pointer to the core that is going to be searched
 * @param pava the virtual address of the access
 * @return returns 1 on success, 0 and failure, and 2 on hit in the region but still brought in from memory
 *
 * we assert that the va matches the entry in the md and cache level
 * possibly calls: avdc_insert_into_md_region
 *
 */
int search_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, type_t type);

int search_md2(avdc_cache *cache, int thread_id, avdc_pa_t va, type_t type);

int search_md3(avdc_cache *self, avdc_pa_t va, int thread_id, type_t type);

/**
 * avdc_insert: creates and adds entries into a core's md1, md2, l1 and l2
 *
 * TODO: add asserts at the end of this function to make sure all data and entries are present
 *
 * @param cache the whole cache object
 * @param core the core that will have entires added to it
 * @param va
 * @return returns md1 index for a possible sanity check
 */
int avdc_insert(avdc_cache *cache, int thread_id, avdc_pa_t pa);

int avdc_insert_into_md_region(avdc_cache *cache, int thread_id, avdc_pa_t pa, int md_level, int md_way);

int avdc_insert_md3(avdc_cache *cache, avdc_pa_t pa, int thread_id, type_t type);

int add_to_core_from_md3(avdc_cache *cache, int thread_id, avdc_pa_t pa);


/**
 * finding ways for the MD these functions return a free way to be used
 *
 * searching for ways returns the asso_way or -1 if not found
 *
 * @param core
 * @param pa
 * @return
 */
int find_md1_way(avdc_cache *cache, int thread_id, avdc_pa_t pa);
int find_md2_way(avdc_cache *cache, int thread_id, avdc_pa_t va);
int find_md3_way(avdc_cache *cache, avdc_pa_t pa);

int search_md3_way(avdc_cache *cache, avdc_pa_t pa);
int search_md1_way (avdark_cache_core *core, avdc_pa_t pa);
int search_md2_way(avdc_cache *cache, avdark_cache_core *core, avdc_pa_t va) ;

/**
 * finding and searching ways for the caches these functions return a free way to be used
 *
 * @param core
 * @param pa
 * @return
 */
int find_l1_way(avdc_cache *cache, int thread_id, avdc_pa_t pa);
int search_l1_way(avdark_cache_core *self, avdc_pa_t pa);

int find_l2_way(avdc_cache *cache, int thread_id, avdc_pa_t pa);
int search_l2_way(avdark_cache_core *self, avdc_pa_t pa);

int find_l3_way(avdc_cache *cache, avdc_pa_t pa);
int search_l3_way(avdc_cache *cache, avdc_pa_t va);

/**
 * Simulate a full cache flush
 *
 * @param self Simulator instance
 */
void avdc_flush_cache(avdark_cache_core *self);
void avdc_flush_cache_full(avdc_cache *self);

/**
 * Execute a cache line access
 *
 * @param self Simulator instance
 * @param pa Physical address to access
 * @param type Access type
 */
void avdc_access(avdc_cache *self, avdc_pa_t pa, type_t type, int thread_id);

/**
 * Reset cache statistics
 *s
 * @param self Simulator instance
 */
void avdc_reset_statistics(avdark_cache_core *self);

/**
 * Print information about the cache, e.g. size and other parameters.
 *
 * @param self Simulator instance
 */
void avdc_print_info(avdark_cache_core *self);
void avdc_print_statistics(avdc_cache *cache);
void avdc_print_cache_statistics(avdc_cache *cache);

/**
 * Functions for cleaning individual md and cache entries
 */
void clear_md1(avdc_cache *cache, int thread_id, int md1_index, int md1_way, int mode);
void clear_md2(avdc_cache *cache, int thread_id, int md2_index, int md2_way, int mode);
void clear_md3(avdc_cache *cache, int md3_index, int md3_way);

void clear_md1_entry_va(avdc_cache *cache, int thread_id, avdc_pa_t va, int mode);
void clear_md2_entry_va(avdc_cache *cache, int thread_id, avdc_pa_t va, int mode);


//void clear_l1_entry(avdark_cache_core *self, int l1_index, int l1_way);
void clear_l1_entry_va(avdark_cache_core *core, avdc_pa_t va);

//void clear_l2_entry(avdark_cache_core *self, int l2_index, int l2_way);
void clear_l2_entry_va(avdark_cache_core *core, avdc_pa_t va);

void clear_l3_entry_va(avdc_cache *cache, avdc_pa_t va);
//void clear_l3_entry(avdc_cache *cache, int l3_index, int l3_way);

/**
 * Functions for tagging and indexing addresses
 */

avdc_pa_t tag_from_pa(avdark_cache_core *self, avdc_pa_t pa);
avdc_pa_t pa_from_tag(avdark_cache_core *self, avdc_pa_t pa);


/** functions to get the index for the caches and the MDs
 *
 * @param self
 * @param pa
 * @return
 */
int l1_index_from_pa(avdark_cache_core *self, avdc_pa_t pa);
int l2_index_from_pa(avdark_cache_core *self, avdc_pa_t pa);
int l3_index_from_pa(avdc_cache *self, avdc_pa_t pa);

int region_from_pa(avdark_cache_core *self, avdc_pa_t pa);

int md1_index_from_pa(avdark_cache_core *self, avdc_pa_t pa);
int md2_index_from_pa(avdark_cache_core *self, avdc_pa_t pa);
int md3_index_from_pa(avdc_cache *self, avdc_pa_t pa);

/**
 * functions for checking bit patterns
 */
int check_l1(int li);
int check_l2(int li);
int check_l3(int li);
int check_mem(int li);
int check_rem(int li);



/**
 * used the assert the correct address is found
 * @param pa_ptr
 * @param pa
 * @param fx_code
 */
void assert_addr(avdc_cache *cache, int thread_id, avdc_pa_t *pa_ptr, avdc_pa_t pa, int fx_code);
int assert_debug(avdc_cache *cache, int thread_id, int value, int test, char * string, int equal);

/**
 *  functions for handling pb operations
 * @param thread_id
 * @return
 */
int set_pb(int thread_id);
int number_of_pb(int pb);
int remove_pb(int pb, int thread_id);
int get_thread(int pb);
int find_remote_core(int pb);

/**
 * functions for creating entries in the caches
 *
 * @param cache
 * @param thread_id
 * @param va
 * @param md2_way
 * @param l2_way
 * @return
 */
int add_to_core(avdc_cache *cache, int thread_id, avdc_pa_t va);
int add_to_l1(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way, int l2_way);
int add_to_l2(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way);
int add_to_l3(avdc_cache *cache, avdc_pa_t LRU_pa);

int create_md2(avdc_cache *cache, int thread_id, avdc_pa_t va);
int create_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way);

//todo implement create md3

avdc_md_t * search_active_md(avdc_cache *cache, int thread_id, avdc_pa_t va);
avdc_md_t * search_md2_entry(avdc_cache *cache, int thread_id, avdc_pa_t va);
avdc_md_t * search_md1_entry(avdc_cache *cache, int thread_id, avdc_pa_t va);
avdc_md3_t * search_md3_entry(avdc_cache *cache, avdc_pa_t va, int code);

avdc_cache_line_t * search_l2_entry(avdc_cache *cache, int thread_id, avdc_pa_t va);
avdc_cache_line_t * search_l3_entry(avdc_cache *cache, avdc_pa_t va);
avdc_cache_line_t * search_l1_entry(avdc_cache *cache, int thread_id, avdc_pa_t va);


/**
 *  functions for moving data around in the caches
 *
 * @param cache
 * @param thread_id
 * @param pa
 * @param md1_index
 * @param LRU_way
 */
void copy_md1_to_md2(avdc_cache *cache, int thread_id, avdc_pa_t pa, int md1_index, int LRU_way);
void copy_from_remote_node(avdc_cache *cache, int thread_id, int li, avdc_pa_t va);
void copy_l2_to_l1(avdc_cache *cache, int thread_id, avdc_pa_t va, int cache_level);
void copy_md_to_md3(avdc_cache *cache, int rem_thread_id, avdc_pa_t pa, int fx_code);
int copy_md2_to_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way);

/**
 *  invadlidate cache line by va, either in a thread, or entre cache
 * @param cache
 * @param thread_id or -1 to nuke all cachelines
 * @param va
 */

void invalidate_cachline(avdc_cache *cache, int thread_id, avdc_pa_t va);
/**
 *     this invalidate only invalidates a cachline in a remote core
 *
 * @param cache
 * @param thread_id is the remote core
 * @param va
 * @param new_thread is the thread that the md will now point to
 * @param new_thread is the thread that the md will now point tosearch_
 */
void invalidate_core_cacheline(avdc_cache *cache, int thread_id, avdc_pa_t va, int new_thread);

<<<<<<< HEAD
void assert_addr_dbg(avdc_pa_t *addr_ptr, avdc_pa_t pa, int fx_code, int md_index, int md_way, int l_index, int l_way, int li);
=======
void md3_validate_pb(avdc_cache *cache, int thread_id, avdc_pa_t va);

void remove_from_core(avdc_cache *cache, int thread_id, avdc_pa_t va);
>>>>>>> temp


#endif
