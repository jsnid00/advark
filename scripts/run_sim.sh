#!/bin/bash

export PIN_ROOT=/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/
cd  /it/slask/student/josn3503/parsec-3.0/
source env.sh
cd
cd advark


source /home/josn3503/advark/scripts/black.sh &

source /home/josn3503/advark/scripts/body.sh &

source /home/josn3503/advark/scripts/canneal.sh &

source /home/josn3503/advark/scripts/facesim.sh &

source /home/josn3503/advark/scripts/ferret.sh &

source /home/josn3503/advark/scripts/fluidanimate.sh &

source /home/josn3503/advark/scripts/freqmine.sh &

source /home/josn3503/advark/scripts/raytrace.sh &

source /home/josn3503/advark/scripts/streamcluster.sh &

source /home/josn3503/advark/scripts/vips.sh &
