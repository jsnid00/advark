#!/bin/bash

export PIN_ROOT=/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/
cd  /it/slask/student/josn3503/parsec-3.0/
source env.sh
cd
cd advark

parsecmgmt -a run -p splash2x.barnes -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/barnes_8_small_stats.txt -- "  &
parsecmgmt -a run -p splash2x.cholesky -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/cholesky_8_small_stats.txt -- " &
parsecmgmt -a run -p splash2x.fft -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/fft_8_small_stats.txt -- " &
parsecmgmt -a run -p splash2x.fmm -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/fmm_8_small_stats.txt -- " &
parsecmgmt -a run -p splash2x.ocean_cp -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/ocean_cp_8_small_stats.txt -- " &
parsecmgmt -a run -p splash2x.ocean_ncp -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/ocean_ncp_8_small_stats.txt -- " &																									 &
parsecmgmt -a run -p splash2x.radiosity -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/radiosity_8_small_stats.txt -- "	&																								 &
parsecmgmt -a run -p splash2x.radix -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/radix_8_small_stats.txt -- "	&																								 &
parsecmgmt -a run -p splash2x.raytrace -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/raytrace_8_small_stats.txt -- " &																									 &
parsecmgmt -a run -p splash2x.water_nsquared -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/water_nsquared_8_small_stats.txt -- " &																									 &
parsecmgmt -a run -p splash2x.water_spatial -i simlarge -s "/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/d2m_pin.so -o /home/josn3503/advark/stats/water_spatial_8_small_stats.txt -- "	&
