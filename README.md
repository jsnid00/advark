# README #

Cache simulator using pin

### Useful commands ###

# Micro benchmark #
compiling micro benchmark on mac with:
clang++ -std=c++11 -o thread thread_test.cpp

# PIN TOOL #

##onmac##
- run this to set a path thing:
    export PIN_ROOT=/Users/jsnider/pin
- run this to build the pin tool:
    make obj-intel64/d2m_pin.dylib
- run this to run the pin tool:
    ../pin/pin -t obj-intel64/d2m_pin.dylib -- /Users/jsnider/advark/benchmarks/thread

    ../pin/pin -appdebug_enable -appdebug_silent -t obj-intel64/d2m_pin.dylib -stackbreak 4000 -- ls

##on linux##

export PIN_ROOT=/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/
cd  /it/slask/student/josn3503/parsec-3.0/
source env.sh
cd


benchamrk:
g++ -pthread -std=c++11 -o thread thread_test.cpp


make obj-intel64/d2m_pin.so


echo $PIN_ROOT/
/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/

../../../it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t obj-intel64/d2m_pin.so -- /bin/ls
134725, 303, 55741, 86, 190077, 184706, 1957, 12, 3402, 0, 0, 0, 0, 0, 0, 0, 389
117524, 309, 48749, 92, 165872, 160381, 2079, 15, 3397, 0, 0, 0, 0, 0, 0, 0, 401, 0, 0, 0, 0, 0, 0, 401, 0, 0


wow it worked:


## Source ##

* Project based on cache simulation assignment from Uppsala University

 ssh -l josn3503 atterbom.it.uu.se


#Auhor#

Johan Snider
