/**
 * Cache simulation using a functional system simulator.
 *
 * Course: Advanced Computer Architecture, Uppsala University
 * Course Part: Lab assignment 1
 *
 * Original authors: UART 1.0(?)
 * Modified by: Andreas Sandberg <andreas.sandberg@it.uu.se>
 * Revision (2015, 2016): German Ceballos

 * Additions made to implement D2D by Johan SNIDER
 */

#include "d2m_cache.h"

#include <stdarg.h>
#include <string.h>
#include <execinfo.h>
#include <stdio.h>

#ifdef SIMICS
/* Simics stuff  */
#include <simics/api.h>
#include <simics/alloc.h>
#include <simics/utils.h>

#define AVDC_MALLOC(nelems, type) MM_MALLOC(nelems, type)
#define AVDC_FREE(p) MM_FREE(p)

#else

#define AVDC_MALLOC(nelems, type) malloc(nelems * sizeof(type))
#define AVDC_FREE(p) free(p)

#endif

<<<<<<< HEAD
void assert_addr(avdc_pa_t *addr_ptr, avdc_pa_t pa, int fx_code) {

    /* //TODO: find out why this has to accept both starting from zero and with memory accesses
     assert((*addr_ptr - BLOCK_SIZE <= pa) || (*addr_ptr <= pa));
     //assert(*addr_ptr <= pa);
     assert(pa < *addr_ptr + BLOCK_SIZE);
     */
    //TODO: find out why this has to accept both starting from zero and with memory accesses
    if(!((*addr_ptr - BLOCK_SIZE <= pa) || (*addr_ptr <= pa))) {
        printf("addr_ptr, pa: %llu : %llu -> %d\n", *addr_ptr, pa, fx_code);
        //exit(1);

       /* void* callstack[128];
        int i, frames = backtrace(callstack, 128);
        char** strs = backtrace_symbols(callstack, frames);
        for (i = 0; i < frames; ++i) {
            printf("%s\n", strs[i]);
        }
        free(strs);
        */
=======
int cap_array[6] = {0};
>>>>>>> temp

void assert_addr(avdc_cache *cache, int thread_id, avdc_pa_t *addr_ptr, avdc_pa_t pa, int fx_code) {

    avdark_cache_core *core;
    int mode = 0;
    if (thread_id >= 0) {
        core = cache->cores[thread_id];
        mode = 0;
    } else {
        mode = 1;
    }

<<<<<<< HEAD

       /*
        * void* callstack[128];
        int i, frames = backtrace(callstack, 128);
        char** strs = backtrace_symbols(callstack, frames);
        for (i = 0; i < frames; ++i) {
            printf("%s\n", strs[i]);
        }
        free(strs);
        */
=======
    if (!((*addr_ptr <= pa) &&
          (pa < *addr_ptr + BLOCK_SIZE))) {
        if (DEBUG) {
            if (mode == 0) {
                core->stat.addr_errors++;
            } else {
                cache->stat3.addr_errors++;
            }
            if (PRINT)
                printf("*** err addr_ptr, pa: %lu : %lu -> %d\n", (unsigned long) *addr_ptr, (unsigned long) pa,
                       fx_code);
        } else {
            printf("*** err addr_ptr, pa: %lu : %lu -> %d\n", (unsigned long) *addr_ptr, (unsigned long) pa, fx_code);
            exit(1);
        }
    }
}

/***
 *
 * @param cache
 * @param thread_id
 * @param value
 * @param test
 * @param string
 * @param equal
 * @return  0 is failure
 */
int assert_debug(avdc_cache *cache, int thread_id, int value, int test, char *string, int equal) {
>>>>>>> temp

    avdark_cache_core *core;
    int mode = 0;
    if (thread_id >= 0) {
        core = cache->cores[thread_id];
        mode = 0;
    } else {
        mode = 1;
    }

    if (DEBUG == 1) {
        if (equal == 1) {
            if (value != test) {
                if (mode == 0) {
                    core->stat.errors++;
                } else {
                    cache->stat3.errors++;
                }
                if (PRINT) {
                    printf("%s", string);
                }
                return 0;
            } else {
                return 1;
            }
        } else if (equal == 0) {
            if (value == test) {
                if (mode == 0) {
                    core->stat.errors++;
                } else {
                    cache->stat3.errors++;
                }
                if (PRINT) {
                    printf("%s", string);
                }
                return 0;
            } else {
                return 1;
            }
        }
    } else {
        if (equal == 1) {
            assert(value == test);
            return 1;
        } else if (equal == 0) {
            assert(value != test);
            return 1;
        }
    }

    return 0;

}

void assert_addr_dbg(avdc_pa_t *addr_ptr, avdc_pa_t pa, int fx_code, int md_index, int md_way, int l_index, int l_way, int li) {


    //TODO: find out why this has to accept both starting from zero and with memory accesses
    if(!((*addr_ptr - BLOCK_SIZE <= pa) || (*addr_ptr <= pa))) {
        printf("addr_ptr1, pa: %llu : %llu -> %d, md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", *addr_ptr, pa, fx_code, md_index, md_way, l_index, l_way, li);
        //printf("md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", md_index, md_way, l_index, l_way, li);


        //exit(1);

    }

    if(!(pa < *addr_ptr + BLOCK_SIZE)) {
        printf("addr_ptr2, pa: %llu : %llu -> %d, md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", *addr_ptr, pa, fx_code, md_index, md_way, l_index, l_way, li);
        //printf("md_index: %d, md_way: %d, l_index: %d, l_way: %d, li: %d\n", md_index, md_way, l_index, l_way, li);

        //exit(1);


    }

}

/**
 * Extract the cache line tag from a physical address.
 */
avdc_pa_t
tag_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    /* printf("tag_from_pa: 0x%.16lx, tag = 0x%.16lx\n",
      (unsigned long)pa ,
      (unsigned long)pa >> (self->tag_shift));*/
    return pa >> (self->tag_shift);
}

/*
 * To fill up the md1 and md2 entries
 */
avdc_pa_t
pa_from_tag(avdark_cache_core *self, avdc_pa_t tag) {
    return tag << (self->tag_shift);
}

/**
 * Calculate the cache line index from a physical address.
 */
int
l1_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {

    return (pa >> self->block_size_log2) & (self->number_of_l1_sets - 1);
}

int
l2_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {

    return (pa >> self->block_size_log2) & (self->number_of_l2_sets - 1);
}

int
l3_index_from_pa(avdc_cache *self, avdc_pa_t pa) {
    return (pa >> self->cores[0]->block_size_log2) & (self->l3_sets - 1);
}


//returns the region place for a cache line
int region_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    /* printf("pa: 0x%.16lx, region num = 0x%.16lx\n",
          (unsigned long)pa ,
          (unsigned long)((pa >> self->block_size_log2) & (self->region_size -1)));
      */

    return (pa >> self->block_size_log2) & (REGION_SIZE - 1);
}

int md1_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    /*printf("pa: 0x%.16lx, md1_region = 0x%.16lx\n",
            (unsigned long)pa ,
            (unsigned long)((pa >> self->tag_shift) & (self->md1_size -1)));
    */
    return (pa >> self->tag_shift) & (self->md1_size - 1);
}

int md2_index_from_pa(avdark_cache_core *self, avdc_pa_t pa) {
    return (pa >> self->tag_shift) & (self->md2_size - 1);
}

int md3_index_from_pa(avdc_cache *self, avdc_pa_t pa) {
    return (pa >> self->cores[0]->tag_shift) & (self->md3_size - 1);
}

/**
 * Computes the log2 of a 32 bit integer value. Used in dc_init
 *
 * Do NOT modify!
 */
static int
log2_int32(uint32_t value) {
    int i;

    for (i = 0; i < 32; i++) {
        value >>= 1;
        if (value == 0)
            break;
    }
    return i;
}

/**
 * Check if a number is a power of 2. Used for cache parameter sanity
 * checks.

 */
static int
is_power_of_two(uint64_t val) {
    return ((((val) & (val - 1)) == 0) && (val > 0));
}

void
avdc_dbg_log(avdc_cache *self, const char *msg, ...) {
    va_list ap;

    if (self->dbg) {
        const char *name = self->dbg_name ? self->dbg_name : "AVDC";
        printf("[%s] dbg: ", name);
        va_start(ap, msg);
        vfprintf(stdout, msg, ap);
        va_end(ap);
    }
}

int check_l1(int li) {
    if (((li & L1_BASE) == L1_BASE) &&
        ((li & L2_BASE) != L2_BASE) &&
        ((li & L3_BASE) != L3_BASE)) {
        return 1;
    } else {
        return 0;
    }
}

int check_l2(int li) {
    if (((li & L1_BASE) != L1_BASE) &&
        ((li & L2_BASE) == L2_BASE) &&
        ((li & L3_BASE) != L3_BASE)) {
        return 1;
    } else {
        return 0;
    }
}

int check_l3(int li) {
    if ((li & L3_BASE) == L3_BASE) {
        return 1;
    } else {
        return 0;
    }
}

int check_mem(int li) {
    if (((li & L1_BASE) == L1_BASE) &&
        ((li & L2_BASE) == L2_BASE) &&
        ((li & L3_BASE) != L3_BASE)) {
        return 1;
    } else {
        return 0;
    }
}

int check_rem(int li) {
    if (((li & L1_BASE) != L1_BASE) &&
        ((li & L2_BASE) != L2_BASE) &&
        ((li & L3_BASE) != L3_BASE) &&
        (0 <= li) &&
        (li <= CORES)) {
        return 1;
    } else
        return 0;
}


void
avdc_access(avdc_cache *cache, avdc_pa_t pa, type_t type, int thread_id) {

    //map threads to cores
    thread_id = thread_id % CORES;
    int hit1 = -1, hit2 = -1, hit3 = -1; //hits for different md levels
    cache->accesses += 1;

    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa);
    int region_num = region_from_pa(cache->cores[0], pa);

//    if(tag == 4303120 && region_num == 15) {
//         printf("  core: %d\n", thread_id);
//    }

    hit1 = search_md1(cache, thread_id, pa, type);

    if (hit1 == 0) {
        //MD1 miss
        hit2 = search_md2(cache, thread_id, pa, type);

        if (hit2 == 0) {
            //MD2 miss
            hit3 = search_md3(cache, pa, thread_id, type);

            if (hit3 == 0) {
                ///CASE D.1 untracked to private
                avdc_insert_md3(cache, pa, thread_id, type); //only access if first time for whole region
            }
        }
    }

    //avdc_pa_t tag = tag_from_pa(cache->cores[thread_id], pa);
    switch (type) {

        case AVDC_READ: /* Read accesses */
            cache->cores[thread_id]->stat.data_read += 1;
            if (hit1 == 0 && hit2 == 0 && hit3 == 0) {
                cache->cores[thread_id]->stat.data_read_miss += 1;
            }
            break;

        case AVDC_WRITE: /* Write accesses */
            cache->cores[thread_id]->stat.data_write += 1;
            if (hit1 == 0 && hit2 == 0 && hit3 == 0)
                cache->cores[thread_id]->stat.data_write_miss += 1;
            break;
    }

}


//insert entry into md1 and md2, and, l1 cache and l2 cache from md3
int add_to_core_from_md3(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    int md2_way = create_md2(cache, thread_id, va);
    int md1_way = create_md1(cache, thread_id, va, md2_way);

    /**
     *  okay here grab the active md (which pretty mch has to be md1 and overwrite the LI info
     */

    avdc_md3_t *md3 = search_md3_entry(cache, va, 0);
    avdc_md_t *active_md = search_active_md(cache, thread_id, va);

    if (md3 == NULL)
        return -1;
    if (active_md == NULL)
        return -1;

    if (md3->valid == 1 && md3->active == 1) {
        //copy LI info over from MD3 to activ_MD

        /**
         * there could be a check here to see if the private bit needs to be set
         */


        for (int i = 0; i < REGION_SIZE; i++) {
            active_md->li[i] = md3->li[i];
        }
    }

    //add to l2 and L1

    int l2_way = add_to_l2(cache, thread_id, va, md2_way);
    int l1_way = add_to_l1(cache, thread_id, va, md2_way, l2_way);

    //add core to PB in MD3

    //TODO test this please please please it s new

    md3_validate_pb(cache, thread_id, va);

    return 1;
}

void md3_validate_pb(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdc_md3_t *md3 = search_md3_entry(cache, va, 1);   // boo

    avdc_md_t *active_md = search_active_md(cache, thread_id, va);

    int pb = md3->pb;

    //this is how you set the private biut if it's zero set it to private

    // if it's shared set it to zero and maybe double check the other cores are also set to zero
    // could check the case where things go from private to shared becasue that's where it should happen

    //okay now we have to deal with private bits in the local cores

    if (pb == 0) {
        //don't need to do anything the PB should already be set to 1

    } else if (number_of_pb(pb) == 1) {

        /***
         *      okay here i think we could go into a shared state but for right now I'm handling this as an invalidation
         */
        int rem_thread_id = get_thread(pb);
        if (thread_id != rem_thread_id) {
            invalidate_core_cacheline(cache, rem_thread_id, va, thread_id);
            active_md->private_bit = 0;
        }

    } else if (number_of_pb(pb) == 2) {

        //everone points to me now
        invalidate_cachline(cache, thread_id, va);
        active_md->private_bit = 0;

    }

    md3->pb = set_pb(thread_id) | md3->pb;

}

void invalidate_core_cacheline(avdc_cache *cache, int thread_id, avdc_pa_t va, int new_thread) {
    avdark_cache_core *rem_core = cache->cores[thread_id];
    int region_num = region_from_pa(rem_core, va);

    avdc_md_t *rem_active_md = search_active_md(cache, thread_id, va);
    int li = rem_active_md->li[region_num];

    //decode li to find out if data is in l1 or l2
    if (check_l1(li)) {
        clear_l1_entry_va(rem_core, va);
        clear_l2_entry_va(rem_core, va); //if it's in l1 then inclusively has to be in l2
    } else if (check_l2(li)) {
        clear_l2_entry_va(rem_core, va);
    } else if (check_l3(li)) {
        //this is not an error it's a false invalidation because another egion in the core is local and this was evicted
        //assert_debug(cache, thread_id, 1, 0, "***err data remote core points to local data but local core points to L3 invalidate_core_cacheline\n", 1);

        /**
         * maybe clear the L3 just to be sure though
         *
         */

        clear_l3_entry_va(cache, va);

    } else if ((check_mem(li))) {
        //this is okay
        //assert_debug(cache, thread_id, 1, 0, "***err data remote core points to local data but local core points to mem: invalidate_core_cacheline\n", 1);
    } else if (check_rem(li)) {
        //this is also okay if triggered by a write
        //assert_debug(cache, thread_id, 1, 0, "***err data remote core points to local data but local core points to remote core: invalidate_core_cacheline\n", 1);
    } else {
        assert_debug(cache, -1, 1, 0, "***err: severe error in li encoding tolken 2948\n", 1);
    }

    //update local MD
    rem_active_md->li[region_num] = new_thread;

}


/**
 * go through the entiure cache and invalidate all copies of the cachline of the va
 * @param cache
 * @param thread_id the calling thread which isn't invalidated, or -1 to invalidate all threads
 * @param va
 */

void invalidate_cachline(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    int region_num = region_from_pa(cache->cores[0], va);

    avdc_md3_t *md3 = search_md3_entry(cache, va, 2);

    //loop through pb and invalidate all cachlines
    int pb = md3->pb;
    for (int i = 0; i < CORES; i++) {
        if (((int) pow((double) 2, i) & pb) && i != thread_id) { //if the pb is a match for the core
            avdark_cache_core *rem_core = cache->cores[i];

            avdc_md_t *active_md = search_active_md(cache, i, va);


            //Update the LI to point either to the new master core or memory
            int li = -1;
            li = active_md->li[region_num];
            if (thread_id != -1) {
                active_md->li[region_num] = thread_id;
                active_md->private_bit = 0; //maybe this can be here
            } else {
                active_md->li[region_num] = MEM_BASE;
            }
            //active_md->private_bit = 0;  //if there are problems with private bits try putting this back

            //decode li to find out if data is in l1 or l2
            if (check_l1(li)) {

                clear_l1_entry_va(rem_core, va);
                clear_l2_entry_va(rem_core, va); //if it's in l1 then inclusively has to be in l2

            } else if (check_l2(li)) {

                clear_l2_entry_va(rem_core, va);

            } else if ((check_l3(li))) {

                //TODO: why did this get commented out
<<<<<<< HEAD
//                clear_l3_entry(cache, l3_index, l3_way);
=======
                clear_l3_entry_va(cache, va);
>>>>>>> temp

            } else if ((check_mem(li))) {
                //              assert_debug(cache, thread_id, 1, 0, "***err: local core points to memory: invalidate_cachline \n", 1);
                //maybe add a statistic

            } else if (check_rem(li)) {

                //okay if this is a write it's not an error

                //assert_debug(cache, thread_id, 1, 0, "***err: local core points to remote core: invalidate_cachline \n", 1);
                //TODO: maybe an interesting statistic
                ///this code executes for how often a remote node is invalided unnessessarily becasue another node has the data

            } else {
                //or maybe the data wasn't there in the first place
                assert_debug(cache, thread_id, 1, 0, "***err: severe error in li encoding tolken 2948\n", 1);
            }
        }
        //TODO: how do regions get back to private once they are shared?
        // is there a check to see when a core has the only copies of the data
    }

}

/**
avdc_insert_into_md_region is called to bring an entry in from memory and add it's data to the cache
the md1 or md2 entry already exists and the data just needs to be added to l1 and/or l2
*/

int avdc_insert_into_md_region(avdc_cache *cache, int thread_id, avdc_pa_t va, int md_level, int md_way) {


    //okay we have an active md
    avdc_md_t *active_md = search_active_md(cache, thread_id, va);

    avdark_cache_core *core = cache->cores[thread_id];
    int md2_way = search_md2_way(cache, core, va);
    int l2_way = add_to_l2(cache, thread_id, va, md2_way);
    int l1_way = add_to_l1(cache, thread_id, va, md2_way, l2_way);

    //data can come from l3
    // mem
    // remote

    /**
     *  TODO but we also have to invalidate this in the other cores
     *   yes this could be improved, it doesn stuff it doesn't need to
     */

    md3_validate_pb(cache, thread_id, va);

    /***
     *
     * TODO: i'm pretty sure this should also check if there are shared things
     *
     * add number_of_pb == 2
     *
     */

    return 1;
}

void
avdc_flush_cache(avdark_cache_core *self) {
    for (int i = 0; i < self->number_of_l1_sets; i++) {
        for (int k = 0; k < L1_ASSOC; k++) {

            self->l1[i][k].valid = 0;
            self->l1[i][k].active = 0;
            self->l1[i][k].address = 0;
            self->l1[i][k].tracker_pointer = 0;
            self->l1[i][k].replacement_pointer = 0;
            self->l1[i][k].timestamp = 0;
        }
    }

    for (int i = 0; i < self->number_of_l2_sets; i++) {
        for (int k = 0; k < L2_ASSOC; k++) {
            self->l2[i][k].valid = 0;
            self->l2[i][k].active = 0;
            self->l2[i][k].address = 0;
            self->l2[i][k].tracker_pointer = 0;
            self->l2[i][k].replacement_pointer = 0;
            self->l2[i][k].timestamp = 0;
        }
    }

    for (int i = 0; i < self->md1_size; i++) {
        for (int j = 0; j < MD1_ASSOC; j++) {
            self->md1[i][j].tag = 0;
            self->md1[i][j].valid = 0;
            self->md1[i][j].active = 0;
            self->md1[i][j].timestamp = 0;
            self->md1[i][j].tracker_pointer = 0;
            int g;
            for (g = 0; g < self->region_size; g++) {
                self->md1[i][j].address[g] = 0;
                self->md1[i][j].li[g] = 0;
            }
        }
    }

    for (int i = 0; i < self->md2_size; i++) {
        for (int j = 0; j < MD2_ASSOC; j++) {
            self->md2[i][j].tag = 0;
            self->md2[i][j].valid = 0;
            self->md2[i][j].active = 0;
            self->md2[i][j].timestamp = 0;
            self->md2[i][j].tracker_pointer = 0;
            int g;
            for (g = 0; g < self->region_size; g++) {
                self->md2[i][j].address[g] = 0;
                self->md2[i][j].li[g] = 0;
            }
        }
    }

    self->number_of_l1_entries = 0;
    self->number_of_l2_entries = 0;
    self->md1_entries = 0;
    self->md2_entries = 0;

    //set all stats to 0
    self->stat.data_write = 0;
    self->stat.data_write_miss = 0;
    self->stat.data_read = 0;
    self->stat.data_read_miss = 0;
    self->stat.md1_hit = 0;
    self->stat.md1_l1_hit = 0;
    self->stat.md1_l2_hit = 0;
    self->stat.md1_ns_hit = 0;
    self->stat.md1_fs_hit = 0;
    self->stat.md1_rem_hit = 0;
    self->stat.md1_mem_hit = 0;
    self->stat.md1_region_hit = 0;
    self->stat.md2_region_hit = 0;
    self->stat.md2_hit = 0;
    self->stat.md2_l1_hit = 0;
    self->stat.md2_l2_hit = 0;
    self->stat.md2_ns_hit = 0;
    self->stat.md2_fs_hit = 0;
    self->stat.md2_rem_hit = 0;
    self->stat.md2_mem_hit = 0;
    self->stat.md2_mem_miss = 0;
    self->stat.md3_hit = 0;
    self->stat.md3_l1_hit = 0;
    self->stat.md3_l2_hit = 0;
    self->stat.md3_ns_hit = 0;
    self->stat.md3_fs_hit = 0;
    self->stat.md3_rem_hit = 0;
    self->stat.md3_mem_hit = 0;
}

void
avdc_flush_cache_full(avdc_cache *self) {

    for (int i = 0; i < CORES; i++) {
        avdc_flush_cache(self->cores[i]);
    }
    for (int i = 0; i < self->l3_sets; i++) {
        for (int k = 0; k < self->l3_assoc; k++) {
            self->l3[i][k].valid = 0;
            self->l3[i][k].active = 0;
            self->l3[i][k].address = 0;
            self->l3[i][k].tracker_pointer = 0;
            self->l3[i][k].replacement_pointer = 0;
            self->l3[i][k].timestamp = 0;

        }
    }

    for (int i = 0; i < self->md3_size; i++) {

        for (int j = 0; j < self->md3_assoc; j++) {
            self->md3[i][j].tag = 0;
            self->md3[i][j].valid = 0;
            self->md3[i][j].active = 0;
            self->md3[i][j].timestamp = 0;
            self->md3[i][j].pb = 0;

            int g;
            for (g = 0; g < self->cores[0]->region_size; g++) {
                self->md3[i][j].address[g] = 0;
                self->md3[i][j].li[g] = 0;
            }
        }
    }
    /*
    self->number_of_l3_entries = 0;
    self->md3_entries = 0;
     */

    self->stat3.md3_hit = 0;
    self->stat3.md3_l1_hit = 0;
    self->stat3.md3_l2_hit = 0;
    self->stat3.md3_l3_hit = 0;
    self->stat3.md3_ns_hit = 0;
    self->stat3.md3_fs_hit = 0;
    self->stat3.md3_rem_hit = 0;
    self->stat3.md3_mem_hit = 0;
    self->stat3.md3_mem_miss = 0;

}


int
avdc_resize(avdark_cache_core *core,
            avdc_size_t size,
            avdc_block_size_t block_size,
            avdc_assoc_t assoc,
            avdc_region_t region_size) {

    /* Verify that the parameters are sane */
    if (!is_power_of_two(size) ||
        !is_power_of_two(block_size) ||
        !is_power_of_two(region_size)) {
        printf("size, block-size, assoc and region size all have to be powers of two and > zero\n");
        return 0;
    }

    /* Update the stored parameters */
    core->l1_size = L1_SIZE;
    core->block_size = BLOCK_SIZE; //TODO: make this dynamic
    //core->assoc = ASSOC;
    core->region_size = REGION_SIZE; //TODO: make this dynamic
    core->l2_size = L1_SIZE * 8; //TODO-fix: 8 times bigger is the spec
    /* Save some common values */
    core->number_of_l1_sets = (core->l1_size / core->block_size) / L1_ASSOC;
    core->max_l1_entries = core->number_of_l1_sets * L1_ASSOC;
    core->number_of_l2_sets = (core->l2_size / core->block_size) / L2_ASSOC;
    core->block_size_log2 = log2_int32(core->block_size);
    core->region_shift = log2_int32(core->region_size);
    core->tag_shift = core->block_size_log2 + core->region_shift;
    core->md1_size = MD1_SIZE;
    core->md2_size = MD2_SIZE;

    /* (Re-)Allocate space for the tags array */
    if (core->l1)
        AVDC_FREE(core->l1);
    core->l1 = (avdc_cache_line_t **) AVDC_MALLOC(core->number_of_l1_sets, avdc_cache_line_t*);
    for (int i = 0; i < core->number_of_l1_sets; i++) {
        core->l1[i] = (avdc_cache_line_t *) AVDC_MALLOC(L1_ASSOC, avdc_cache_line_t);
    }

    if (core->md1)
        AVDC_FREE(core->md1);
    core->md1 = (avdc_md_t **) AVDC_MALLOC(core->md1_size, avdc_md_t*);
    for (int i = 0; i < core->md1_size; i++) {
        core->md1[i] = (avdc_md_t *) AVDC_MALLOC(MD1_ASSOC, avdc_md_t);
    }

    if (core->md2)
        AVDC_FREE(core->md2);
    core->md2 = (avdc_md_t **) AVDC_MALLOC(core->md2_size, avdc_md_t*);
    for (int i = 0; i < core->md2_size; i++) {
        core->md2[i] = (avdc_md_t *) AVDC_MALLOC(MD2_ASSOC, avdc_md_t);
    }

    if (core->l2)
        AVDC_FREE(core->l2);
    core->l2 = (avdc_cache_line_t **) AVDC_MALLOC(core->number_of_l2_sets, avdc_cache_line_t*);
    for (int i = 0; i < core->number_of_l2_sets; i++) {
        core->l2[i] = (avdc_cache_line_t *) AVDC_MALLOC(L2_ASSOC, avdc_cache_line_t);
    }

    /* Flush the cache, this initializes the tag array to a known state */
    avdc_flush_cache(core);

    return 1;
}

void
avdc_reset_statistics(avdark_cache_core *self) {
    self->stat.data_read = 0;
    self->stat.data_read_miss = 0;
    self->stat.data_write = 0;
    self->stat.data_write_miss = 0;

    self->stat.md1_hit = 0;
    self->stat.md1_l1_hit = 0;
    self->stat.md1_l2_hit = 0;

    self->stat.md2_hit = 0;
    self->stat.md2_l1_hit = 0;
    self->stat.md2_l2_hit = 0;

}

avdark_cache_core *avdc_new(avdc_size_t size, avdc_block_size_t block_size,
                            avdc_assoc_t assoc, avdc_region_t region_size) {
    avdark_cache_core *core;

    core = AVDC_MALLOC(1, avdark_cache_core);

    memset(core, 0, sizeof(*core));

    if (!avdc_resize(core, size, block_size, assoc, region_size)) {
        AVDC_FREE(core);
        return NULL;
    }

    return core;
}

avdc_cache *avdc_new_multi_core(avdc_size_t size, avdc_block_size_t block_size,
                                avdc_assoc_t assoc, avdc_region_t region_size) {
    //assert(assoc == ASSOC);
    //assert(region_size == REGION_SIZE);

    avdc_cache *cache;
    cache = AVDC_MALLOC(1, avdc_cache);
    memset(cache, 0, sizeof(*cache));
    cache->dbg = 0;

    //printf("block size:\t %d\n", BLOCK_SIZE );

    printf("region size:\t %d\n", REGION_SIZE);

    printf("l1 ASSOC:\t %d\n", L1_ASSOC);
    printf("md1 ASSOC:\t %d\n", MD1_ASSOC);
    printf("md2 ASSOC:\t %d\n", MD2_ASSOC);
    printf("md3 ASSOC:\t %d\n", MD3_ASSOC);

    for (int i = 0; i < CORES; i++) {
        cache->cores[i] = avdc_new(32768, 64, MD1_ASSOC, REGION_SIZE);
    }

    cache->l3_size = size * 128; //TODO-fix: 128 times bigger is the spec
    cache->md3_assoc = MD3_ASSOC; //todo: make dynamic
    cache->l3_assoc = L3_ASSOC;
    cache->accesses = 0;
    cache->md3_size = MD3_SIZE;
    cache->block_size = block_size;
    cache->l3_sets = (cache->l3_size / cache->block_size) / cache->l3_assoc;

    if (cache->l3)
        AVDC_FREE(cache->l3);
    cache->l3 = (avdc_cache_line_t **) AVDC_MALLOC(cache->l3_sets, avdc_cache_line_t*);
    for (int i = 0; i < cache->l3_sets; i++) {
        cache->l3[i] = (avdc_cache_line_t *) AVDC_MALLOC(cache->l3_assoc, avdc_cache_line_t);
    }

    if (cache->md3)
        AVDC_FREE(cache->md3);
    cache->md3 = (avdc_md3_t **) AVDC_MALLOC(cache->md3_size, avdc_md3_t*);
    for (int i = 0; i < cache->md3_size; i++) {
        cache->md3[i] = (avdc_md3_t *) AVDC_MALLOC(cache->l3_assoc, avdc_md3_t);
    }

    //avdc_flush_cache_full(cache);

    return cache;
}


void
avdc_delete(avdark_cache_core *self) {
    if (self->l1) {
        for (int i = 0; i < self->number_of_l1_sets; i++) {
            if (self->l1[i]) {
                AVDC_FREE(self->l1[i]);
            }
        }
        AVDC_FREE(self->l1);
    }

    if (self->l2) {
        for (int i = 0; i < self->number_of_l2_sets; i++) {
            if (self->l2[i]) {
                AVDC_FREE(self->l2[i]);
            }
        }
        AVDC_FREE(self->l2);
    }

    // free md1 and md2 like this
    if (self->md1) {
        for (int i = 0; i < self->md1_size; i++) {
            if (self->md1[i]) {
                AVDC_FREE(self->md1[i]);
            }
        }
        AVDC_FREE(self->md1);
    }

    if (self->md2) {
        for (int i = 0; i < self->md2_size; i++) {
            if (self->md2[i])
                AVDC_FREE(self->md2[i]);
        }
        AVDC_FREE(self->md2);
    }

    AVDC_FREE(self);
}

void
multi_core_delete(avdc_cache *self) {
    //delete all cores
    for (int i = 0; i < CORES; i++) {
        avdc_delete(self->cores[i]);
    }

    if (self->l3)
        AVDC_FREE(self->l3);

    // free md3
    if (self->md3) {
        for (int i = 0; i < self->md3_size; i++) {
            if (self->md3[i]) {
                AVDC_FREE(self->md3[i]);
            }
        }
        AVDC_FREE(self->md3);
    }

    AVDC_FREE(self);
}

int search_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, type_t type) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    int region_num = region_from_pa(core, va);

    int md1_way = search_md1_way(core, va);
    if (md1_way != -1) {

        avdc_md_t *md1 = search_md1_entry(cache, thread_id, va); //new

        if ((md1->tag == tag) &&
            (md1->valid == 1) &&
            (md1->active == 1)) {

            int li = md1->li[region_num];
            assert_addr(cache, thread_id, &md1->address[region_num], va, 7);
            core->stat.md1_hit += 1;

//            if(tag == 4303120 && region_num == 15) {
//                printf("  md1 hit LI: %d\n", li);
//            }

            if (check_l1(li)) {
                //if the tag matches and pos is valid then extract region for cache line info

                core->stat.md1_l1_hit += 1;
                int l1_way = li & (L1_BASE - 1);
                int l1_way_val = search_l1_way(core, va);

                assert_debug(cache, thread_id, l1_way, l1_way_val, "***err l1_way problem in search_md1\n", 1);

                avdc_cache_line_t *l1 = search_l1_entry(cache, thread_id, va);

                assert_debug(cache, thread_id, l1->active, 1, "***err L1 not active, md1_search, l1 hit\n", 1);
                assert_debug(cache, thread_id, l1->valid, 1, "***err L1 not valid, md1_search, l1 hit\n", 1);
                assert_addr(cache, thread_id, &l1->address, va, 8);


                if ((md1->private_bit == 1 && type == AVDC_WRITE) ||
                    (md1->private_bit == 1 && type == AVDC_READ) ||
                    (md1->private_bit == 0 && type == AVDC_READ)) {

                    //simply return
                    return 1;
                } else if (md1->private_bit == 0 && type == AVDC_WRITE) {

                    //in this case you have to remove the data form the other cores
                    invalidate_cachline(cache, thread_id, va);
                    return 1;
                }

            } else if (check_l2(li)) {

                core->stat.md1_l2_hit += 1;
                int l2_way = li & (L2_BASE - 1);
                int l2_way_val = search_l2_way(core, va);
                assert_debug(cache, thread_id, l2_way, l2_way_val, "***err l2_way problem in search_md1\n", 1);

                avdc_cache_line_t *l2 = search_l2_entry(cache, thread_id, va);
                assert_debug(cache, thread_id, l2->valid, 1, "***err L2 not valid, md1_search, l2 hit\n", 1);
                assert_debug(cache, thread_id, l2->active, 1, "***err L2 not active, md1_search, l2 hit\n", 1);
                assert_addr(cache, thread_id, &l2->address, va, 9);

                copy_l2_to_l1(cache, thread_id, va, 1); //test this

                if ((md1->private_bit == 1 && type == AVDC_WRITE) ||
                    (md1->private_bit == 1 && type == AVDC_READ) ||
                    (md1->private_bit == 0 && type == AVDC_READ)) {

                    return 1;
                } else if (md1->private_bit == 0 && type == AVDC_WRITE) {

                    invalidate_cachline(cache, thread_id, va);
                    return 1;
                }
            } else if (check_mem(li)) {

<<<<<<< HEAD
//                    if(type == AVDC_READ) {
//                        //TODO-done: don't access md3 go straight to the source
//                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
//
//                        //TODO-done: and update MD3
//                        copy_md_to_md3(cache, thread_id, va); //test this
//
//                    } else if(type == AVDC_WRITE) {
                        //FROMERIK: invalidate only the cache line on write
                        //invalidate
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
//                    }
                }
                return 1; //TODO-question why wasn't this here before
=======
                core->stat.md1_mem_hit += 1;

                //CASE A: Read Miss, MD1/MD2 Hit
                //CASE B: Write Miss, Private Region, MD1/MD2 hit

                //if in memory and region is private add to region - addendum might not have to be private, no maybe should be, not sure
                avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);

                return 1;
>>>>>>> temp
            } else if (check_rem(li)) {

                assert_debug(cache, thread_id, md1->private_bit, 0,
                             "***err md1 private bit not shared and data found in remote core, search_md1\n", 1);
                core->stat.md1_rem_hit += 1;

<<<<<<< HEAD
                //TODO-done: check if read or write, i think if it's write you have to invalidate,
                //todo-cont: if it's a read you can maybe allow both copies
//                if(type == AVDC_READ) {
//                    //TODO-done: don't access md3 go straight to the sorce
//                    //install untracked data into md1 and md2
//                    copy_from_remote_node(cache, thread_id, li, va); //TODO: test this
//
//                } else if(type == AVDC_WRITE) {
=======
                //if it's a read you can duplicate data
                if (type == AVDC_READ) {
                    // don't access md3 go straight to the sorce
                    copy_from_remote_node(cache, thread_id, li, va);

                } else if (type == AVDC_WRITE) {
>>>>>>> temp
                    //invalidate
                    invalidate_cachline(cache, thread_id, va);
                    avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
//                }

                return 1;
            } else if (check_l3(li)) {

                core->stat.md1_l3_hit += 1;

                avdc_cache_line_t *l3 = search_l3_entry(cache, va);

                assert_debug(cache, thread_id, l3->active, 1, "***err l3 not active, search_md1\n", 1);
                assert_debug(cache, thread_id, l3->valid, 1, "***err l3 not valid, search_md1\n", 1);
                assert_addr(cache, thread_id, &l3->address, va, 10);

<<<<<<< HEAD
                //insert data into requesting node
                if (md1->private_bit == 1) {
                    //if in memory and region is private add to region - addendum might not have to be private, no maybe should be, not sure
                    //TODO: check if this returns 2
                    return avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
                } else {
                    //TODO
                    //find master location

//                    if(type == AVDC_READ) {
//                        //TODO-done: don't access md3 go straight to the source
//                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way); //this kinda worked
//                        copy_md_to_md3(cache, thread_id, va); //test this
//
//                    } else if(type == AVDC_WRITE) {
                        //FROMERIK: invalidate only the cache line on write
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
//                    }
                }
=======
                avdc_insert_into_md_region(cache, thread_id, va, 1, md1_way);
                //clear md in add_to_l1 now
                //clear_l3_entry_va(cache, va); //TODO: question does this need to be added
>>>>>>> temp
                return 1;
            } else {
                assert_debug(cache, thread_id, 1, 0, "***err: severe error in li encoding md1 in search_md1\n", 1);
            }
        }
    }
    return 0;
}

int search_md1_way(avdark_cache_core *core, avdc_pa_t pa) {
    //if entry already exists return it

    int md1_index = md1_index_from_pa(core, pa);
    avdc_pa_t tag = tag_from_pa(core, pa);
    int i;
    for (i = 0; i < MD1_ASSOC; i++) {
        if (core->md1[md1_index][i].tag == tag &&
            core->md1[md1_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_md1_way(avdc_cache *cache, int thread_id, avdc_pa_t pa) {

    avdark_cache_core *core = cache->cores[thread_id];

    int md1_index = md1_index_from_pa(core, pa);
    avdc_pa_t tag = tag_from_pa(core, pa);

    int i;
    //find an entry that isn't valid yet use that one
    for (i = 0; i < MD1_ASSOC; i++) {
        if (core->md1[md1_index][i].valid == 0) {
            return i;
        }
    }

    if (CAP_PRINT && (cap_array[3] == 0)) {
        printf("***info: MD1 CAP\n");
        cap_array[3] = 1;
    }

    //if all entries are taken, select one for eviction then remove everything
    int LRU_way = 0;
    double diff_time = 0;
<<<<<<< HEAD
    for(i=0; i < MD1_ASSOC; i++){
=======
    for (i = 1; i < MD1_ASSOC; i++) {
>>>>>>> temp
        diff_time = difftime(core->md1[md1_index][LRU_way].timestamp,
                             core->md1[md1_index][i].timestamp);
        if (diff_time > 0)
            LRU_way = i;
    }
    //int LRU_way now has the smallest index

    copy_md1_to_md2(cache, thread_id, pa, md1_index, LRU_way);

    return LRU_way;
}

/*
 * this function copies data from one core to another core
 *
 * tailored for case a read miss, md1/md2 hit going to a remote node
 *
 * @li is the remote core
 *
 */
void copy_from_remote_node(avdc_cache *cache, int thread_id, int li, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];
    avdark_cache_core *rem_core = cache->cores[li];

    int rem_thread_id = li;

    //TODO: assert shared in md3 maybe

    avdc_md_t *active_md = search_active_md(cache, thread_id, va);
    avdc_md_t *active_rem_md = search_active_md(cache, rem_thread_id, va);

    //    assert(active_rem_md->private_bit == 0);
    assert_debug(cache, thread_id, active_rem_md->private_bit, 0,
                 "***err active_rem_md->private_bit not zero: copy_from_remote_node\n", 1); //boo 1

    if (active_md->private_bit == 1)
        active_md->private_bit = 0;

    if (active_rem_md->private_bit == 1)
        active_rem_md->private_bit = 0;

    /***
     *     TODO: when this breaks that means the region is in a private state when it shoulnd't be
     */

    /**
     *  what we're doing is sharing here no invalidation
     */



    //assert that data in remote core is in l1 or l2
    int region_num = region_from_pa(core, va);
    int rem_li = active_rem_md->li[region_num];

    if (check_l1(rem_li)) {   // there was amistake here using just the normal li

        avdc_cache_line_t *l1 = search_l1_entry(cache, rem_thread_id, va);

        assert_debug(cache, thread_id, l1->active, 1, "***err L1 not active, copy_from_remote_node, l1 hit\n", 1);
        assert_debug(cache, thread_id, l1->valid, 1, "***err L1 not valid, copy_from_remote_node, l1 hit\n", 1);

        assert_addr(cache, li, &l1->address, va, 1818);  // boo 3 285

    } else if (check_l2(rem_li)) {  // there was amistake here using just the normal li

        avdc_cache_line_t *l2 = search_l2_entry(cache, rem_thread_id, va);

        assert_debug(cache, thread_id, l2->active, 1, "***err L2 not active, copy_from_remote_node, l2 hit\n", 1);
        assert_debug(cache, thread_id, l2->valid, 1, "***err L2 not valid, copy_from_remote_node, l2 hit\n", 1);

        assert_addr(cache, li, &l2->address, va, 118); // boo 4 285
    } else if (check_l3(li)) {
        //error
        assert_debug(cache, -1, 1, 0, "***error data not found in local core, pointing to l3 copy_from_remote_node\n",
                     1); //boo 2
    } else if (check_mem(li)) {
        //error
        assert_debug(cache, -1, 1, 0, "***error data not found in local core, pointing to mem copy_from_remote_node\n",
                     1); //boo 2
    } else if (check_rem(li)) {
        //error
        /**
         *      TODO check to see if this is an error or not i thikn it might not be if it's a write, becasue you can have a shared copy and then upgrade to write
         */

        //assert_debug(cache, -1, 1, 0, "***error data not found in local core, pointing to rem core copy_from_remote_node\n", 1); //boo 2
    } else {
        assert_debug(cache, -1, 1, 0, "***error data not found in local core copy_from_remote_node\n", 1); //boo 2

    }

    //install data core
    int md2_way = search_md2_way(cache, core, va);
    int l2_way = add_to_l2(cache, thread_id, va, md2_way);
    add_to_l1(cache, thread_id, va, md2_way, l2_way);

    avdc_md3_t *md3 = search_md3_entry(cache, va, 3);
    assert_debug(cache, -1, md3->valid, 1, "***err md3 not valid copy_from_remote_node\n", 1);
    assert_debug(cache, -1, md3->active, 1, "***err md3 not active copy_from_remote_node\n", 1);


    //I don't thikn this line needs to be here
    md3->pb = md3->pb | set_pb(thread_id) | set_pb(li); //whoa no idea if this works need to test
}


void copy_md1_to_md2(avdc_cache *cache, int thread_id, avdc_pa_t pa, int md1_index, int LRU_way) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_md_t *LRU_md1 = &core->md1[md1_index][LRU_way];

    avdc_pa_t LRU_va = LRU_md1->address[0]; //default use first address in region
    int region_num = region_from_pa(core, LRU_va);

    int LRU_md1_index = md1_index_from_pa(core, LRU_va);
    int LRU_md2_index = md2_index_from_pa(core, LRU_va);

    int LRU_md2_way = LRU_md1->tracker_pointer;

    avdc_md_t *LRU_md2 = &core->md2[LRU_md2_index][LRU_md2_way];

    //assert(LRU_md1->valid == 1);
    //assert(LRU_md1->active == 1);
    assert_debug(cache, thread_id, LRU_md1->valid, 1, "***err LRU_md1 not valid, copy_md1_to_md2\n", 1);
    assert_debug(cache, thread_id, LRU_md1->active, 1, "***err LRU_md1 not active, copy_md1_to_md2\n", 1);

    assert_addr(cache, thread_id, &LRU_md1->address[region_num], LRU_va, 11); //dumb assert
    assert_addr(cache, thread_id, &LRU_md2->address[region_num], LRU_va, 12);

    *LRU_md2 = *LRU_md1; //whoa
    //LRU_md1->valid = 0;
    LRU_md1->active = 0; //this prevent data from being deleted
    LRU_md2->valid = 1;
    LRU_md2->active = 1;
    LRU_md2->tracker_pointer = -1;
    LRU_md2->timestamp = clock();

    clear_md1(cache, thread_id, LRU_md1_index, LRU_way, 0);
}

/**
 *
 * @param cache
 * @param thread_id
 * @param md1_index
 * @param md1_way
 * @param mode mode-> invaldiate in other cores, mode->1 just erase this one
 */
void clear_md1(avdc_cache *cache, int thread_id, int md1_index, int md1_way, int mode) {

    if (md1_way == -1)
        return;

    avdark_cache_core *core = cache->cores[thread_id];
    avdc_md_t *md1 = &core->md1[md1_index][md1_way];

    //assert_debug(cache, thread_id, md1->active, 1, "***err md1 not active in clear md1\n", 1); //check
    assert_debug(cache, thread_id, md1->valid, 1, "***err md1 not valid in clear md1\n", 1); //check

    if (md1->active == 1 && md1->valid == 1) {

        for (int i = 0; i < REGION_SIZE; i++) {
            int li = md1->li[i];
            avdc_pa_t va = md1->address[i];

            if (check_l1(li)) {
                clear_l1_entry_va(core, va);
                clear_l2_entry_va(core, va);
            } else if (check_l2(li)) {
                clear_l2_entry_va(core, va);
            } else if (check_l3(li)) {
                //don't need to do anything i don't think
                //maybe notify md3
            } else if (check_mem(li)) {
                //don't do anything
            } else if (check_rem(li)) {
                //maybe send a notification to the remote node or to md3
                //to be removed from the pb
                if(mode == 0) {
                    invalidate_cachline(cache, -1, va);
                }
            } else {
                assert_debug(cache, thread_id, 1, 0, "***err in LI encoding_clear_md1\n", 1);
            }

        }
    }

    md1->valid = 0;
    md1->active = 0;
    md1->tag = 0;
    md1->timestamp = 0; //maybe this breaks things
    md1->private_bit = 0;
    md1->tracker_pointer = -1;

    for (int g = 0; g < REGION_SIZE; g++) {
        md1->address[g] = 0;
        md1->li[g] = 0;
    }

}


int search_md2(avdc_cache *cache, int thread_id, avdc_pa_t va, type_t type) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    int region_num = region_from_pa(core, va);
    int md2_index = md2_index_from_pa(core, va);
    int l1_index = l1_index_from_pa(core, va);
    int l2_index = l2_index_from_pa(core, va);
    int l1_way;

    int md2_way = search_md2_way(cache, core, va);

    if (md2_way != -1) {

        avdc_md_t *md2 = &core->md2[md2_index][md2_way];

        if ((md2->tag == tag) &&
            (md2->valid == 1) &&
            (md2->active == 1)) {

            int li = md2->li[region_num];
            assert_addr(cache, thread_id, &md2->address[region_num], va, 13);
            core->stat.md2_hit += 1;

            if (check_l1(li)) {

                //update internal statistics:
                core->stat.md2_l1_hit += 1;

                l1_way = li & (L1_BASE - 1);
                avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
                //assert(l1->active == 1);
                //assert(l1->valid == 1);
                assert_debug(cache, thread_id, l1->active, 1, "***err L1 not active, md2_search, l1 hit\n", 1);
                assert_debug(cache, thread_id, l1->valid, 1, "***err L1 not valid, md2_search, l1 hit\n", 1);

                assert_addr(cache, thread_id, &l1->address, va, 14);

//                if( (md2->private_bit == 1 && type == VDC_WRITE) ||
//                    (md2->private_bit == 1 && type == AVDC_READ) ||
//                    (md2->private_bit == 0 && type == AVDC_READ) ){ //reads are always safe and if it's private it doesn't matter
//
//                    //do not return until md2 is moved to l1
//
//                } else
                if (md2->private_bit == 0 && type == AVDC_WRITE) { //then you have to go to md3
                    invalidate_cachline(cache, thread_id, va);
                }
            } else if (check_l2(li)) {

                //update internal statistics:
                core->stat.md2_l2_hit += 1;

                int l2_way = li & (L2_BASE - 1);
                avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

                assert_debug(cache, thread_id, l2->active, 1, "***err L2 not active, md2_search, l2 hit\n", 1);
                assert_debug(cache, thread_id, l2->valid, 1, "***err L2 not valid, md2_search, l2 hit\n", 1);

                assert_addr(cache, thread_id, &l2->address, va, 15);

                //move cacheline to L1 on L2 hit
                copy_l2_to_l1(cache, thread_id, va, 2);

//                if( (md2->private_bit == 1 && type == AVDC_WRITE) ||
//                    (md2->private_bit == 1 && type == AVDC_READ) ||
//                    (md2->private_bit == 0 && type == AVDC_READ)) { //reads are always safe and if it's private it doesn't matter
//
//                    //do not return until md2 is moved to l1
//
//                } else

<<<<<<< HEAD
                }else {
                    //TODO
                    //find master location
//                    if(type == AVDC_READ) {
//                        //TODO-done: don't access md3 go straight to the sorce
//                        //install untracked data into md1 and md2
//
//                        //TODO-question-erik: hits from memory can just beadded to the region yeah?
//                        //copy_from_remote_node(cache, thread_id, li, va);
//                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
//
//                        //TODO: and update MD3
//                        copy_md_to_md3(cache, thread_id, va); //test this
//
//
//                    } else if(type == AVDC_WRITE) {
                        //FROMERIK: invalidate only the cache line on write
                        //invalidate
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
//                    }
                }
=======
                if (md2->private_bit == 0 && type == AVDC_WRITE) { //then you have to go to md3
                    invalidate_cachline(cache, thread_id, va);
                }

            } else if (check_mem(li)) {

                //update internal statistics:
                core->stat.md2_mem_hit += 1;

                //CASE A: Read Miss, MD1/MD2 Hit
                //CASE B: Write Miss, Private Region, MD1/MD2 hit

                avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                //copy_md_to_md3(cache, thread_id, va); //test this
>>>>>>> temp

            } else if (check_rem(li)) {

                //assert(md2->private_bit==0);  ahh please put these back in
                assert_debug(cache, thread_id, md2->private_bit, 0, "***err l3 not active, search_md2\n", 1);
//                if(DEBUG ==1){
//                    if(md2->private_bit != 0) {
//                        core->stat.errors++;
//                        if(PRINT)
//                            printf("***err l3 not active, search_md2\n");
//                        //invalidate_cachline(cache, -1, va);
//                    }
//                } else {
//                    assert(md2->private_bit == 0);
//                }

                core->stat.md2_rem_hit += 1;

<<<<<<< HEAD
//                if(type == AVDC_READ) {
//                    copy_from_remote_node(cache, thread_id, li, va);
//                } else if(type == AVDC_WRITE) {
//                    //invalidate
                    invalidate_cachline(cache, thread_id, va); //TODO: test this
                    avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
//                }
=======
                if (type == AVDC_READ) {
                    copy_from_remote_node(cache, thread_id, li, va);
                } else if (type == AVDC_WRITE) {
                    //invalidate
                    invalidate_cachline(cache, thread_id, va); //TODO-done: test this

                    /*
                     * TODO: doesn't data need to be added here?
                     *
                     */

                    //this was super wrong and inserting data into the core twice
                    //avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                }
                //do ot return
>>>>>>> temp
            } else if (check_l3(li)) {

                //update internal statistics:
                core->stat.md2_l3_hit += 1;

                int l3_index = l3_index_from_pa(cache, va);
                int l3_way = li & (L3_BASE - 1);

                avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];
<<<<<<< HEAD
                assert(l3->active == 1);
                assert(l3->valid == 1);
                //assert_addr(&l3->address, va, 16); //TODO-fix errors.. errors everywhere prio-1
                assert_addr_dbg(&l3->address, va, 116, md2_index, md2_way, l3_index, l3_way, li);


                //insert data into requesting node
                if (md2->private_bit == 1) {
                    //if in memory and region is private add to region - addendum might not have to be private, no maybe should be, not sure
                    //TODO: check if this returns 2
                    avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                } else {
                    //TODO
                    //find master location

//                    if(type == AVDC_READ) {
//                        //TODO-done: don't access md3 go straight to the source
//                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way); //this kinda worked
//                        copy_md_to_md3(cache, thread_id, va); //test this
//
//                    } else if(type == AVDC_WRITE) {
                        //FROMERIK: invalidate only the cache line on write
                        invalidate_cachline(cache, thread_id, va); //TODO: test this
                        avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
//                    }
                }
                //return 1; this shouldn't be here
=======

                assert_debug(cache, thread_id, l3->active, 1, "***err L3 not active, md2_search, l3 hit\n", 1);
                assert_debug(cache, thread_id, l3->valid, 1, "***err L3 not valid, md2_search, l3 hit\n", 1);

                assert_addr(cache, thread_id, &l3->address, va, 16); //TODO-fix errors.. errors everywhere prio-1

                //insert data into requesting node
                avdc_insert_into_md_region(cache, thread_id, va, 2, md2_way);
                //copy_md_to_md3(cache, thread_id, va, 2002); //TODO: question does this need to be added

                //don't think i need this anymore either
                //clear_l3_entry_va(cache, va); //TODO: question does this need to be added

>>>>>>> temp
            } else {
                assert_debug(cache, thread_id, 1, 0, "***err: severe error in li encoding md2\n", 1);

            }

            //because we're in md2 is we have a hit here we copy the entry to md1 everytime
            int result = copy_md2_to_md1(cache, thread_id, va, md2_way);

            return result;

        }
    }
    //core->stat.md2_mem_miss += 1;
    return 0;
}

int search_md2_way(avdc_cache *cache, avdark_cache_core *core, avdc_pa_t va) {

    int md2_index = md2_index_from_pa(core, va);
    avdc_pa_t tag = tag_from_pa(core, va);

    int i;
    for (i = 0; i < MD2_ASSOC; i++) {
        if (core->md2[md2_index][i].tag == tag &&
            core->md2[md2_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_md2_way(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];

    int md2_index = md2_index_from_pa(core, va);
    int md1_index = md1_index_from_pa(core, va);

    int i;

    //find an entry that isn't valid yet use that one
    for (i = 0; i < MD2_ASSOC; i++) {
        if (core->md2[md2_index][i].valid == 0) {
            return i;
        }
    }

    if (CAP_PRINT && (cap_array[4] == 0)) {
        printf("***info: MD2 CAP\n");
        cap_array[4] = 1;
    }

    // find the inactive md2 region with the smallest time stamp
    int LRU_way = 0;
    double diff_time = 0;
    for (i = 0; i < MD2_ASSOC; i++) {
        diff_time = difftime(core->md2[md2_index][LRU_way].timestamp,
                             core->md2[md2_index][i].timestamp);
        if (diff_time > 0)
            LRU_way = i;
    }

    //at this point LRU is the right way to evict

    //TODO: make this it's own function
    avdc_pa_t LRU_pa = core->md2[md2_index][LRU_way].address[0]; //default use first address in region
    int region_num = region_from_pa(core, LRU_pa);
    //int LRU_li = core->md2[md2_index][LRU_way].li[region_num];

    int LRU_md2_index = md2_index_from_pa(core, LRU_pa);
    int LRU_md1_index = md1_index_from_pa(core, LRU_pa);
    int LRU_md1_way = -1;
    int LRU_md1_way_test = -1;

    avdc_md_t *LRU_md2 = &core->md2[LRU_md2_index][LRU_way];

    //search for active md1 entry, has to match on addr!
    LRU_md1_way = LRU_md2->tracker_pointer;
    LRU_md1_way_test = search_md1_way(core, LRU_pa);

    //copy valid MD to md2

    assert_debug(cache, thread_id, LRU_md1_way_test, LRU_md1_way, "***err lru_way don't match md1\n", 1);
//    assert(LRU_md1_way_test == LRU_md1_way);

    //if there in an MD1 copy it to MD2
    if (LRU_md1_way_test >= 0) {

        assert_addr(cache, thread_id, &LRU_md2->address[region_num], LRU_pa, 17);
        avdc_md_t *LRU_md1 = &core->md1[LRU_md1_index][LRU_md1_way];

        //copy md1 data to md2
        if (LRU_md1->active && LRU_md1->valid) {

            copy_md1_to_md2(cache, thread_id, va, md1_index, LRU_md1_way);

        }
    }

    //now MD2 is the active valid entry

    //remove data from cache

    assert_debug(cache, thread_id, LRU_md2->active, 1, "***err LRU_md2->active, find_md2_way\n", 1);

    //find the md3 entry
    int md3_index = md3_index_from_pa(cache, LRU_pa);
    avdc_pa_t LRU_tag = tag_from_pa(core, LRU_pa);

    int md3_way = search_md3_way(cache, LRU_pa);
    assert_debug(cache, -1, md3_way, -1, "***err md3 not valid, find_md2_way\n", 0);

    avdc_md3_t *LRU_md3 = &cache->md3[md3_index][md3_way];

    LRU_md3->pb = remove_pb(LRU_md3->pb, thread_id);

    clear_md2(cache, thread_id, md2_index, LRU_way, 0);

    return LRU_way;

}

/**
 *      TODO restructure this function
 *
 */

int copy_md2_to_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    int region_num = region_from_pa(core, va);
    int md2_index = md2_index_from_pa(core, va);
    int md1_index = md1_index_from_pa(core, va);

    //move md2 entry to md1
    int md1_way = -1;
    int md1_way_assert = core->md2[md2_index][md2_way].tracker_pointer;
    for (int j = 0; j < MD1_ASSOC; j++) {
        avdc_pa_t tag_md1 = core->md1[md1_index][j].tag;
        int active = core->md1[md1_index][j].active;

        if (tag_md1 == tag && //TODO-done: test changed to go off the tag not the addr
            active == 1) {
            md1_way = j;
        }
    }

    //TODO: pull common lines down to remove some duplicated code
    avdc_md_t *md2, *md1;

    if (md1_way != -1) {
        //if the md1 entry exists just update it

        //assert(md1_way == md1_way_assert);
        if (DEBUG == 1) {
            if (md1_way != md1_way_assert) {
                cache->stat3.errors++;
                if (PRINT) {
                    printf("***error in md1 way copy_md2_to_md1\n");
                }
            }
        } else {
            assert(md1_way == md1_way_assert);
        }

        md2 = &core->md2[md2_index][md2_way];


        //md1 = md2;
        //TODO:test case write a test case for this
        core->md1[md1_index][md1_way] = *md2; //content of md2 copy to md1
        md1 = &core->md1[md1_index][md1_way];
        md1->timestamp = clock();
        md2->active = 0;
        md1->tracker_pointer = md2_way; //this is new
        md2->tracker_pointer = md1_way; //this is new

    } else {
        //if it doesn't exist create an md1 entry
        md1_way = find_md1_way(cache, thread_id, va); //TODO: be sure it's new

        md2 = &core->md2[md2_index][md2_way];
        md1 = &core->md1[md1_index][md1_way];

        ///I think these asserts are a good idea to make sure the md1 is new
        assert_debug(cache, thread_id, md1->active, 0, "***err md1 not active\n", 1);
        assert_debug(cache, thread_id, md1->valid, 0, "***err md1 not valid\n", 1);
        assert_debug(cache, thread_id, md1->tag, 0, "***err md1 tag not zero\n", 1);


        ////**info: CREATE md1 entry from md2 entry
        for (int i = 0; i < REGION_SIZE; i++) {
            md1->li[i] = md2->li[i];
            md1->address[i] = md2->address[i];
        }

        assert_debug(cache, thread_id, md2->valid, 1, "***err md2 not valid\n", 1);
        assert_debug(cache, thread_id, md2->active, 1, "***err md2 not valid\n", 1);
        assert_debug(cache, thread_id, md2->tag, tag, "***err md2 not valid\n", 1);

        md1->valid = 1;
        md1->tag = tag;
        md1->active = 1;
        md1->private_bit = md2->private_bit; //i think this was the line //oh this was a very bad line

        md1->tracker_pointer = md2_way;
        md2->tracker_pointer = md1_way;

        assert_addr(cache, thread_id, &md2->address[region_num], va, 20);
        md1->timestamp = clock();
        md2->active = 0;
    }

//    assert(md1->active != md2->active);
    assert_debug(cache, thread_id, md1->active, md2->active, "***err md1 and md2 invalid\n", 0);

    return 1;
}


int add_to_l3(avdc_cache *cache, avdc_pa_t va) {

    int md3_way = search_md3_way(cache, va);
    avdc_pa_t tag = tag_from_pa(cache->cores[0], va);
    avdc_pa_t base_pa = pa_from_tag(cache->cores[0], tag);

    int region_num = region_from_pa(cache->cores[0], va);
//    if(tag == 137321127281 && region_num == 13) {
//          //printf("  aded to l3 what!!!: \n");
//    }

    int result;
    result = assert_debug(cache, -1, md3_way, -1, "***error md3_way invalid in add_to_l3\n", 0);
    if (result == 0) {
        return -1;
    }

    avdc_md3_t *md3 = search_md3_entry(cache, va, 4);

    //search to make sure doesn't exist already
    int check_l3_way = search_l3_way(cache, va);

    result = assert_debug(cache, -1, check_l3_way, -1, "***error check_l3_way in add_to_l3\n", 1);
    if (result == 0) {
        return -1;
    }

    //then get a fresh one
    int l3_way = find_l3_way(cache, va);
    int l3_index = l3_index_from_pa(cache, va);
    //int region_num = region_from_pa(cache->cores[0], va);

    avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];

    l3->valid = 1;

    l3->address = base_pa + (cache->cores[0]->block_size *
                             region_num); //hope this works it only needs to core for the block size which shoujld be the same everywhere
    //l3->address = va;
    l3->active = 1;
    l3->timestamp = clock();
    l3->tracker_pointer = md3_way; //TODO-done: also should fix this
    l3->replacement_pointer = -1; //TODO: fix this, pretty sure it should be null though

    md3->li[region_num] = L3_BASE + l3_way;


    //update active md entry

    if (md3->pb == 0) {
//        if (PRINT) {
//            printf("***info  add to l3 md3 untracked\n");
//        }

    } else if (number_of_pb(md3->pb) == 1) {
        //private region fine to evict
        //update the active MD
        int thread_id = get_thread(md3->pb);
        avdc_md_t *active_md = search_active_md(cache, thread_id, va); //new


        assert_debug(cache, thread_id, active_md->private_bit, 1, "***err active md private bit error in add_to_l3\n",
                     1);

        active_md->li[region_num] = L3_BASE + l3_way;


    } else if (number_of_pb(md3->pb) == 2) {
        //just as a test invalidate the other copies
        //invalidate_cachline(cache, -1, va);

        //go into all the cores and point to l3

        ///CASE F

        int pb = md3->pb;
        for (int i = 0; i < CORES; i++) {
            if (((int) pow((double) 2, i) & pb)) { //if the pb is a match for the core
                avdark_cache_core *rem_core = cache->cores[i];

                //md resources
                int md1_index = md1_index_from_pa(rem_core, va);
                int md2_index = md2_index_from_pa(rem_core, va);
                int md1_way = search_md1_way(rem_core, va);
                int md2_way = search_md2_way(cache, rem_core, va);

                assert_debug(cache, i, md2_way, -1, "*** err md2_way in add_to_l3\n", 0);
//                if (DEBUG == 1) {
//                    if (md2_way == -1) {
//                        cache->stat3.errors++;
//                        if (PRINT)
//                            printf("*** err md2_way in add_to_l3\n");
//                    }
//
//                } else {
//                    assert(md2_way != -1);
//                }

                avdc_md_t *md2 = &rem_core->md2[md2_index][md2_way];
                avdc_md_t *active_md;
                if (md1_way != -1) {
                    avdc_md_t *md1 = &rem_core->md1[md1_index][md1_way];
                    if (md1->active == 1) {
                        active_md = md1;
                    } else if (md2->active == 1) {
                        active_md = md2;
                    } else {
                        assert_debug(cache, i, 1, 0, "error in md active : add_to_l3\n", 1);

//                        if (DEBUG == 1) {
//                            rem_core->stat.errors++;
//                            if (PRINT == 1) {
//                                printf("error in md active : add_to_l3");
//                            }
//                        } else {
//                            printf("error in md active : add_to_l3");
//                            exit(1);
//                        }
                    }
                } else {
                    active_md = md2;
                }

                int li = -1;
                li = active_md->li[region_num];

                active_md->li[region_num] = L3_BASE + l3_way;

                //decode li to find out if data is in l1 or l2
                if (check_l1(li)) {

                    //okay the core could have a copy of the data if it was shared to lets wipe it
                    clear_l1_entry_va(rem_core, va);
                    clear_l2_entry_va(rem_core, va); //if it's in l1 then inclusively has to be in l2

                } else if (check_l2(li)) {

                    clear_l2_entry_va(rem_core, va);

                } else if ((check_l3(li))) {

                    //ERROR this is the data being put into L3 for the first time

                } else if ((check_mem(li))) {

                    //maybe add a statistic

                } else if (check_rem(li)) {
                    //not nessesarily a failure but cool to know
                    //exit(EXIT_FAILURE);

                } else {

                    assert_debug(cache, i, 1, 0, "***err: severe error in li encoding tolken 2948\n", 1);

                    //TODO: or maybe the data wasn't there in the first place

//                    if (DEBUG == 1) {
//                        cache->stat3.errors++;
//                        if (PRINT == 1) {
//                            printf("***err: severe error in li encoding tolken 2948\n");
//                        }
//                    } else {
//                        printf("***err: severe error in li encoding tolken 2948\n");
//                        exit(EXIT_FAILURE);
//                    }
                }

            }

        }


    } else {
        assert_debug(cache, -1, 1, 0, "***err: severe error in li encoding md3\n", 1);
//        if (DEBUG == 1) {
//            cache->stat3.errors++;
//            if (PRINT == 1) {
//                printf("***err: severe error in li encoding md3\n");
//            }
//        } else {
//            printf("***err: severe error in li encoding md3\n");
//            exit(EXIT_FAILURE);
//        }
    }


    return l3_way;

}

/***
 *
 * @param cache
 * @param thread_id
 * @param md2_index
 * @param md2_way
 * @param mode mode->0 for clear and add to md3 mode ->1 just delte don't add to l3
 */

void clear_md2(avdc_cache *cache, int thread_id, int md2_index, int md2_way, int mode) {

    if (md2_way == -1)
        return;

    avdark_cache_core *core = cache->cores[thread_id];
    avdc_md_t *md2 = &core->md2[md2_index][md2_way];

    if (md2->active == 0 && md2->valid == 1) {
        //then there is an md1 entry that has to be delted right?
        assert_debug(cache, thread_id, 1, 0, "***err clear_md2 called without valid parameter\n", 0);
    }
    if (md2->active == 1 && md2->valid == 1) {

        /***
         *    TODO: check to see if the MD2 is in a shared state or not
         */


        for (int i = 0; i < REGION_SIZE; i++) {

            avdc_pa_t pa = md2->address[i]; //default use first address in region
            int l1_index = l1_index_from_pa(core, pa);
            int l2_index = l2_index_from_pa(core, pa);

            int li = md2->li[i];
            avdc_pa_t va = md2->address[i];

            if (check_l1(li)) {
                int li_way = li & (L1_BASE - 1);

                assert_addr(cache, thread_id, &core->l1[l1_index][li_way].address, pa, 18);

                //add to l3 before removing
                if(mode == 0) {
                    add_to_l3(cache, pa);
                }

                clear_l1_entry_va(core, pa);
                //also need to clear l2 (spend 3 hours on this bug)
                clear_l2_entry_va(core, pa);
            } else if (check_l2(li)) {

                int li_way = li & (L2_BASE - 1);

                assert_addr(cache, thread_id, &core->l2[l2_index][li_way].address, pa, 19);
                //add to l3 before removing
                if(mode == 0){
                    add_to_l3(cache, pa); //if we are removing the MD# entry we shouldn't ad to l3
                }

                clear_l2_entry_va(core, pa);
            } else if (check_l3(li)) {
                //don't need to do anything i don't think
                //maybe notify md3
            } else if (check_mem(li)) {
                //don't do anything
            } else if (check_rem(li)) {
                //maybe send a notification to the remote node or to md3
                //to be removed from the pb
                if(mode == 0) {
                    invalidate_cachline(cache, -1, va);
                }
            } else {
                assert_debug(cache, thread_id, 1, 0, "error in li encosing 3212\n", 1);
            }

        }
    }

    if (md2->tracker_pointer != -1) {
        //TODO: erase md1
        avdc_pa_t va = md2->address[0];
        clear_md1_entry_va(cache, thread_id, va, 0); //TODO test
    }

    md2->valid = 0;
    md2->active = 0;
    md2->tracker_pointer = -1;
    md2->tag = 0;
    md2->timestamp = 0;
    md2->private_bit = 0;

    for (int g = 0; g < REGION_SIZE; g++) {
        md2->address[g] = 0;
        md2->li[g] = 0;
    }

}


int search_md3(avdc_cache *cache, avdc_pa_t pa, int thread_id, type_t type) {

    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa);
    int region_num = region_from_pa(cache->cores[0], pa);
    int md3_index = md3_index_from_pa(cache, pa);
    //int l3_index = l3_index_from_pa(cache, pa);

    int md3_way = search_md3_way(cache, pa);
    if (md3_way != -1) {

        avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];

        if ((md3->tag == tag) &&
            (md3->valid == 1) &&
            (md3->active == 1)) {

            cache->stat3.md3_hit += 1;
            assert_addr(cache, -1, &md3->address[region_num], pa, 5);

            if (md3->pb == 0) {

                //Has an entry in MD3 but not in any nodes
                //D1: Read Miss, MD1/MD2 Miss (#PB=0, Untracked -> Private)
                //check if it's in memory or if it's in l3

                int li = md3->li[region_num];
                if (check_mem(li)) {

                    //update md3_mem_hit
                    cache->stat3.md3_mem_hit += 1;

                    //install untracked data into md1 and md2
                    //int test_md1_index = avdc_insert(cache, thread_id, pa);
                    //add_to_core(cache, thread_id, pa);

                    add_to_core_from_md3(cache, thread_id, pa); //new

                    //set pb bits
                    md3->timestamp = clock();
                    md3->pb = set_pb(thread_id); //TODO-done: TEST this

//                    //TODO: remove duplicate code
//                    avdark_cache_core *core = cache->cores[thread_id];
//                    int md1_way = search_md1_way(core, pa);
//                    int md2_way = search_md2_way(cache, core, pa);
//                    int md2_index = md2_index_from_pa(core, pa);
//                    int md1_index = md1_index_from_pa(core, pa);
//                    avdc_md_t *md1 = &core->md1[md1_index][md1_way];
//                    avdc_md_t *md2 = &core->md2[md2_index][md2_way];
//                    md1->private_bit = 1;
//                    md2->private_bit = 1;

                    avdc_md_t *active_md = search_active_md(cache, thread_id, pa);
                    active_md->private_bit = 1;

                    //because we can
                    //assert(test_md1_index == md1_index);
                    return 1;

                } else if (check_l3(li)) {

                    //update md3_l3_hit
                    cache->stat3.md3_l3_hit += 1;

                    //TODO: remove duplicate code
                    int l3_index = l3_index_from_pa(cache, pa);
                    int l3_way = li & (L3_BASE - 1);

                    avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];
                    //    assert(l3->active == 1);
                    assert_debug(cache, -1, l3->active, 1, "***err L3_active not active, search_md3\n", 1);
                    assert_debug(cache, -1, l3->valid, 1, "***err l3 valid not active, search_md3\n", 1);

                    //validate entry
                    assert_addr(cache, -1, &l3->address, pa, 40); //TODO: fix this it errors sometimes

                    //insert into core from l3
                    //assert that md doesn't exist already
                    avdark_cache_core *core = cache->cores[thread_id];

                    //these searches are kinda work around to avoid adding data twice
                    //this could happen if a cachline was invalidated unnessesarily
                    int l1_way_search = search_l1_way(core, pa);
                    int l2_way_search = search_l2_way(core, pa);

                    assert_debug(cache, -1, l1_way_search, -1,
                                 "***err insert from MD3 failed, data already present in core l1\n", 1);
                    assert_debug(cache, -1, l2_way_search, -1,
                                 "***err insert from MD3 failed, data already present in core l2\n", 1);

                    int result = add_to_core_from_md3(cache, thread_id, pa);

                    /***
                     * TODO: i think we might want to set the private bit in the MD1 here just maybe, we do it in the case above -mark
                     *
                     */

                    assert_debug(cache, -1, result, -1, "***err insert from MD3 failed, search_md3 in check(l3)\\n", 0);

                    //TODO-done:remove l3 entry
                    //clear_l3_entry_va(cache, pa); this is taken care of in l1 now

                    //update md3
                    md3->li[region_num] = thread_id; //maybe this change will fix the breaks

                    //TODO-done: set thread bit, rem bit
                    ///should be 3
                    //md3->pb = set_pb(thread_id); //TODO: test this //this is also taken care of in add_to_core_from_md3 now

                    return 1;

                } else if (check_rem(li)) {

                    //NEW thing added after debugging

                    //THIS SHOULD JUST NEVER HAPPEB
                    assert_debug(cache, -1, 1, 0,
                                 "***errr remote data in untracked md3 thread_id: %d remte core li: %d\n", 1);
//                    printf("***errr remote data in untracked md3 thread_id: %d remte core li: %d\n", thread_id, li);
//                    exit(1);

                } else {
                    assert_debug(cache, -1, 1, 0, "*** err data not in memory or l3, search_md3 - really bad one\n", 1);

                }

            } else if (number_of_pb(md3->pb) == 1) {

                int pb = md3->pb;
                int rem_thread_id = get_thread(pb);

                if (type == AVDC_READ) {
                    //CASE D.2: from paper READ miss, MD miss
                    //D2: Read Miss, MD1/MD2 Miss (#PB=1, Private -> Shared)
                    //memory is in a remote node
                    cache->stat3.md3_rem_hit += 1;

                    copy_md_to_md3(cache, rem_thread_id, pa, 5); //make sure this still works, tested still works
                    //install untracked data into md1 and md2

                    //assert that md doesn't exist already
                    avdark_cache_core *core = cache->cores[thread_id];

                    //these searches are kinda work around to avoid adding data twice
                    //this could happen if a cachline was invalidated unnessesarily
                    int l1_way_search = search_l1_way(core, pa);
                    int l2_way_search = search_l2_way(core, pa);
                    int md1_way_search = search_md1_way(core, pa);
                    int md2_way_search = search_md2_way(cache, core, pa);

                    assert_debug(cache, thread_id, l1_way_search, -1, "***err insert from MD3 failed, l1_way_search\n",
                                 1);
                    assert_debug(cache, thread_id, l2_way_search, -1, "***err insert from MD3 failed, l2_way_search\n",
                                 1);
                    assert_debug(cache, thread_id, md1_way_search, -1,
                                 "***err insert from MD3 failed, md1_way_search\n", 1);
                    assert_debug(cache, thread_id, md2_way_search, -1,
                                 "***err insert from MD3 failed, md2_way_search\n", 1);

                    //TODO: it would be nice to restructure to avoid this
                    //TODO:doing okay maybe this doesn't happen as much now

                    //add the core to the MD3 presence bits
                    ///had to move this up so that add_to_core_from_md3 would work right

                    //adding this current thread id, this causes problems when the eviction goes to l3 and it thinks the region is shared but... nbd
                    //md3->pb = set_pb(thread_id) | set_pb(rem_thread_id); //TODO-done: this needs to be both the calling core and callee core

                    //insert shared cacheline on the local core
                    int result = add_to_core_from_md3(cache, thread_id, pa);
                    assert_debug(cache, -1, result, -1, "***err insert from MD3 failed, search_md3 2\n", 0);

                    //TODO-done: copy md from rem thread to md3
                    //TODO-done: clear remote private bits
                    //TODO-done: change md3 LI to point to rem_node
                    //TODO-done: send md to calling thread with LI for the access set to that node

                    return 1;
                } else if (type == AVDC_WRITE) {


                    //invalidate all slave cachlines and set LI to req node_id
                    invalidate_cachline(cache, thread_id, pa);

                    copy_md_to_md3(cache, rem_thread_id, pa, 1515);

                    //add the data
                    int result = add_to_core_from_md3(cache, thread_id, pa);

                    assert_debug(cache, -1, result, -1, "***err insert from MD3 failed, search_md3 3\n", 0);

                    //md3->pb = set_pb(thread_id) |  set_pb(rem_thread_id);

                    cache->stat3.md3_rem_hit += 1;

                    return 1;
                }

            } else if (number_of_pb(md3->pb) == 2) {

<<<<<<< HEAD
//                if(type == AVDC_READ) { //TODO: test this section
//                    //CASE D.3 read miss, md1/md2 miss
//                    //#PB > 1: shared -> shared
//
//                    //memory is in a remote node
//                    //TODO: not if it's an md1/md2 hit
//                    cache->stat3.md3_rem_hit += 1;
//
//                    //install untracked data into md1 and md2
//                    avdc_insert_from_md3(cache, thread_id, pa); //TODO TEST
//                    md3->pb = set_pb(thread_id) ^ md3->pb;
//
//                    return 1;
//
//                } else if(type == AVDC_WRITE) {
=======
                if (type == AVDC_READ) {
                    //CASE D.3 read miss, md1/md2 miss
                    //#PB > 1: shared -> shared

                    //memory is in a remote node
                    //TODO: not if it's an md1/md2 hit
                    cache->stat3.md3_rem_hit += 1;

                    //here the data needs to be copied back from a core to md3

                    int rem_thread_id = find_remote_core(md3->pb);
                    copy_md_to_md3(cache, rem_thread_id, pa, 3001); //TODO test
                    //so this is maybe in accurate becasue if data is replicated across multiple cores md3 just picks the first core as the master location
                    //but maybe it doesn't matter

                    //install untracked data into md1 and md2
                    int result = add_to_core_from_md3(cache, thread_id, pa);
                    assert_debug(cache, -1, result, -1, "***err insert from MD3 failed, search_md3 3 \n", 0);

                    //md3->pb = set_pb(thread_id) | md3->pb;

                    return 1;

                } else if (type == AVDC_WRITE) {
>>>>>>> temp
                    ///Case C from the paper write
                    ///write miss, shared region, md1/md2 hit

                    //invalidate all slave cachlines and set LI to req node_id
                    invalidate_cachline(cache, thread_id, pa);

                    //add the data
                    int result = add_to_core_from_md3(cache, thread_id, pa);

                    assert_debug(cache, -1, result, -1, "***err insert from MD3 failed, search_md3 4\n", 0);

                    //md3->pb = set_pb(thread_id) |  md3->pb;
                    cache->stat3.md3_rem_hit += 1;

                    return 1;
<<<<<<< HEAD
//                } else {printf("error in AVDC type"); exit(1);}

            }else {printf("***err: severe error in li encoding md3\n"); exit(EXIT_FAILURE);}
=======
                } else {
                    assert_debug(cache, -1, 1, 0, "***err in AVDC type in search md3\n", 1);
                }
            } else {
                assert_debug(cache, -1, 1, 0, "***err: severe error in li encoding md3\n", 1);
            }
>>>>>>> temp
        }
    }
    cache->cores[thread_id]->stat.md2_mem_miss += 1;
    return 0;
}

/*
 * okay this looks through the bit pattern and determines if only one core is set
 *
 * zero means multiple cores
 *
 * one means single core
 *
 */
int number_of_pb(int pb) {
    if (pb == 0)
        return -1;
    if (pb == 2)
        return 2;
    int flag = 0;
    for (int i = 0; i < CORES; i++) {

        int result = pow((double) 2, i);
        if (result & pb) {
            if (flag == 0) {
                flag = 1;
            } else {
                return 2;
            }
        }
    }
    if (flag == 1) {
        return 1;
    } else {
        return -1;
    }
}

int find_remote_core(int pb) {
    if (pb == 0)
        return -1;
    for (int i = 0; i < CORES; i++) {

        int result = pow((double) 2, i);
        if (result & pb) {
            return i;
        }
    }

    return -1;
}

int search_md3_way(avdc_cache *cache, avdc_pa_t pa) {

    int md3_index = md3_index_from_pa(cache, pa);
    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa);

    int i;
    for (i = 0; i < cache->md3_assoc; i++) {
        if (cache->md3[md3_index][i].tag == tag &&
            cache->md3[md3_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_md3_way(avdc_cache *cache, avdc_pa_t pa) {

    int md3_index = md3_index_from_pa(cache, pa);
    avdc_pa_t tag = tag_from_pa(cache->cores[0], pa); //doesn't matter that it's core 0

    int i;

    //find an entry that isn't valid yet use that one
    for (int i = 0; i < cache->md3_assoc; i++) {
        if (cache->md3[md3_index][i].valid == 0) {
            //printf("-------------------found free md3 (valid) way: %d\n",i );
            return i;
        }
    }

    if (CAP_PRINT && (cap_array[5] == 0)) {
        printf("***info: MD3 CAP\n");
        cap_array[5] = 1;
    }

    // find the md3 region with the smallest time stamp
    int LRU_way = 0; //found a bug this has to be zer to start with
    double diff_time = 0;
<<<<<<< HEAD
    for(i=0; i < MD3_ASSOC; i++){
=======
    for (i = 1; i < cache->md3_assoc; i++) {
>>>>>>> temp
        diff_time = difftime(cache->md3[md3_index][LRU_way].timestamp,
                             cache->md3[md3_index][i].timestamp);
        if (diff_time > 0)
            LRU_way = i;
    }

    //avdc_md3_t * md3 = &cache->md3[md3_index][LRU_way];
    //int pb = md3->pb;
    //avdc_pa_t LRU_pa = md3->address[0]; //just take the first one

    clear_md3(cache, md3_index, LRU_way);

    /***
     *    TODO: I think this is a good place to check for bugs
     *
     */

//    if (pb == 0) {
//        //there is no data in any nodes it's all in l3 or in memory
//        //evict all data out of l3
//        for(int i = 0; i<cache->md3_assoc; i++){
//            int li = md3->li[i];
//            if(check_l3(li)) {
//                avdc_pa_t evict_va = md3->address[i];
//                int l3_index = l3_index_from_pa(cache, evict_va);
//                int l3_way = search_l3_way(cache, evict_va);
//                //assert(l3_way != -1);
//                assert_debug(cache, -1, l3_way, -1, "***err L3_way  -1, find_md3_way\n", 0);
//
//                clear_l3_entry_va(cache, evict_va);
//            } else if (check_mem(li)) {
//                //no-op
//                //it's in memory what you gonna do
//
//            } else {
//                assert_debug(cache, -1, 1, 0, "***err MD3 LI encoding, data not tracked in pb, but not found in L3 or mem\n", 1);
//            }
//        }
//    } else if (number_of_pb(pb) == 1) {
//        //there is entries in one core remove everything
//
//        int thread_id = get_thread(pb);
//        remove_from_core(cache, thread_id, LRU_pa);
//
//    } else if (number_of_pb(pb) == 2) {
//
//        for(int i = 0; i<CORES; i++) {
//            if (((int) pow((double) 2, i) & pb)) { //if the pb is a match for the core
//
//                int thread_id = i;
//                remove_from_core(cache, thread_id, LRU_pa);
//            }
//        }
//    }
//    //md3->valid = 0;
//    clear_md3(cache, md3_index, LRU_way);

    return LRU_way;

}


/*
 * In this function we install a md entry into the md3
 * And we will call add_to_core
 * to add entries in md1 and md2 and l1 and l2
 *
 * covers case D.1 untracked -> private
 *
 */

int avdc_insert_md3(avdc_cache *cache, avdc_pa_t va, int thread_id, type_t type) {

    avdark_cache_core *core = cache->cores[thread_id];
    avdc_pa_t tag = tag_from_pa(core, va);
    avdc_pa_t base_pa = pa_from_tag(core, tag);
    //int region_num = region_from_pa(core, va);

    int md3_index = md3_index_from_pa(cache, va);
    int md3_way = find_md3_way(cache, va);

    avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];
    assert_debug(cache, -1, md3->active, 0, "***error MD3 active error: avdc_insert_md3\n", 1);

    //create new md3
    for (int i = 0; i < REGION_SIZE; i++) {
        md3->li[i] = MEM_BASE;
        md3->address[i] = base_pa + (core->block_size * i);
    }
    md3->valid = 1;
    md3->tag = tag;
    md3->active = 1;

    add_to_core(cache, thread_id, va);

    assert_debug(cache, -1, md3->pb, 0, "***err md3 PB not set correctly\n", 1); //might blow up

    int pb = set_pb(thread_id);
    md3->pb = md3->pb | pb; //add the thread id to the bit pattern

    cache->stat3.md3_mem_miss += 1;
    md3->timestamp = clock();

    return 0;
}

/*
 * this function takes a thread id
 * and return bit position for that thread set to 1
 */

int set_pb(int thread_id) {
    if (thread_id >= 0 && thread_id <= CORES) {
        return pow(2, thread_id);
    } else {
        if (DEBUG == 1) {
            if (PRINT == 1) {
                printf("err*** set pb boom");
            }
        } else {
            printf("err*** set pb boom");
            exit(EXIT_FAILURE);
        }
        return 0;
    }
}

/*
 * get a pb bit pattern and return the one thread that is bas
 */
int get_thread(int pb) {
    if (pb > 0)
        return log2((double) pb);
    else
        return -1;
}

int remove_pb(int pb, int thread_id) {
    if (DEBUG == 1) {
        if ((pb < 0) && PRINT) {
            printf("*** err pb bit 0, in remove_pb %d\n", pb);
            return set_pb(thread_id);
        }
    } else {
        assert(pb > 0);
    }
    return pb ^ set_pb(thread_id);
}


void clear_md3(avdc_cache *cache, int md3_index, int md3_way) {

    if (md3_way == -1)
        return;

    avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];

    assert_debug(cache, -1, md3->active, 1, "***err clear md3 active called but no md3 found\n", 1);
    assert_debug(cache, -1, md3->valid, 1, "***err clear md3 valid called but no md3 found\n", 1);

    //first remove data from cores
    if (md3->active == 1 && md3->valid == 1) {
        int pb = md3->pb;

        if (pb == 0) {
            //there is no data in any nodes it's all in l3 or in memory
            //evict all data out of l3
            for (int i = 0; i < cache->md3_assoc; i++) {
                int li = md3->li[i];
                if (check_l3(li)) {
                    avdc_pa_t evict_va = md3->address[i];
                    int l3_index = l3_index_from_pa(cache, evict_va);
                    int l3_way = search_l3_way(cache, evict_va);
                    //assert(l3_way != -1);
                    assert_debug(cache, -1, l3_way, -1, "***err L3_way  -1, find_md3_way\n", 0);

                    clear_l3_entry_va(cache, evict_va);
                } else if (check_mem(li)) {
                    //no-op
                    //it's in memory what you gonna do

                } else {
                    assert_debug(cache, -1, 1, 0,
                                 "***err MD3 LI encoding, data not tracked in pb, but not found in L3 or mem\n", 1);
                }
            }
        } else if (number_of_pb(pb) == 1) {
            //there is entries in one core remove everything

            int thread_id = get_thread(pb);
            avdc_pa_t pa = md3->address[0];
            copy_md_to_md3(cache, thread_id, pa, 188);
            remove_from_core(cache, thread_id, pa);

        } else if (number_of_pb(pb) == 2) {

            int flag = 0;
            for (int i = 0; i < CORES; i++) {
                if (((int) pow((double) 2, i) & pb)) { //if the pb is a match for the core

                    avdc_pa_t pa = md3->address[0];
                    int thread_id = i;
                    if (flag == 0) {
                        copy_md_to_md3(cache, thread_id, pa, 18118);
                        flag = 1;
                    }
                    remove_from_core(cache, thread_id, pa);
                }
            }
        }

        //now valid md data has been copied to MD3 so remove from L3


        for (int i = 0; i < REGION_SIZE; i++) {
            int li = md3->li[i];
            avdc_pa_t va = md3->address[i];


            /***
             *
             * TODO: work on this function build it up so it gives more info
             *
             */

            if (check_l1(li)) {
                assert_debug(cache, -1, 0, 1, "*** error in li encoding l1 clear_md3\n", 1); //check

            } else if (check_l2(li)) {
                assert_debug(cache, -1, 0, 1, "*** error in li encoding l2 clear_md3\n", 1); //check

            } else if (check_l3(li)) {
                clear_l3_entry_va(cache, va);

            } else if (check_mem(li)) {
                //don't do anything
            } else if (check_rem(li)) {
                //already removed from core

                //maybe send a notification to the remote node or to md3
                //to be removed from the pb


            } else {
                assert_debug(cache, -1, 0, 1, "*** error in li encoding clear_md3\n", 1); //check
            }

        }

        md3->valid = 0;
        md3->active = 0;
        md3->tag = 0;
        md3->pb = 0;
        md3->timestamp = 0;


        for (int g = 0; g < REGION_SIZE; g++) {

            md3->address[g] = 0;
            md3->li[g] = 0;
        }

    }

}


/*
 * used to copy active md entry to md3 entry
 * */

void copy_md_to_md3(avdc_cache *cache, int rem_thread_id, avdc_pa_t pa, int fx_code) {

    avdark_cache_core *rem_core = cache->cores[rem_thread_id];

    int md3_index = md3_index_from_pa(cache, pa);
    int md2_index = md2_index_from_pa(rem_core, pa);
    int md1_index = md1_index_from_pa(rem_core, pa);

    avdc_pa_t tag = tag_from_pa(cache->cores[rem_thread_id], pa);
    int md3_way = search_md3_way(cache, pa);

    int md2_way = search_md2_way(cache, rem_core, pa);
    int md1_way = search_md1_way(rem_core, pa);
    //assert(md1_way != -1); //sometimes there is no md1
    //assert(md2_way != -1); //i think there has to be an md2_directory here

    assert_debug(cache, -1, md2_way, -1, "***err md2_way failure, copy_md_to_md3\n", 0);
    assert_debug(cache, -1, md3_way, -1, "***err md3_way failure, copy_md_to_md3\n", 0);

    avdc_md_t *rem_active_md = search_active_md(cache, rem_thread_id, pa);

    avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];

    rem_active_md->private_bit = 0; //TODO-doing test this, tested once

    md3->active = rem_active_md->active;
    md3->tag = rem_active_md->tag; //TODO: test: is this right?
    md3->valid = rem_active_md->valid;
    md3->timestamp = clock();

    //assert(md3->active == 1 && md3->valid == 1);

    assert_debug(cache, -1, md3->active, 1, "***err md3 active not active, copy_md_to_md3\n", 1);
    assert_debug(cache, -1, md3->valid, 1, "***err md3 valid not active, copy_md_to_md3\n", 1);

    /*from paper
     * It clears its private bit and sends8 metadata to MD3;
     * LIs pointing to local location in the slave node (e.g. in L1D) are converted to point to the slave NodeID
     * and stored in MD3 metadata;
     */

    for (int j = 0; j < REGION_SIZE; j++) {
        md3->address[j] = rem_active_md->address[j];

        //TODO-doing: add l3 when it comes
        int li = rem_active_md->li[j];

        if (check_l1(li) || check_l2(li)) {
            md3->li[j] = rem_thread_id;
        } else {
            md3->li[j] = rem_active_md->li[j];
        }
    }

}


int search_l1_way(avdark_cache_core *self, avdc_pa_t pa) {

    int l1_index = l1_index_from_pa(self, pa);
    int i;

    for (i = 0; i < L1_ASSOC; i++) {
        //TODO: this has to be a range, please test
        //TODO: is be to large a range, change
        if ((self->l1[l1_index][i].address <= pa) &&
            (pa < self->l1[l1_index][i].address + BLOCK_SIZE) &&
            self->l1[l1_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_l1_way(avdc_cache *cache, int thread_id, avdc_pa_t pa) {

    avdark_cache_core *self = cache->cores[thread_id];

    int l1_index = l1_index_from_pa(self, pa);
    int i;

    for (i = 0; i < L1_ASSOC; i++) {
        if (self->l1[l1_index][i].valid == 0) {
            return i;
        }
    }

    if (CAP_PRINT && (cap_array[0] == 0)) {
        printf("***info: L1 CAP\n");
        cap_array[0] = 1;
    }

    //no more space left find something to evict
    int LRU_way = 0;
    double diff_time = 0;
<<<<<<< HEAD
    for(i=0; i < L1_ASSOC; i++){
=======
    for (i = 1; i < L1_ASSOC; i++) {
>>>>>>> temp
        diff_time = difftime(self->l1[l1_index][LRU_way].timestamp,
                             self->l1[l1_index][i].timestamp);
        if (diff_time > 0)
            LRU_way = i;
    }

    avdc_pa_t LRU_pa = self->l1[l1_index][LRU_way].address;
    avdc_pa_t LRU_tag = tag_from_pa(self, LRU_pa);
    int region_num = region_from_pa(self, LRU_pa);

    if (LRU_tag == 549287177920) {
        printf("moving cache line from l1 to l2\n");
    }
//
//    int LRU_l2_index = l2_index_from_pa(self, LRU_pa);
//    int LRU_l1_index = l1_index_from_pa(self, LRU_pa);

    avdc_cache_line_t *LRU_l2, *LRU_l1;
    LRU_l2 = search_l2_entry(cache, thread_id, LRU_pa);
    LRU_l1 = search_l1_entry(cache, thread_id, LRU_pa);

//    LRU_l1 = &self->l1[LRU_l1_index][LRU_way];
//
    int LRU_l2_way = -1;
//    //LRU_l2_way = LRU_l1->replacement_pointer;
    LRU_l2_way = search_l2_way(self, LRU_pa);
//
//    LRU_l2 = &self->l2[LRU_l2_index][LRU_l2_way];
//
//    //not equal right?
//    assert_debug(cache, thread_id, LRU_l2_way, -1, "***err: something wrong l2 address tag - find_l1_way\n", 0);
//
//    //TODO-maybe assert that md1 is active
//
//    //move the data and update active states
//    //assert(LRU_l2->address == LRU_l1->address);
//    assert_addr(cache, thread_id, &LRU_l2->address, LRU_l1->address, 1);

    //this should be a call to add to l2

    *LRU_l2 = *LRU_l1;
    LRU_l2->timestamp = clock(); //reset timestamp
    LRU_l2->replacement_pointer = -1; //replacement pointer invalid

    clear_l1_entry_va(self, LRU_l1->address);


    assert_debug(cache, thread_id, LRU_l2->active, 1, "***err LRU_l2->active, find_l1_way\n", 1);
    assert_debug(cache, thread_id, LRU_l2->valid, 1, "***err LRU_l2->valid, find_l1_way\n", 1);

//    if(DEBUG ==1){
//        if((LRU_l2->active != 1) ) {
//            self->stat.errors++;
//            if(PRINT)
//            printf("***err LRU_l2->active, find_l1_way\n"); }
//    } else {
//        assert(LRU_l2->active == 1);
//    }
//    if(DEBUG ==1){
//        if((LRU_l2->valid != 1) ) {
//            self->stat.errors++;
//            if(PRINT)
//            printf("***err LRU_l2->valid, find_l1_way \n"); }
//    } else {
//        assert(LRU_l2->valid == 1);
//    }

//    //UPDATE MD1 and MD2
//    int LRU_md1_index = md1_index_from_pa(self, LRU_pa);
//    int LRU_md2_index = md2_index_from_pa(self, LRU_pa);
//
//    int LRU_md2_way = -1;
//
//    int LRU_md2_way_assert = LRU_l2->tracker_pointer;
//    LRU_md2_way = search_md2_way(cache, self, LRU_pa);
//
//    assert_debug(cache, -1, LRU_md2_way, -1, "***err md2 way not found, find_l1_way\n", 0);
//    assert_debug(cache, -1, LRU_md2_way_assert, LRU_md2_way, "***err LRU_md2_way not no match with assert, find_l1_way\n", 1);
//
//    //int region_num = region_from_pa(self, LRU_pa);
//
//    int LRU_md1_way_assert = self->md2[LRU_md2_index][LRU_md2_way].tracker_pointer;
//    int LRU_md1_way = -1;
//
//    LRU_md1_way = search_md1_way(self, LRU_pa);
//
//    //assert(LRU_md1_way != -1); //blows up sim_ls test
//    //assert(LRU_md1_way_assert == LRU_md1_way); //this should always be the same
//
//    assert_debug(cache, thread_id, LRU_md1_way_assert, LRU_md1_way, "***err : LRU_md1_way_assert, find_l1_way\n", 1);

//    if(DEBUG == 1){
//        if((LRU_md1_way_assert != LRU_md1_way)) {
//            cache->stat3.errors++;
//            if(PRINT) {
//              /*  printf("***err : LRU_md1_way_assert, find_l1_way\n");
//                printf("---> va %llu\n", LRU_pa);
//                printf("---> LRU_md1_way_assert: %d, LRU_md1_way: %d\n", LRU_md1_way_assert, LRU_md1_way);
//                */
//            }
//        }
//    } else {
//        assert(LRU_md1_way_assert == LRU_md1_way); //this should always be the same
//
//    }
//    avdc_md_t * md2 = &self->md2[LRU_md2_index][LRU_md2_way];

    avdc_md_t *active_md = search_active_md(cache, thread_id, LRU_pa);

//    if(LRU_md1_way != -1 ) {
//        avdc_md_t * md1 = &self->md1[LRU_md1_index][LRU_md1_way];
//        if(md1->active == 1) {
//            active_md = md1;
//        } else if(md2->active == 1) {
//            active_md = md2;
//        } else {
//            if(DEBUG == 1 ) {
//                cache->stat3.errors++;
//                if(PRINT == 1) {
//                    printf("sever error in md active state 7253\n");
//                }
//            } else {
//                printf("sever error in md active state 7253\n");
//                exit(1);
//            }
//        }
//    } else {
//        active_md = md2;
//    }



    assert_addr(cache, thread_id, &active_md->address[region_num], LRU_pa, 2);
    //**info: store the evicted data location into either md1 or md2
    active_md->li[region_num] = L2_BASE + LRU_l2_way;

    return LRU_way;
}

void clear_md1_entry_va(avdc_cache *cache, int thread_id, avdc_pa_t va, int mode) {
    avdark_cache_core *core = cache->cores[thread_id];

    int md1_index = md1_index_from_pa(core, va);
    int md1_way = search_md1_way(core, va);

    clear_md1(cache, thread_id, md1_index, md1_way, mode);
}

/**
 *
 * @param cache
 * @param thread_id
 * @param va
 * @param mode mode to forward to clear md2 1 for delete and not add to l3 0 for add to l3
 */
void clear_md2_entry_va(avdc_cache *cache, int thread_id, avdc_pa_t va, int mode) {
    avdark_cache_core *core = cache->cores[thread_id];

    int md2_index = md2_index_from_pa(core, va);
    int md2_way = search_md2_way(cache, core, va);

    clear_md2(cache, thread_id, md2_index, md2_way, mode);
}


void clear_l1_entry_va(avdark_cache_core *core, avdc_pa_t va) {

    int l1_index = l1_index_from_pa(core, va);
    int l1_way = search_l1_way(core, va);
    //assert(l1_way != -1); //TODO: add this back in
    //remove this work around
    if (l1_way == -1)
        return;

    avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];

    l1->valid = 0;
    l1->active = 0;
    l1->address = 0;
    l1->tracker_pointer = 0;
    l1->replacement_pointer = 0;
    l1->timestamp = 0;

    core->number_of_l1_entries--;

}

int add_to_core(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    //crete MD1 and MD2 entries
    int md2_way = create_md2(cache, thread_id, va);
    int md1_way = create_md1(cache, thread_id, va, md2_way);

    //add l1 and l2 cache

    int l2_way = add_to_l2(cache, thread_id, va, md2_way);
    int l1_way = add_to_l1(cache, thread_id, va, md2_way, l2_way);

    return l1_way;
}

void remove_from_core(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];

    //these delete data too
    clear_md1_entry_va(cache, thread_id, va, 1);
    clear_md2_entry_va(cache, thread_id, va, 1);

}

/**
 * @param cache
 * @param thread_id
 * @param va
 * @param cache_level
 * @return this returns in l1_way to be stored in the md li
 */
int add_to_l1(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way, int l2_way) {

    avdark_cache_core *core = cache->cores[thread_id];

    int l1_index = l1_index_from_pa(core, va);
    int l1_way = find_l1_way(cache, thread_id, va);
    int region_num = region_from_pa(core, va);
    avdc_pa_t tag = tag_from_pa(core, va);
    avdc_pa_t base_pa = pa_from_tag(core, tag);

    avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];

    /// CREATE l1 entry
    //assert(l1->valid == 0 && l1->active == 0);
    assert_debug(cache, thread_id, l1->active, 0, "***err L1 not active, add_to_l1\n", 1);
    assert_debug(cache, thread_id, l1->valid, 0, "***err L1 not valid, add_to_l1\n", 1);

    l1->valid = 1;
    l1->address = base_pa + (core->block_size * region_num);
    //l1->address = va;
    l1->active = 1;
    l1->timestamp = clock();
    l1->tracker_pointer = md2_way;
    l1->replacement_pointer = l2_way;
    core->number_of_l1_entries++;

    //get active md and update LI
    avdc_md_t *active_md = search_active_md(cache, thread_id, va);

    if (check_l3(active_md->li[region_num])) {
        clear_l3_entry_va(cache, active_md->address[region_num]);
    }

    active_md->li[region_num] = L1_BASE + l1_way;

    return l1_way;

}

int add_to_l2(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way) {

    avdark_cache_core *core = cache->cores[thread_id];

    int l2_index = l2_index_from_pa(core, va);

    int test_l2_way = search_l2_way(core, va);
    int result = assert_debug(cache, thread_id, test_l2_way, -1, "***err L2 way already exists, add_to_l2\n", 1);
    if(result == 0)
        return test_l2_way; //hack

    int l2_way = find_l2_way(cache, thread_id, va);
    int region_num = region_from_pa(core, va);
    avdc_pa_t tag = tag_from_pa(core, va);
    avdc_pa_t base_pa = pa_from_tag(core, tag);

    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

    /// CREATE l2 entry
    //assert(l1->valid == 0 && l1->active == 0);
    assert_debug(cache, thread_id, l2->active, 0, "***err L2 not active, add_to_l2\n", 1);
    assert_debug(cache, thread_id, l2->valid, 0, "***err L2 not valid, add_to_l2\n", 1);

    l2->valid = 1;
    l2->address = base_pa + (core->block_size * region_num);

    l2->active = 0;
    l2->timestamp = clock();
    l2->tracker_pointer = md2_way;
    l2->replacement_pointer = -1;
    //TODO: l2->replacement_pointer = MD3_way;
    core->number_of_l2_entries++;

    return l2_way;

}

int create_md1(avdc_cache *cache, int thread_id, avdc_pa_t va, int md2_way) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    avdc_pa_t base_pa = pa_from_tag(core, tag);
    int region_num = region_from_pa(core, va);

    int md1_index = md1_index_from_pa(core, va);
    int test_md1_way = search_md1_way(core, va);
    int result = assert_debug(cache, thread_id, test_md1_way, -1, "***err test md1_way already exists in create_md1\n",
                              1);

    avdc_md_t *md1;
    int md1_way;
    if (result == 1) {
        md1_way = find_md1_way(cache, thread_id, va);
        md1 = &core->md1[md1_index][md1_way];
    } else {
        md1 = &core->md1[md1_index][test_md1_way];
        md1_way = test_md1_way;
    }

    if (md1->valid == 0 && md1->active == 0) {
        for (int i = 0; i < REGION_SIZE; i++) {
            //initalize to memory ie: 011000
            md1->li[i] = MEM_BASE;
            md1->address[i] = base_pa + (core->block_size * i);
        }
        md1->valid = 1;
        md1->tag = tag;
        md1->active = 1;
    }

    ///added this just to be safe
    //adding here might need to add in other places as well
    // need to check the LI for the region and if it's in L3 delete it
    if (check_l3(md1->li[region_num])) {
        clear_l3_entry_va(cache, md1->address[region_num]);
    }

    md1->private_bit = 1;
    md1->tracker_pointer = md2_way;
    //md1->li[region_num] = L1_BASE + l1_way; this will be added with add_to_l1
    md1->timestamp = clock();
    core->md1_entries++;

    //add tracker pointer to md2
    avdc_md_t *md2 = search_md2_entry(cache, thread_id, va);
    md2->tracker_pointer = md1_way;

    return md1_way;
}

avdc_md_t *search_md2_entry(avdc_cache *cache, int thread_id, avdc_pa_t va) {
    avdark_cache_core *core = cache->cores[thread_id];
    int md2_index = md2_index_from_pa(core, va);
    int md2_way = search_md2_way(cache, core, va);

    assert_debug(cache, thread_id, md2_way, -1, "***err md2_way doesn't exist: search_md2\n", 0);

    avdc_md_t *md2 = &core->md2[md2_index][md2_way];

    return md2;
}

avdc_md3_t *search_md3_entry(avdc_cache *cache, avdc_pa_t va, int code) {

    int md3_index = md3_index_from_pa(cache, va);
    int md3_way = search_md3_way(cache, va);

    switch (code) {
        case 0:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 0\n", 0);
            break;
        case 1:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 1\n", 0);
            break;
        case 2:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 2\n", 0);
            break;
        case 3:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 3\n", 0);
            break;
        case 4:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 4\n", 0);
            break;
        case 5:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 5\n", 0);
            break;
        case 6:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 6\n", 0);
            break;
        default:
            assert_debug(cache, -1, md3_way, -1, "***err md3_way doesn't exist: search_md3 7\n", 0);
            break;

    }

    avdc_md3_t *md3 = &cache->md3[md3_index][md3_way];

    return md3;
}

avdc_md_t *search_active_md(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdc_md_t *md1 = search_md1_entry(cache, thread_id, va);
    avdc_md_t *md2 = search_md2_entry(cache, thread_id, va);

    if (md1 != NULL) {
        assert_debug(cache, thread_id, md1->active, md2->active,
                     "***err md1 and md2 active conflict search_active_md\n", 0);

        if (md1->active == 1 && md2->active == 0) {
            return md1;
        } else if (md2->active == 1 && md1->active == 0) {
            return md2;
        } else {
            assert_debug(cache, thread_id, 1, 0,
                         "***err md1 and md2 active conflict search_active_md echo second error\n", 1);
        }
    } else {
        assert_debug(cache, thread_id, md2->active, 1, "***err no md1 entry and md2 not valid search_active_md\n", 1);
        return md2;
    }

    return NULL;
}

avdc_cache_line_t *search_l3_entry(avdc_cache *cache, avdc_pa_t va) {

    int l3_index = l3_index_from_pa(cache, va);
    int l3_way = search_l3_way(cache, va);

    //assert_debug(cache, thread_id, md1_way, -1, "***err md2_way doesn't exist: search_md1\n", 0); /MD1 doesn't always ned to exist
    if (l3_way == -1) {
        return NULL;
    } else {
        avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];
        return l3;
    }
}

avdc_cache_line_t *search_l2_entry(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];

    int l2_index = l2_index_from_pa(core, va);
    int l2_way = search_l2_way(core, va);

    assert_debug(cache, thread_id, l2_way, -1, "***err l2_way doesn't exist: search_l2_entry\n", 0);
    if (l2_way == -1) {
        return NULL;
    } else {
        avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];
        return l2;
    }
}

avdc_cache_line_t *search_l1_entry(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];

    int l1_index = l1_index_from_pa(core, va);
    int l1_way = search_l1_way(core, va);

    assert_debug(cache, thread_id, l1_way, -1, "***err l1_way doesn't exist: search_l1_entry\n", 0);
    if (l1_way == -1) {
        return NULL;
    } else {
        avdc_cache_line_t *l1 = &core->l1[l1_index][l1_way];
        return l1;
    }
}


avdc_md_t *search_md1_entry(avdc_cache *cache, int thread_id, avdc_pa_t va) {
    avdark_cache_core *core = cache->cores[thread_id];
    int md1_index = md1_index_from_pa(core, va);
    int md1_way = search_md1_way(core, va);

    //assert_debug(cache, thread_id, md1_way, -1, "***err md2_way doesn't exist: search_md1\n", 0); /MD1 doesn't always ned to exist
    if (md1_way == -1) {
        return NULL;
    } else {
        avdc_md_t *md1 = &core->md1[md1_index][md1_way];
        return md1;
    }

}

int create_md2(avdc_cache *cache, int thread_id, avdc_pa_t va) {

    avdark_cache_core *core = cache->cores[thread_id];

    avdc_pa_t tag = tag_from_pa(core, va);
    avdc_pa_t base_pa = pa_from_tag(core, tag);

    int test_md2_way = search_md2_way(cache, core, va);
    assert_debug(cache, thread_id, test_md2_way, -1, "***err md2_way already exists can't add again: create_md2\n",
                 1); //his might blow up

    int md2_index = md2_index_from_pa(core, va);
    int md2_way = find_md2_way(cache, thread_id, va);

    avdc_md_t *md2 = &core->md2[md2_index][md2_way];

    assert_debug(cache, thread_id, md2->active, 0, "***err md2 active: create_md2\n", 1);
//    if ((md2->valid == 1 && md2->active == 1) && (md2->tag != tag)) {
//        printf("***err: OVERWRITE ACTIVE MD2 INDEX: %d\n", md2_index);
//        exit(EXIT_FAILURE);
//    }
    if (md2->valid == 0 && md2->active == 0) {
        for (int i = 0; i < REGION_SIZE; i++) {
            //initalize to memory ie: 011000
            md2->li[i] = MEM_BASE;
            md2->address[i] = base_pa + (core->block_size * i);

        }
        md2->valid = 1;
        md2->tag = tag;
        md2->active = 0;
    }

    md2->tracker_pointer = -1; //this will be added by create_md1

    //md2->li[region_num] = L1_BASE + l1_way; //this will done in add_to_l1
    md2->timestamp = clock();
    md2->active = 0;
    md2->private_bit = 1; //why not it is private
    core->md2_entries++;

    return md2_way;

}


/**
 *
 *    //TODO: test this function
 *
 * @param cache
 * @param thread_id
 * @param va
 * @param cache_level
 */
void copy_l2_to_l1(avdc_cache *cache, int thread_id, avdc_pa_t va, int cache_level) {

    avdark_cache_core *core = cache->cores[thread_id];
    int region_num = region_from_pa(core, va);

    avdc_md_t *active_md;
    if (cache_level == 1) {
        int md1_index = md1_index_from_pa(core, va);
        int md1_way = search_md1_way(core, va);
        assert(md1_way != -1);
        active_md = &core->md1[md1_index][md1_way];
    } else if (cache_level == 2) {
        int md2_index = md2_index_from_pa(core, va); //tested
        int md2_way = search_md2_way(cache, core, va);
        assert(md2_way != -1);
        active_md = &core->md2[md2_index][md2_way];
    } else {
        assert_debug(cache, thread_id, 1, 0, "***err wrong parameter in copy_l2_to_l1\n", 1);
//        if(DEBUG == 1 ) {
//            core->stat.errors++;
//            if(PRINT == 1) {
//                printf("wrong parameter in copy_l2_to_l1");
//            }
//        } else {
//            printf("wrong parameter in copy_l2_to_l1");
//            exit(1);
//        }
    }

    int l2_way = search_l2_way(core, va);
    // assert(l2_way != -1);
    assert_debug(cache, thread_id, l2_way, -1, "***err l2_way copy_l2_to_l1\n", 0);
//    if(DEBUG ==1){
//        if(l2_way == -1) {
//            cache->stat3.errors++;
//            if(PRINT)
//                printf("***err l2_way copy_l2_to_l1\n"); } //ahh
//    } else {
//        assert(l2_way != -1);
//    }

    int md2_way = search_md2_way(cache, core, va);
    //assert(md2_way != -1);
    assert_debug(cache, thread_id, md2_way, -1, "***err md2_way copy_l2_to_l1\n", 0);

//    if(DEBUG ==1){
//        if(md2_way == -1) {
//            cache->stat3.errors++;
//            if(PRINT)
//                printf("***err md2_way copy_l2_to_l1\n"); }
//    } else {
//        assert(md2_way != -1);
//    }

    int l1_way = add_to_l1(cache, thread_id, va, md2_way, l2_way);

    //active_md->li[region_num] = L1_BASE + l1_way; //don't need this anymore is taken care of in add_to_l1

    int l2_index = l2_index_from_pa(core, va);
    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

    l2->active = 0; //deactivate l2

}


int search_l2_way(avdark_cache_core *self, avdc_pa_t pa) {

    int l2_index = l2_index_from_pa(self, pa);
    int i;

    for (i = 0; i < L2_ASSOC; i++) {

        if ((self->l2[l2_index][i].address <= pa) &&
            (pa < self->l2[l2_index][i].address + BLOCK_SIZE) &&
            self->l2[l2_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_l2_way(avdc_cache *cache, int thread_id, avdc_pa_t pa) {

    avdark_cache_core *self = cache->cores[thread_id];

    int l2_index = l2_index_from_pa(self, pa);
    int i;

    for (i = 0; i < L2_ASSOC; i++) {
        if (self->l2[l2_index][i].valid == 0) {
            return i;
        }
    }

    if (CAP_PRINT && (cap_array[1] == 0)) {
        printf("***info: L2 CAP\n");
        cap_array[1] = 1;
    }

    int LRU_way = 0;
    double diff_time = 0;
<<<<<<< HEAD
    for(i=0; i < L2_ASSOC; i++){
=======
    for (i = 1; i < L2_ASSOC; i++) {
>>>>>>> temp
        diff_time = difftime(self->l2[l2_index][LRU_way].timestamp,
                             self->l2[l2_index][i].timestamp);
        if (diff_time > 0)
            LRU_way = i;
    }

    avdc_pa_t LRU_pa = self->l2[l2_index][LRU_way].address;
    avdc_pa_t LRU_tag = tag_from_pa(self, LRU_pa);
    int region_num = region_from_pa(self, pa);

    int LRU_l1_way = search_l1_way(self, LRU_pa);

    if (LRU_l1_way != -1) {
        clear_l1_entry_va(self, LRU_pa);
    }

    //insert data into l3 and remove from l2
    int l3_way = add_to_l3(cache, LRU_pa);
    int result = assert_debug(cache, thread_id, l3_way, -1, "*** err: l3_way, in find_l2_way\n", 0);

    //TODO remove this line since add_l3 takes care of the core clearing, but right now this function has a check so it can stay
    clear_l2_entry_va(self, LRU_pa);

    avdc_md_t *active_md = search_active_md(cache, thread_id, LRU_pa);
    assert_addr(cache, thread_id, &active_md->address[region_num], LRU_pa, 4);
    active_md->li[region_num] = L3_BASE + l3_way; //huge bug fix


    return LRU_way;
}


void clear_l2_entry_va(avdark_cache_core *core, avdc_pa_t va) {

    int l2_index = l2_index_from_pa(core, va);
    int l2_way = search_l2_way(core, va);
    //assert(l2_way != -1); //TODO: add back in
    //TODO: remove this works for right now
    if (l2_way == -1)
        return;

    avdc_cache_line_t *l2 = &core->l2[l2_index][l2_way];

    l2->valid = 0;
    l2->active = 0;
    l2->address = 0;
    l2->tracker_pointer = 0;
    l2->replacement_pointer = 0;
    l2->timestamp = 0;

    core->number_of_l2_entries--;

}


void
avdc_print_info(avdark_cache_core *self) {
    printf("Cache Info\n");
    printf("size: %d, assoc: %d, line-size: %d, num-sets l1: %d, num-sets l2: %d\n",
           self->l1_size, self->assoc, self->block_size,
           self->number_of_l1_sets, self->number_of_l2_sets);
}

void
avdc_print_statistics(avdc_cache *cache) {
    //printf("**info: statistics:\n");
    //printf(" accesses: \t%d\n", )

    unsigned long long stats[19] = {0};

    for (int i = 0; i < CORES; i++) {
        stats[0] += cache->cores[i]->stat.data_read;
        stats[1] += cache->cores[i]->stat.data_read_miss;
        stats[2] += cache->cores[i]->stat.data_write;
        stats[3] += cache->cores[i]->stat.data_write_miss;
        stats[4] += cache->cores[i]->stat.md1_hit;
        stats[5] += cache->cores[i]->stat.md1_l1_hit;
        stats[6] += cache->cores[i]->stat.md1_l2_hit;
        stats[7] += cache->cores[i]->stat.md1_l3_hit;
        stats[8] += cache->cores[i]->stat.md1_mem_hit;
        stats[9] += cache->cores[i]->stat.md1_rem_hit;
        stats[10] += cache->cores[i]->stat.md2_hit;
        stats[11] += cache->cores[i]->stat.md2_l1_hit;
        stats[12] += cache->cores[i]->stat.md2_l2_hit;
        stats[13] += cache->cores[i]->stat.md2_l3_hit;
        stats[14] += cache->cores[i]->stat.md2_mem_hit;
        stats[15] += cache->cores[i]->stat.md2_rem_hit;
        stats[16] += cache->cores[i]->stat.md2_mem_miss;
        stats[17] += cache->cores[i]->stat.errors;
        stats[18] += cache->cores[i]->stat.addr_errors;

    }

    printf("%llu, ", stats[0]);
    printf("%llu, ", stats[1]);
    printf("%llu, ", stats[2]);
    printf("%llu, ", stats[3]);
    printf("%llu, ", stats[4]);
    printf("%llu, ", stats[5]);
    printf("%llu, ", stats[6]);
    printf("%llu, ", stats[7]);
    printf("%llu, ", stats[8]);
    printf("%llu, ", stats[9]);
    printf("%llu, ", stats[10]);
    printf("%llu, ", stats[11]);
    printf("%llu, ", stats[12]);
    printf("%llu, ", stats[13]);
    printf("%llu, ", stats[14]);
    printf("%llu, ", stats[15]);
    printf("%llu, ", stats[16]);
    printf("%llu, ", stats[17]);
    printf("%llu, ", stats[18]);


    printf("%llu, ", cache->stat3.md3_hit);
    printf("%llu, ", cache->stat3.md3_l3_hit);
    printf("%llu, ", cache->stat3.md3_mem_hit);
    printf("%llu, ", cache->stat3.md3_rem_hit);
    printf("%llu, ", cache->stat3.md3_mem_miss);
    printf("%llu, ", cache->stat3.errors);
    printf("%llu, ", cache->stat3.addr_errors);


//    /* TODO: add these statistics
//        int                stat_md2_ns_hit;
//        int                stat_md2_fs_hit;
//        int                stat_md2_rem_hit;
//    */

}

void
avdc_print_cache_statistics(avdc_cache *cache) {

    //printf("**info: statistics:\n");
//    printf(" md3_hit: \t%ld\n md3_l1_hit: \t%ld\n md3_l2_hit: \t%ld\n md3_l3_hit: \t%ld\n md3_mem_hit: \t%ld\n md3_mem_miss: \t%ld\n md3_rem_hit: \t%ld\n",
//           cache->stat3.md3_hit,
//           cache->stat3.md3_l1_hit,
//           cache->stat3.md3_l2_hit,
//           cache->stat3.md3_l3_hit,
//           cache->stat3.md3_mem_hit,
//           cache->stat3.md3_mem_miss,
//           cache->stat3.md3_rem_hit);

    /* TODO: add these statistics
//    int                md3_ns_hit;
//    int                md3_fs_hit;
//    int                md3_rem_hit;
    */

}


int search_l3_way(avdc_cache *cache, avdc_pa_t va) {
    int l3_index = l3_index_from_pa(cache, va);


    /*
     *  TODO: does this still use the block thing?
     */

    int i;
    for (i = 0; i < L3_ASSOC; i++) {
        if (cache->l3[l3_index][i].address <= va &&
            va < cache->l3[l3_index][i].address + BLOCK_SIZE &&
            cache->l3[l3_index][i].valid == 1) {
            return i;
        }
    }
    return -1;
}

int find_l3_way(avdc_cache *cache, avdc_pa_t pa) {

    int l3_index = l3_index_from_pa(cache, pa);
    int i;
    for (i = 0; i < L3_ASSOC; i++) { //that's a fix
        if (cache->l3[l3_index][i].valid == 0) {
            return i;
        }
    }

    if (CAP_PRINT && (cap_array[2] == 0)) {
        printf("***info: L3 CAP\n");
        cap_array[2] = 1;
    }

    //find LRU l3
    int LRU_way = 0;
    double diff_time = 0;
<<<<<<< HEAD
    for(i=0; i < L3_ASSOC; i++){
=======
    for (i = 1; i < L3_ASSOC; i++) {
>>>>>>> temp
        diff_time = difftime(cache->l3[l3_index][LRU_way].timestamp,
                             cache->l3[l3_index][i].timestamp);
        if (diff_time > 0) {
            LRU_way = i;
        }
    }

    avdc_cache_line_t *l3 = &cache->l3[l3_index][LRU_way];

    assert_debug(cache, -1, l3->active, 1, "***err l3->active find_l3_way\n", 1);
    assert_debug(cache, -1, l3->valid, 1, "***err l3->valid find_l3_way\n", 1);

    avdc_pa_t LRU_va = l3->address;

    avdc_md3_t *md3 = search_md3_entry(cache, LRU_va, 5);

    int region_num = region_from_pa(cache->cores[0], LRU_va);

    assert_addr(cache, -1, &md3->address[region_num], LRU_va, 6);

    //TODO: add check to see if pb gets reset
    //TODO-flag: not finished
    //TODO: gonna need a thread id here
    int pb = md3->pb;

    avdc_pa_t tag = region_from_pa(cache->cores[0], LRU_va);
    if (tag == 137321127281 && region_num == 13) {
        printf(" evicting cachline from l3 to memory \n");
    }


    //update md3
    md3->li[region_num] = MEM_BASE;
    //remove l3 entry from md3
    clear_l3_entry_va(cache, LRU_va);


    //TODO: check md3 pb and then decide to invaldiate
    //i think doing this manually here would be better
    invalidate_cachline(cache, -1, LRU_va); //TODO: test this might not be the right place

    return LRU_way;
}

void clear_l3_entry_va(avdc_cache *cache, avdc_pa_t va) {

    int l3_index = l3_index_from_pa(cache, va);
    int l3_way = search_l3_way(cache, va);
    //assert(l3_way != -1); //TODO: add back in
    if (l3_way == -1)
        return;

    avdc_cache_line_t *l3 = &cache->l3[l3_index][l3_way];

    l3->valid = 0;
    l3->active = 0;
    l3->address = 0;
    l3->tracker_pointer = 0;
    l3->replacement_pointer = 0;
    l3->timestamp = 0;

}
