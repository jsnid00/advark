
/*! @file
 *  This is an example of the PIN tool that demonstrates some basic PIN APIs
 *  and could serve as the starting point for developing your first PIN tool
 */

#include "pin.H"
#include <iostream>
#include <fstream>

#include <sys/time.h>

extern "C" {
#include "d2m_cache.h"
}
FILE * trace;
PIN_LOCK lock;

KNOB<string> knob_output(KNOB_MODE_WRITEONCE,    "pintool",
                         "o", "stats.out", "specify log file name");
KNOB<UINT32> knob_size(KNOB_MODE_WRITEONCE, "pintool",
                       "s", "32768", "Cache size (bytes)");
KNOB<UINT32> knob_associativity(KNOB_MODE_WRITEONCE, "pintool",
                                "a", "8", "Cache associativity");
KNOB<UINT32> knob_line_size(KNOB_MODE_WRITEONCE, "pintool",
                            "l", "64", "Cache line size");
KNOB<UINT32> knob_region_size(KNOB_MODE_WRITEONCE, "pintool",
                              "r", "16", "Region size in cache lines");

//static avdark_cache_core *avdc = NULL;
static     avdc_cache *avdc = NULL;

/**
 * Memory access callback. Will be called for every memory access
 * executed by the the target application.
 */
static VOID
simulate_access(VOID *addr, UINT32 access_type, THREADID id_thread)
{

    /* NOTE: We deliberately ignore the fact that addr may
     * straddle multiple cache lines */
<<<<<<< HEAD
    //PIN_LockClient();
    PIN_GetLock(&lock, id_thread);
=======
   // PIN_LockClient();
    PIN_GetLock(&lock, id_thread+1);

>>>>>>> temp
    if(access_type == AVDC_WRITE) {
        fprintf(trace,"W %p %d\n", addr, id_thread);
    } else {
        fprintf(trace,"R %p %d\n", addr, id_thread);
    }
<<<<<<< HEAD
    avdc_access(avdc, (avdc_pa_t)addr, (type_t)access_type, id_thread);

    PIN_ReleaseLock(&lock);
    //PIN_UnlockClient();
=======

    avdc_access(avdc, (avdc_pa_t)addr, (type_t)access_type, id_thread);

    PIN_ReleaseLock(&lock);
  //  PIN_UnlockClient();
>>>>>>> temp
}

VOID printip(VOID *ip) {
    //    fprintf(trace, "%p\n", ip);
}

// Print a memory read record
VOID RecordMemRead(VOID * ip, VOID * addr, THREADID id_thread)
{
    PIN_GetLock(&lock, id_thread+1);
    //fprintf(trace,"R %p %d\n", addr, id_thread);
    //avdc_access(avdc, (avdc_pa_t)addr, AVDC_READ, id_thread);

    PIN_ReleaseLock(&lock);
}

// Print a memory write record
VOID RecordMemWrite(VOID * ip, VOID * addr, THREADID id_thread)
{
    PIN_GetLock(&lock, id_thread+1);
    //fprintf(trace,"W %p %d\n", addr, id_thread);
    //avdc_access(avdc, (avdc_pa_t)addr, AVDC_WRITE, id_thread);

    PIN_ReleaseLock(&lock);
}

VOID ThreadStart(THREADID threadid, CONTEXT *ctxt, INT32 flags, VOID *v)
{
    PIN_GetLock(&lock, threadid+1);
    //fprintf(trace, "thread begin %d\n",threadid);
    //fflush(trace);
    PIN_ReleaseLock(&lock);
}

// This routine is executed every time a thread is destroyed.
VOID ThreadFini(THREADID threadid, const CONTEXT *ctxt, INT32 code, VOID *v)
{
    PIN_GetLock(&lock, threadid+1);
    //fprintf(trace, "thread end %d code %d\n",threadid, code);
    //fflush(trace);
    PIN_ReleaseLock(&lock);
}

/**
 * PIN instrumentation callback, called for every new instruction that
 * PIN discovers in the application. This function is used to
 * instrument code blocks by inserting calls to instrumentation
 * functions.
 */
static VOID
instruction(INS ins, VOID *not_used)
{
    UINT32 no_ops =      INS_MemoryOperandCount(ins);

<<<<<<< HEAD
    int trace = 0;
=======

		//don't have both turned on
    int trace_flag = 0;
>>>>>>> temp
    int sim = 1;

    for (UINT32 op = 0; op < no_ops; op++) {
        //const UINT32 size = INS_MemoryOperandSize(ins, op);
        //const bool is_rd = INS_MemoryOperandIsRead(ins, op);
        const bool is_wr = INS_MemoryOperandIsWritten(ins, op);
        const UINT32 atype = is_wr ? AVDC_WRITE : AVDC_READ;

        //INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)printip, IARG_INST_PTR, IARG_END);

     ///take this out to optimize for the access
        if(trace_flag == 1) {
            if (INS_MemoryOperandIsRead(ins, op)) {
                INS_InsertCall(
                        ins, IPOINT_BEFORE, (AFUNPTR) RecordMemRead,
                        IARG_INST_PTR,
                        IARG_MEMORYOP_EA, op, IARG_THREAD_ID,
                        IARG_END);
            }
            // Note that in some architectures a single memory operand can be
            // both read and written (for instance incl (%eax) on IA-32)
            // In that case we instrument it once for read and once for write.
            if (INS_MemoryOperandIsWritten(ins, op)) {
                INS_InsertCall(
                        ins, IPOINT_BEFORE, (AFUNPTR) RecordMemWrite,
                        IARG_INST_PTR,
                        IARG_MEMORYOP_EA, op, IARG_THREAD_ID,
                        IARG_END);
            }
        }
        if(sim == 1) {
            INS_InsertCall(ins, IPOINT_BEFORE,
                           (AFUNPTR) simulate_access,
                           IARG_MEMORYOP_EA, op,
                           IARG_UINT32, atype, IARG_THREAD_ID,
                           IARG_END);
        }
    }
}

/**
 * PIN fini callback. Called after the target application has
 * terminated. Used to print statistics and do cleanup.
 */
static VOID
fini(INT32 code, VOID *v)
{

    std::ofstream out(knob_output.Value().c_str());

    int stats[19] = {0};

    for(int i = 0; i < CORES; i++) {
        stats[0] += avdc->cores[i]->stat.data_read;
        stats[1] += avdc->cores[i]->stat.data_read_miss;
        stats[2] += avdc->cores[i]->stat.data_write;
        stats[3] += avdc->cores[i]->stat.data_write_miss;
        stats[4] += avdc->cores[i]->stat.md1_hit;
        stats[5] += avdc->cores[i]->stat.md1_l1_hit;
        stats[6] += avdc->cores[i]->stat.md1_l2_hit;
        stats[7] += avdc->cores[i]->stat.md1_l3_hit;
        stats[8] += avdc->cores[i]->stat.md1_mem_hit;
        stats[9] += avdc->cores[i]->stat.md1_rem_hit;
        stats[10] += avdc->cores[i]->stat.md2_hit;
        stats[11] += avdc->cores[i]->stat.md2_l1_hit;
        stats[12] += avdc->cores[i]->stat.md2_l2_hit;
        stats[13] += avdc->cores[i]->stat.md2_l3_hit;
        stats[14] += avdc->cores[i]->stat.md2_mem_hit;
        stats[15] += avdc->cores[i]->stat.md2_rem_hit;
        stats[16] += avdc->cores[i]->stat.md2_mem_miss;
        stats[17] += avdc->cores[i]->stat.errors;
        stats[18] += avdc->cores[i]->stat.addr_errors;

//        out << avdc->cores[i]->stat.data_read << ", ";
//        out << avdc->cores[i]->stat.data_read_miss << ", ";
//        out << avdc->cores[i]->stat.data_write << ", ";
//        out << avdc->cores[i]->stat.data_write_miss << ", ";
//        out << avdc->cores[i]->stat.md1_hit << ", ";
//        out << avdc->cores[i]->stat.md1_l1_hit << ", ";
//        out << avdc->cores[i]->stat.md1_l2_hit << ", ";
//        out << avdc->cores[i]->stat.md1_l3_hit << ", ";
//        out << avdc->cores[i]->stat.md1_mem_hit << ", ";
//        out << avdc->cores[i]->stat.md1_rem_hit << ", ";
//        out << avdc->cores[i]->stat.md2_hit << ", ";
//        out << avdc->cores[i]->stat.md2_l1_hit << ", ";
//        out << avdc->cores[i]->stat.md2_l2_hit << ", ";
//        out << avdc->cores[i]->stat.md2_l3_hit << ", ";
//        out << avdc->cores[i]->stat.md2_mem_hit << ", ";
//        out << avdc->cores[i]->stat.md2_rem_hit << ", ";
//        out << avdc->cores[i]->stat.md2_mem_miss << ", ";
//        out << avdc->cores[i]->stat.errors << endl;
    }

    out << stats[0]   << ", ";
    out << stats[1]   << ", ";
    out << stats[2]   << ", ";
    out << stats[3]   << ", ";
    out << stats[4]   << ", ";
    out << stats[5]   << ", ";
    out << stats[6]   << ", ";
    out << stats[7]   << ", ";
    out << stats[8]   << ", ";
    out << stats[9]   << ", ";
    out << stats[10]  << ", ";
    out << stats[11]  << ", ";
    out << stats[12]  << ", ";
    out << stats[13]  << ", ";
    out << stats[14]  << ", ";
    out << stats[15]  << ", ";
    out << stats[16]  << ", ";
    out << stats[17]  << ", ";
    out << stats[18]  << ", ";


    out << avdc->stat3.md3_hit << ", ";
    out << avdc->stat3.md3_l3_hit << ", ";
    out << avdc->stat3.md3_mem_hit << ", ";
    out << avdc->stat3.md3_rem_hit << ", ";
    out << avdc->stat3.md3_mem_miss << ", ";
    out << avdc->stat3.errors << ", ";
    out << avdc->stat3.addr_errors << endl;


    multi_core_delete(avdc);

    fprintf(trace, "#eof\n");
    fclose(trace);


}

static int
usage()
{
    cerr <<
         "This is the Advanced Computer Architecture analysis tool\n"
                 "\n";

    cerr << KNOB_BASE::StringKnobSummary();

    cerr << endl;

    return -1;
}

int main(int argc, char *argv[])
{
    PIN_InitLock(&lock);
    //
    trace = fopen("/it/slask/student/josn3503/trace/trace.out", "w");

    if (PIN_Init(argc, argv))
        return usage();
    //TODO-question: should this be included
     PIN_InitSymbols();

    //avdc_size_t size = knob_size.Value();
    //avdc_block_size_t block_size = knob_line_size.Value();
    //avdc_assoc_t assoc = knob_associativity.Value();
    //avdc_region_t region_size = knob_region_size.Value();

    //avdc = avdc_new(size, block_size, assoc, region_size);
    avdc = avdc_new_multi_core(32768, 64, 8, 16); //these parameters aren't used
    if (!avdc) {
        cerr << "Failed to initialize the AvDark cache simulator." << endl;
        return -1;
    }

    INS_AddInstrumentFunction(instruction, 0);
    PIN_AddThreadStartFunction(ThreadStart, 0);
    PIN_AddThreadFiniFunction(ThreadFini, 0);
    PIN_AddFiniFunction(fini, 0);

    PIN_StartProgram();
    return 0;
}
