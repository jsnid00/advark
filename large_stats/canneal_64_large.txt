[PARSEC] Benchmarks to run:  parsec.canneal

[PARSEC] [========== Running benchmark parsec.canneal [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simlarge'.
400000.nets
[PARSEC] Running '/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/64_d2m_pin.so -o /home/josn3503/advark/large_stats/canneal_64_large_stats.txt --  /it/slask/student/josn3503/parsec-3.0/pkgs/kernels/canneal/inst/amd64-linux.gcc/bin/canneal 1 15000 2000 400000.nets 128':
[PARSEC] [---------- Beginning of output ----------]
PARSEC Benchmark Suite Version 3.0-beta-20150206
Threadcount: 1
15000 swaps per temperature step
start temperature: 2000
netlist filename: 400000.nets
number of temperature steps: 128
locs created
locs assigned
Just saw element: 100000
Just saw element: 200000
Just saw element: 300000
Just saw element: 400000
netlist created. 400000 elements.
Final routing is: 6.90232e+08
block size:	 64
region size:	 64
l1 ASSOC:	 8
md1 ASSOC:	 3
md2 ASSOC:	 3
md3 ASSOC:	 3
***info: L1 CAP
***info: MD1 CAP
***info: L2 CAP
***info: L3 CAP
***info: MD2 CAP
***info: MD3 CAP
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.
