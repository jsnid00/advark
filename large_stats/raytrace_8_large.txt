[PARSEC] Benchmarks to run:  parsec.raytrace

[PARSEC] [========== Running benchmark parsec.raytrace [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simlarge'.
happy_buddha.obj
[PARSEC] Running '/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/8_d2m_pin.so -o /home/josn3503/advark/large_stats/raytrace_8_large_stats.txt --  /it/slask/student/josn3503/parsec-3.0/pkgs/apps/raytrace/inst/amd64-linux.gcc/bin/rtview happy_buddha.obj -automove -nthreads 1 -frames 3 -res 1920 1080':
[PARSEC] [---------- Beginning of output ----------]
this =0xb501c0
PARSEC Benchmark Suite Version 3.0-beta-20150206
initializing LRT ray tracer ...
default BVH builder : binnedalldimssavespace


Options:  automove = ;
    files = happy_buddha.obj;
   frames = 3;
 nthreads = 1;
      res = [1920, 1080];

Using memory framebuffer...
Adding obj file: happy_buddha.obj
Using auto camera...
num nodes in scene graph 1
adding 543522 vertices
adding 1087425 triangles
finalizing geometry
vertices            543522 (6369.4 KB)
triangles           1087425 (12743.3 KB)
texture coordinates 0 (0 KB)
No materials -> create dummy material
building index
using BVH builder default
build time 1950.94
done
sceneAABB =[[[-0.04615,0.049738,-0.047301,0] ],[[0.035226,0.24777,0.034077,0] ]]
Rendering 3 frames... 
Done
block size:	 64
region size:	 8
l1 ASSOC:	 8
md1 ASSOC:	 12
md2 ASSOC:	 12
md3 ASSOC:	 12
***info: L1 CAP
***info: L2 CAP
***info: MD1 CAP
***info: L3 CAP
***info: MD2 CAP
***info: MD3 CAP
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.
