[PARSEC] Benchmarks to run:  parsec.freqmine

[PARSEC] [========== Running benchmark parsec.freqmine [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simlarge'.
kosarak_990k.dat
[PARSEC] Running '/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/32_d2m_pin.so -o /home/josn3503/advark/large_stats/freqmine_32_large_stats.txt --  /it/slask/student/josn3503/parsec-3.0/pkgs/apps/freqmine/inst/amd64-linux.gcc/bin/freqmine kosarak_990k.dat 790':
[PARSEC] [---------- Beginning of output ----------]
PARSEC Benchmark Suite Version 3.0-beta-20150206
transaction number is 990002
1548
13483
46649
131381
415489
1239344
2985564
5696308
8709873
10789491
10902648
9011183
6094236
3374161
1530954
564958
165173
36535
5727
578
33
1
the data preparation cost 372.314923 seconds, the FPgrowth cost 4602.136000 seconds
block size:	 64
region size:	 32
l1 ASSOC:	 8
md1 ASSOC:	 5
md2 ASSOC:	 5
md3 ASSOC:	 5
***info: L1 CAP
***info: L2 CAP
***info: MD1 CAP
***info: L3 CAP
***info: MD2 CAP
***info: MD3 CAP
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.
