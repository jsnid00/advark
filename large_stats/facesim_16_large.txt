[PARSEC] Benchmarks to run:  parsec.facesim

[PARSEC] [========== Running benchmark parsec.facesim [1] ==========]
[PARSEC] Deleting old run directory.
[PARSEC] Setting up run directory.
[PARSEC] Unpacking benchmark input 'simlarge'.
Face_Data/
Face_Data/Eftychis_840k/
Face_Data/Eftychis_840k/Front_370k/
Face_Data/Eftychis_840k/Front_370k/attachment_list.txt
Face_Data/Eftychis_840k/Front_370k/buccinator_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/buccinator_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/corrugator_supercilii_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/corrugator_supercilii_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/cranium.attached_nodes
Face_Data/Eftychis_840k/Front_370k/cranium_extended.attached_nodes
Face_Data/Eftychis_840k/Front_370k/depressor_anguli_oris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/depressor_anguli_oris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/depressor_labii_inferioris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/depressor_labii_inferioris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/face_simulation.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_1.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_128.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_16.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_2.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_3.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_32.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_4.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_6.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_64.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_8.tet
Face_Data/Eftychis_840k/Front_370k/face_simulation_original.log
Face_Data/Eftychis_840k/Front_370k/flesh.attached_nodes
Face_Data/Eftychis_840k/Front_370k/flesh_extended.attached_nodes
Face_Data/Eftychis_840k/Front_370k/jaw.attached_nodes
Face_Data/Eftychis_840k/Front_370k/jaw_joint_parameters
Face_Data/Eftychis_840k/Front_370k/levator_anguli_oris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/levator_anguli_oris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/levator_labii_superioris_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/levator_labii_superioris_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_lateral_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_lateral_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_medial_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/llsan_medial_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/mentalis_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/mentalis_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/muscle_list.txt
Face_Data/Eftychis_840k/Front_370k/nasalis_alar_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/nasalis_alar_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/nasalis_transverse.constitutive_data
Face_Data/Eftychis_840k/Front_370k/node_divisions_1.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_128.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_16.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_2.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_3.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_32.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_4.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_6.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_64.dat
Face_Data/Eftychis_840k/Front_370k/node_divisions_8.dat
Face_Data/Eftychis_840k/Front_370k/orbicularis_oculi_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/orbicularis_oculi_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/orbicularis_oris.constitutive_data
Face_Data/Eftychis_840k/Front_370k/particle_positions_1.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_128.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_16.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_2.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_3.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_32.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_4.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_6.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_64.dat
Face_Data/Eftychis_840k/Front_370k/particle_positions_8.dat
Face_Data/Eftychis_840k/Front_370k/peak_isometric_stress_list.txt
Face_Data/Eftychis_840k/Front_370k/procerus_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/procerus_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/risorius_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/risorius_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_major_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_major_right.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_minor_left.constitutive_data
Face_Data/Eftychis_840k/Front_370k/zygomatic_minor_right.constitutive_data
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface.phi
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface.rgd
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface.tri
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface_smoothed.phi
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface_smoothed.rgd
Face_Data/Eftychis_840k/eftychis_cranium_collision_surface_smoothed.tri
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface.phi
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface.rgd
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface.tri
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface_smoothed.phi
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface_smoothed.rgd
Face_Data/Eftychis_840k/eftychis_jaw_collision_surface_smoothed.tri
Face_Data/Motion_Data/
Face_Data/Motion_Data/Storytelling_Controls/
Face_Data/Motion_Data/Storytelling_Controls/storytelling_controls.1
Face_Data/Motion_Data/Storytelling_Controls/storytelling_controls.2
[PARSEC] Running '/it/slask/student/josn3503/pin-3.4-97438-gf90d1f746-gcc-linux/pin -t /home/josn3503/advark/obj-intel64/16_d2m_pin.so -o /home/josn3503/advark/large_stats/facesim_16_large_stats.txt --  /it/slask/student/josn3503/parsec-3.0/pkgs/apps/facesim/inst/amd64-linux.gcc/bin/facesim -timing -threads 1':
[PARSEC] [---------- Beginning of output ----------]
PARSEC Benchmark Suite Version 3.0-beta-20150206
Creating directory using system("mkdir -p Storytelling/output")...Successful!
Simulation                                        Reading simulation model : ./Face_Data/Eftychis_840k/Front_370k/face_simulation_1.tet
Total particles = 80598
Total tetrahedra = 372126
muscles = 32
attachments = 3

  Frame 1                                         
  END Frame 1                                     3980.1334 s
SIMULATION                                          0.0000
  FRAME                                           3980.1334
    ADB                                           3979.4616
      UPBS                                        270.8838
        UPBS (FEM) - Initialize                    13.7786 s
        UPBS (FEM) - Element Loop                 256.9905 s
        UPBS (CPF)                                  0.0009 s
      ADO                                         3708.5547
        ADO - Update collision bodies               0.0019 s
        AOTSQ                                     3708.5380
          AOTSQ - Initialize                        0.1772 s
          AOTSQ - NR loop                         3708.3572
            AOTSQ - NR loop - Initialize            0.1861 s
            UCPF                                   21.0978 s
            NRS                                   3068.3332
              NRS - Initialize                      0.0015 s
              NRS - Boundary conditions 1           0.2022 s
              UPBS                                384.0692
                UPBS (FEM) - Initialize             0.0007 s
                UPBS (FEM) - Element Loop         384.0656 s
                UPBS (CPF)                          0.0001 s
              AFD (FEM)                             6.6276 s
              AVIF                                 22.7179
                AVIF (FEM)                         22.5512 s
                AVIF (CPF)                          0.1602 s
              NRS - Boundary conditions 2           0.0470 s
              NRS - Compute residual                0.2654
              NRS - Copy initial guess              0.1466 s
            UPBS                                  569.7405
              UPBS (FEM) - Initialize               0.0005 s
              UPBS (FEM) - Element Loop           569.7362 s
              UPBS (CPF)                            0.0000 s
            AVIF                                   48.0006
              AVIF (FEM)                           47.8158 s
              AVIF (CPF)                            0.1825 s
            AOTSQ - NR loop - Boundary conditions   0.0532 s
            AOTSQ - NR loop - Compute residual      0.2105 s
block size:	 64
region size:	 16
l1 ASSOC:	 8
md1 ASSOC:	 8
md2 ASSOC:	 8
md3 ASSOC:	 8
[PARSEC] [----------    End of output    ----------]
[PARSEC]
[PARSEC] BIBLIOGRAPHY
[PARSEC]
[PARSEC] [1] Bienia. Benchmarking Modern Multiprocessors. Ph.D. Thesis, 2011.
[PARSEC]
[PARSEC] Done.
